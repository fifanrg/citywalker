//
//  UIView+UIView_Additional.m
//  Demo
//
//  Created by Jianing Ren on 12-10-25.
//  Copyright (c) 2012年 Jianing Ren. All rights reserved.
//

#import "UIView+UIView_Additional.h"

@implementation UIView (UIView_Additional)
- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    [self setFrame:frame];
}
- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    [self setFrame:frame];
}
- (void)setSize:(CGSize)size keepCenter:(BOOL)_keepCenter {
    CGPoint center = self.center;
    [self setSize:size];
    [self setCenter:center];
}
@end
