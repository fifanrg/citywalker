//
//  PostActivityViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-4-11.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "PostActivityViewController.h"
#import "HGPhotoWall.h"
#import "MWPhoto.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ASIFormDataRequest.h"
#import "WCAlertView.h"
#import "MBProgressHUD.h"
#import "UtilDB.h"
#define PHOTOWALLHEIGHT 95.
#define MAXIMUMPHOTONUMBER 4
#define FIELDBACKGROUNDCOLOR [UIColor colorWithRed:255.0/255.0 green:242.0/255.0 blue:232.0/255.0 alpha:1]

#define FIELD_START_ORIGIN_X 4
#define FIELD_NAME_WIDTH 53

#define MARGIN 10
@interface PostActivityViewController ()<HGPhotoWallDelegate,UIActionSheetDelegate>


@property (strong, nonatomic) UIBarButtonItem *barButtonEdit;
@property (strong, nonatomic) UIBarButtonItem *barButtonCancel;
@property (strong, nonatomic) UIBarButtonItem *barButtonDone;

@property (strong, nonatomic) HGPhotoWall *photoWall;
@property(nonatomic)int tagWhichIndex;
@property(nonatomic,retain)NSMutableArray *photos;
@property(retain,nonatomic)TPKeyboardAvoidingScrollView *containerView;

@property(retain,nonatomic)UITextField *titleTextField;
@property(retain,nonatomic)UITextField *contactTextField;


@property(retain,nonatomic)UITextField *passwordTextField;
@property(retain,nonatomic)UITextField *telephoneTextField;
@property(retain,nonatomic)UITextField *emailTextField;

@property(retain,nonatomic)UITextField *priceTextField;
@property(retain,nonatomic)UITextField *detailTextField;
@property(retain,nonatomic)UITextField *addressTextField;
//种类
@property(retain,nonatomic)NIDropDown *cateoryDropDown;
@property(retain,nonatomic)UIButton *categoryButton;
@property(retain,nonatomic)NSArray *categoryInfoArr;
//租房种类
@property(retain,nonatomic)UIButton *rentTypeButton;
@property(retain,nonatomic)NIDropDown *rentTypeDropDown;
@property(retain,nonatomic)NSArray *rentTypeInfoArr;

//suburb选择
@property(retain,nonatomic)UIButton *surburSelectionButton;

@property(nonatomic)int catId;

@property(nonatomic)int rentType;

@property(nonatomic)CGRect lastUIFrame;
@end

@implementation PostActivityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
       self.title = @"New post";
    self.barButtonCancel = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPost)];
    
    self.barButtonDone = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(sendPost)];
    self.navigationItem.leftBarButtonItem = self.barButtonCancel;
    self.navigationItem.rightBarButtonItem = self.barButtonDone;
    
    self.photoWall = [[HGPhotoWall alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+10, self.view.frame.size.width, PHOTOWALLHEIGHT)];
//    [self.photoWall setPhotos:[NSArray arrayWithObjects:
//                               @"http://www.weiphone.com/images_v3/logo.png",
//                               @"http://www.cocoachina.com/cms/uploads/allimg/121204/4196-1212041055480-L.jpg",
//                               @"http://www.cocoachina.com/cms/uploads/allimg/121204/4196-1212041513010-L.jpg", nil]];
    [self.photoWall setPhotos:[NSArray arrayWithObjects:nil, nil]];
//    [self.view addSubview:self.photoWall];
    self.photoWall.delegate = self;
    [self.photoWall setEditModel:YES];
    
//    [self.view setBackgroundColor:[UIColor grayColor]];
    
    self.photoWall.backgroundColor = [UIColor clearColor];
    
    [self.containerView setBackgroundColor:[UIColor clearColor]];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];

   
    //初始化种类
    self.catId = -1;
    self.rentType = -1;
    
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"房 屋 出 租",@"name",@1,@"catid",nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"汽 车 买 卖",@"name",@2,@"catid", nil];
    NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"生 意 转 让",@"name",@3, @"catid",nil];
    NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"教 材 转 让",@"name",@4, @"catid",nil];
    NSDictionary *dic5 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"新 货 市 场",@"name",@5, @"catid",nil];
    NSDictionary *dic6 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"招 聘 信 息",@"name",@6,@"catid", nil];
    NSDictionary *dic7 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"二 手 市 场",@"name",@7,@"catid", nil];
    NSDictionary *dic8 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"服 务 信 息",@"name",@8, @"catid",nil];
    
    self.categoryInfoArr = [[NSArray alloc]initWithObjects:dic1,dic2,dic3,dic4,dic5,dic6,dic7,dic8,nil];
    
    
    NSDictionary *dicrenttype1 = [NSDictionary dictionaryWithObjectsAndKeys:@"整  租",@"name",@0,@"renttype", nil];
    NSDictionary *dicrenttype2 = [NSDictionary dictionaryWithObjectsAndKeys:@"分  租",@"name",@1, @"renttype",nil];

    
    self.rentTypeInfoArr = [[NSArray alloc]initWithObjects:dicrenttype1,dicrenttype2, nil];
    
    
    
    [self generateFormUI];
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)cancelPost{
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)generateFormUI{
    
    CGRect screenRect = [ UIScreen mainScreen ].bounds;
    
    if (!self.containerView) {
        self.containerView = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, screenRect.size.width, screenRect.size.height)];
        [self.view addSubview:self.containerView];
        [self.containerView release];
    }
   
    
    [self.containerView addSubview:self.photoWall];
    [self.photoWall release];
    
    //title row
    CGRect titleLabelFrame = CGRectMake(FIELD_START_ORIGIN_X, 25+PHOTOWALLHEIGHT, FIELD_NAME_WIDTH, 30);
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:titleLabelFrame];
    titleLabel.text = @"标  题";
    [titleLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    
    UIImageView *requiredImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView setFrame:CGRectMake(titleLabelFrame.origin.x+titleLabelFrame.size.width+2, titleLabelFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView];
    [requiredImageView release];

    
    self.titleTextField = [[UITextField alloc]initWithFrame:CGRectMake(titleLabelFrame.origin.x+titleLabelFrame.size.width+MARGIN, titleLabelFrame.origin.y, 250, titleLabelFrame.size.height)];
    [self.titleTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.titleTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.titleTextField];
    [self.containerView addSubview:titleLabel];
    [self.titleTextField release];
    [titleLabel release];
       
    
    
    
    //category row
    CGRect categoryTitleFrame = CGRectMake(FIELD_START_ORIGIN_X, titleLabelFrame.origin.y+titleLabelFrame.size.height+3, FIELD_NAME_WIDTH, 30);
    UILabel *catLabel = [[UILabel alloc]initWithFrame:categoryTitleFrame];
    catLabel.text = @"类  别";
    [catLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    
    self.categoryButton = [[UIButton alloc]initWithFrame:CGRectMake(categoryTitleFrame.origin.x+categoryTitleFrame.size.width+MARGIN, categoryTitleFrame.origin.y, 250, categoryTitleFrame.size.height)];
    [self.categoryButton setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    [self.containerView addSubview:self.categoryButton];
    [self.containerView addSubview:catLabel];
    [self.categoryButton release];
    [catLabel release];

    
    [self.categoryButton setTitle:@"选   择   类   别" forState:UIControlStateNormal];
    [self.categoryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.categoryButton addTarget:self action:@selector(categoryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImageView *requiredImageView5 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView5 setFrame:CGRectMake(categoryTitleFrame.origin.x+categoryTitleFrame.size.width+2, categoryTitleFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView5];
    [requiredImageView5 release];
    
    
    //category data:

    
    
    //Price row
    CGRect priceFrame = CGRectMake(FIELD_START_ORIGIN_X,categoryTitleFrame.origin.y+categoryTitleFrame.size.height+3 , FIELD_NAME_WIDTH, 30);
    UILabel *priceLabel = [[UILabel alloc]initWithFrame:priceFrame];
    priceLabel.text = @"价  钱";
    [priceLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    self.priceTextField = [[UITextField alloc]initWithFrame:CGRectMake(priceFrame.origin.x+priceFrame.size.width+MARGIN, priceFrame.origin.y, 250, priceFrame.size.height)];
    [self.priceTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.priceTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.priceTextField];
    [self.containerView addSubview:priceLabel];
    [self.priceTextField release];
    [priceLabel release];

    UIImageView *requiredImageView2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView2 setFrame:CGRectMake(priceFrame.origin.x+priceFrame.size.width+2, priceFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView2];
    [requiredImageView2 release];

    
    
    
    
    CGRect contactFrame = CGRectMake(FIELD_START_ORIGIN_X,priceFrame.origin.y+priceFrame.size.height+3 , FIELD_NAME_WIDTH, 30);
    UILabel *contactLabel = [[UILabel alloc]initWithFrame:contactFrame];
    contactLabel.text = @"联系人";
    [contactLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    self.contactTextField = [[UITextField alloc]initWithFrame:CGRectMake(contactFrame.origin.x+contactFrame.size.width+MARGIN, contactFrame.origin.y, 250, contactFrame.size.height)];
    [self.contactTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.contactTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.contactTextField];
    [self.containerView addSubview:contactLabel];
    [self.contactTextField release];
    [contactLabel release];

    UIImageView *requiredImageView3 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView3 setFrame:CGRectMake(contactFrame.origin.x+contactFrame.size.width+2, contactFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView3];
    [requiredImageView3 release];
    
    
    
    CGRect phoneFrame = CGRectMake(FIELD_START_ORIGIN_X,contactFrame.origin.y+contactFrame.size.height+3 , FIELD_NAME_WIDTH, 30);
    UILabel *phoneLabel = [[UILabel alloc]initWithFrame:phoneFrame];
    phoneLabel.text = @"电  话";
    [phoneLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    self.telephoneTextField = [[UITextField alloc]initWithFrame:CGRectMake(phoneFrame.origin.x+phoneFrame.size.width+MARGIN, phoneFrame.origin.y, 250, phoneFrame.size.height)];
    [self.telephoneTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.telephoneTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.telephoneTextField];
    [self.containerView addSubview:phoneLabel];
    [self.telephoneTextField release];
    [phoneLabel release];
    
    UIImageView *requiredImageView10 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView10 setFrame:CGRectMake(phoneFrame.origin.x+phoneFrame.size.width+2, phoneFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView10];
    [requiredImageView10 release];
    
    
    
    CGRect emailFrame = CGRectMake(FIELD_START_ORIGIN_X,phoneFrame.origin.y+phoneFrame.size.height+3 , FIELD_NAME_WIDTH, 30);
    UILabel *emailLabel = [[UILabel alloc]initWithFrame:emailFrame];
    emailLabel.text = @"Email";
    [emailLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    self.emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(emailFrame.origin.x+emailFrame.size.width+MARGIN, emailFrame.origin.y, 250, emailFrame.size.height)];
    [self.emailTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.emailTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.emailTextField];
    [self.containerView addSubview:emailLabel];
    [self.emailTextField release];
    [emailLabel release];
    
    UIImageView *requiredImageView11 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView11 setFrame:CGRectMake(emailFrame.origin.x+emailFrame.size.width+2, emailFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView11];
    [requiredImageView11 release];
    
    
    
    
    CGRect passwordFrame = CGRectMake(FIELD_START_ORIGIN_X,emailFrame.origin.y+emailFrame.size.height+3 , FIELD_NAME_WIDTH, 30);
    UILabel *passwordLabel = [[UILabel alloc]initWithFrame:passwordFrame];
    passwordLabel.text = @"密  码";
    [passwordLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    self.passwordTextField = [[UITextField alloc]initWithFrame:CGRectMake(passwordFrame.origin.x+passwordFrame.size.width+MARGIN, passwordFrame.origin.y, 250, passwordFrame.size.height)];
    [self.passwordTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.passwordTextField.textColor = [UIColor blackColor];
    
    self.passwordTextField.placeholder = @"   可登陆inSyd修改发布信息";
    [self.containerView addSubview:self.passwordTextField];
    [self.containerView addSubview:passwordLabel];
    [self.passwordTextField release];
    [passwordLabel release];
    
    UIImageView *requiredImageView13 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView13 setFrame:CGRectMake(passwordFrame.origin.x+passwordFrame.size.width+2, passwordFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView13];
    [requiredImageView13 release];
    
    
    
    
    
    
    //Address row
    CGRect addressFrame = CGRectMake(FIELD_START_ORIGIN_X,passwordFrame.origin.y+passwordFrame.size.height+3 , FIELD_NAME_WIDTH, 40);
    
    self.lastUIFrame = addressFrame;
    
    UILabel *addressLabel = [[UILabel alloc]initWithFrame:addressFrame];
    addressLabel.text = @"地  址";
    [addressLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    self.addressTextField = [[UITextField alloc]initWithFrame:CGRectMake(addressFrame.origin.x+addressFrame.size.width+MARGIN, addressFrame.origin.y, 250, addressFrame.size.height)];
    [self.addressTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.addressTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.addressTextField];
    [self.containerView addSubview:addressLabel];
    [self.addressTextField release];
    [addressLabel release];

    
    
    //Details row
    CGRect detailFrame = CGRectMake(FIELD_START_ORIGIN_X,addressFrame.origin.y+addressFrame.size.height+3 , FIELD_NAME_WIDTH, 90);
    
    
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:detailFrame];
    detailLabel.text = @"详  细";
    [detailLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
    
    //设置tag给detailLabel方便重新设置位置
    [detailLabel setTag:5555];
    
    self.detailTextField = [[UITextField alloc]initWithFrame:CGRectMake(detailFrame.origin.x+detailFrame.size.width+MARGIN, detailFrame.origin.y, 250, detailFrame.size.height)];
    [self.detailTextField setBackgroundColor:FIELDBACKGROUNDCOLOR];
    self.detailTextField.textColor = [UIColor blackColor];
    
    [self.containerView addSubview:self.detailTextField];
    [self.containerView addSubview:detailLabel];
    [self.detailTextField release];
    [detailLabel release];
    
    
    UIImageView *requiredImageView4 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
    [requiredImageView4 setTag:5556];
    [requiredImageView4 setFrame:CGRectMake(detailFrame.origin.x+detailFrame.size.width+2, detailFrame.origin.y+3, 5, 5)];
    [self.containerView addSubview:requiredImageView4];
    [requiredImageView4 release];
    
    
    
    self.titleTextField.delegate = self;
    self.priceTextField.delegate = self;
    self.detailTextField.delegate = self;
    self.contactTextField.delegate = self;
    self.addressTextField.delegate = self;
    self.telephoneTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.emailTextField.delegate = self;
    
    
    self.containerView.contentSize = CGSizeMake(320, detailFrame.origin.y+detailFrame.size.height+100);
    self.containerView.scrollEnabled = YES;
}

-(void)categoryButtonClick:(id)sender{
    
    [self.cateoryDropDown setTag:9999];
    //    arr = [NSArray arrayWithObjects:@"Hello 0", @"Hello 1", @"Hello 2", @"Hello 3", @"Hello 4", @"Hello 5", @"Hello 6", @"Hello 7", @"Hello 8", @"Hello 9",nil];
    if(self.cateoryDropDown == nil) {
        CGFloat f = 400;
//        self.cateoryDropDown = [[NIDropDown alloc]showDropDown:sender :&f : self.categoryInfoArr];
        self.cateoryDropDown = [[NIDropDown alloc]showDropDown:sender withHeight:&f andInfo:self.categoryInfoArr];
        [self.cateoryDropDown setTag:9999];
        self.cateoryDropDown.delegate = self;
    }
    else {
        [self.cateoryDropDown hideDropDown:sender];
        [self rel:self.cateoryDropDown];
    }
}

-(void)rentTypeSelect:(id)sender{
    
    [self.rentTypeDropDown setTag:8888];
    //    arr = [NSArray arrayWithObjects:@"Hello 0", @"Hello 1", @"Hello 2", @"Hello 3", @"Hello 4", @"Hello 5", @"Hello 6", @"Hello 7", @"Hello 8", @"Hello 9",nil];
    if(self.rentTypeDropDown == nil) {
        CGFloat f = 200;
       self.rentTypeDropDown = [[NIDropDown alloc]showDropDown:sender withHeight:&f andInfo:self.rentTypeInfoArr];
        [self.rentTypeDropDown setTag:8888];
        self.rentTypeDropDown.delegate = self;
    }
    else {
        [self.rentTypeDropDown hideDropDown:sender];
        [self rel:self.rentTypeDropDown];
    }
}


//如果是租房种类，加入租房类型和选择区域
-(void)generateCatgeory1Component{
    CGRect rentTypeFrame = CGRectMake(10, self.lastUIFrame.origin.y+self.lastUIFrame.size.height+3, 50, 30);
    //rent Type
    if (!self.rentTypeButton) {
        //category row
//        self.lastUIFrame = rentTypeFrame;
        UILabel *rentLabel = [[UILabel alloc]initWithFrame:rentTypeFrame];
        rentLabel.text = @"租房种类";
        [rentLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
        
        [rentLabel setTag:7777];
        
        self.rentTypeButton = [[UIButton alloc]initWithFrame:CGRectMake(rentTypeFrame.origin.x+rentTypeFrame.size.width+MARGIN, rentTypeFrame.origin.y, 250, rentTypeFrame.size.height)];
        [self.rentTypeButton setBackgroundColor:FIELDBACKGROUNDCOLOR];
        
        [self.containerView addSubview:self.rentTypeButton];
        [self.containerView addSubview:rentLabel];
        [self.rentTypeButton release];
        [rentLabel release];
        
        
        
        [self.rentTypeButton setTitle:@"请选择..." forState:UIControlStateNormal];
        [self.rentTypeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.rentTypeButton addTarget:self action:@selector(rentTypeSelect:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *requiredImageView6 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
        [requiredImageView6 setFrame:CGRectMake(rentTypeFrame.origin.x+rentTypeFrame.size.width+2, rentTypeFrame.origin.y+3, 5, 5)];
        [self.containerView addSubview:requiredImageView6];
        [requiredImageView6 release];

        [requiredImageView6 setTag:9090];
        
        
        //category data:
    }
    if (self.rentTypeButton) {
        [self.rentTypeButton setHidden:NO];
        UILabel *rentLabel = (UILabel *)[self.containerView viewWithTag:7777];
        [rentLabel setHidden:NO];
    }
    
    
    if (!self.surburSelectionButton) {
        //category row
        CGRect surburFrame = CGRectMake(10, rentTypeFrame.origin.y+rentTypeFrame.size.height+3, 50, 30);
        UILabel *surburLabel = [[UILabel alloc]initWithFrame:surburFrame];
        surburLabel.text = @"地区";
        [surburLabel setBackgroundColor:FIELDBACKGROUNDCOLOR];
        
        [surburLabel setTag:6666];
        
        self.surburSelectionButton = [[UIButton alloc]initWithFrame:CGRectMake(surburFrame.origin.x+surburFrame.size.width+MARGIN, surburFrame.origin.y, 250, surburFrame.size.height)];
        [self.surburSelectionButton setBackgroundColor:FIELDBACKGROUNDCOLOR];
        
        [self.containerView addSubview:self.surburSelectionButton];
        [self.containerView addSubview:surburLabel];
        [self.surburSelectionButton release];
        [surburLabel release];
        
        
        [self.surburSelectionButton setTitle:@"请选择..." forState:UIControlStateNormal];
        [self.surburSelectionButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.surburSelectionButton addTarget:self action:@selector(suburbSelection:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIImageView *requiredImageView7 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"RequiredField.png"]];
        [requiredImageView7 setFrame:CGRectMake(surburFrame.origin.x+surburFrame.size.width+2, surburFrame.origin.y+3, 5, 5)];
        [self.containerView addSubview:requiredImageView7];
        [requiredImageView7 release];
        
        [requiredImageView7 setTag:9091];
        //category data:
    }
    if (self.rentTypeButton) {
        [self.rentTypeButton setHidden:NO];
        UILabel *rentLabel = (UILabel *)[self.containerView viewWithTag:7777];
        [rentLabel setHidden:NO];
        
        UIImageView *requireField = (UIImageView *)[self.containerView viewWithTag:9090];
        [requireField setHidden:NO];

    }
    
    if (self.surburSelectionButton) {
        [self.surburSelectionButton setHidden:NO];
        UILabel *suburbLabel = (UILabel *)[self.containerView viewWithTag:6666];
        [suburbLabel setHidden:NO];
        
        UIImageView *requireField = (UIImageView *)[self.containerView viewWithTag:9091];
        [requireField setHidden:NO];
    }
    
    //重新定位detailLabel和textfield
    UILabel *detailLabel = (UILabel *)[self.containerView viewWithTag:5555];
    [detailLabel setOrigin:CGPointMake(detailLabel.frame.origin.x, self.surburSelectionButton.frame.origin.y+self.surburSelectionButton.frame.size.height+3)];
    
    [self.detailTextField setOrigin:CGPointMake(self.detailTextField.frame.origin.x, self.surburSelectionButton.frame.origin.y+self.surburSelectionButton.frame.size.height+3)];
    
    
    UIImageView *requredViewForDetail = (UIImageView *)[self.containerView viewWithTag:5556];
//    detailFrame.origin.x+detailFrame.size.width+2
    [requredViewForDetail setOrigin:CGPointMake(requredViewForDetail.frame.origin.x, self.surburSelectionButton.frame.origin.y+self.surburSelectionButton.frame.size.height+3)];
}



-(void)suburbSelection:(id)sender{
    SuburbAreaSelectionsViewController *suburbAreaSelectionsViewController = [[SuburbAreaSelectionsViewController alloc]initWithNibName:@"SuburbAreaSelectionsViewController" bundle:nil];
    suburbAreaSelectionsViewController.suburbDelegate = self;
    [self presentViewController:suburbAreaSelectionsViewController animated:YES completion:nil];
    
}


-(void)selectSurburb:(id)sender{
    if (sender) {
        [self.surburSelectionButton setTitle:sender forState:UIControlStateNormal];
    }
}


//the sender here is the dropdown
-(void)niDropDownDelegateMethod:(NIDropDown *)sender andDic:(NSDictionary *)dic{
    if (sender.tag == 9999) {
        NSLog(@"dic....%@",dic);
        if ([(NSNumber *)[dic objectForKey:@"catid"] integerValue] == 1) {
            //tag 9999 表示选择租房
            [self generateCatgeory1Component];
        }else{
            if (self.rentTypeButton) {
                [self.rentTypeButton setHidden:YES];
                UILabel *rentLabel = (UILabel *)[self.containerView viewWithTag:7777];
                [rentLabel setHidden:YES];
                
                UIImageView *requireField = (UIImageView *)[self.containerView viewWithTag:9090];
                [requireField setHidden:YES];
                
            }
            if (self.surburSelectionButton) {
                [self.surburSelectionButton setHidden:YES];
                UILabel *suburbLabel = (UILabel *)[self.containerView viewWithTag:6666];
                [suburbLabel setHidden:YES];
                
                UIImageView *requireField = (UIImageView *)[self.containerView viewWithTag:9091];
                [requireField setHidden:YES];
            }
          
        //如果不是租房，重新把位置调回来
            UILabel *detailLabel = (UILabel *)[self.containerView viewWithTag:5555];
            [detailLabel setOrigin:CGPointMake(detailLabel.frame.origin.x, self.lastUIFrame.origin.y+self.lastUIFrame.size.height+3)];
            [self.detailTextField setOrigin:CGPointMake(self.detailTextField.frame.origin.x, self.lastUIFrame.origin.y+self.lastUIFrame.size.height+3)];
            
            
            UIImageView *requredViewForDetail = (UIImageView *)[self.containerView viewWithTag:5556];
            //    detailFrame.origin.x+detailFrame.size.width+2
            [requredViewForDetail setOrigin:CGPointMake(requredViewForDetail.frame.origin.x, self.lastUIFrame.origin.y+self.lastUIFrame.size.height+3)];
            
        }
        self.catId = [(NSNumber *)[dic objectForKey:@"catid"] integerValue];
        
        [self.cateoryDropDown release];
        self.cateoryDropDown = nil;
    }else if (sender.tag == 8888){
        
        self.rentType = [(NSNumber *)[dic objectForKey:@"renttype"] integerValue];
        [self.rentTypeDropDown release];
        self.rentTypeDropDown = nil;
    }
}



-(void)rel:(id)sender{
    NIDropDown *dropdown = (NIDropDown *)sender;
    if (dropdown.tag == 9999) {
        [self.cateoryDropDown release];
        self.cateoryDropDown = nil;
    }

    if (dropdown.tag == 8888) {
        [self.rentTypeDropDown release];
        self.rentTypeDropDown = nil;
    }

}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self.containerView adjustOffsetToIdealIfNeeded];
}




- (void)sendPost
{
    
    if (self.titleTextField.text.length == 0 || self.detailTextField.text.length == 0 || self.priceTextField.text.length == 0
        || self.contactTextField.text.length == 0  || self.emailTextField.text.length == 0  || self.passwordTextField.text.length == 0  || self.telephoneTextField.text.length == 0  || (self.catId < 0)
        ) {
        
        if (self.catId == 1 && ([self.surburSelectionButton.titleLabel.text isEqualToString:@"请选择..."] || self.rentType < 0)) {
            [WCAlertView showAlertWithTitle:@"Notice"
                                    message:@"Please fill in the required fields."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                             
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            
        }else{
            [WCAlertView showAlertWithTitle:@"Notice"
                                    message:@"Please fill in the required fields."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                             
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            
        }
        return;
    }

    
    
    ASIFormDataRequest *formRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/category/classifieds?json&newad",Host_address]]];
    
       if (self.photos) {
        NSLog(@"photos:%@",self.photos);
        for (int i=0; i<[self.photos  count]; i++) {
            MWPhoto *photo = [self.photos objectAtIndex:i];
            
            UIImage *img = photo.underlyingImage;
            
             NSData *imgData = UIImagePNGRepresentation([UIImage reSizeImage:img toSize:CGSizeMake(320, 480) keepRatio:YES]);
            
//            [imgData writeToFile:[self GetTempPath:[NSString stringWithFormat:@"%d.png",i+1]] atomically:NO];
            
            
//            [formRequest addData:imgData withFileName:[NSString stringWithFormat:@"%d.png",i+1] andContentType:@"image/png" forKey:@"file"];
            
            
             [formRequest addData:imgData withFileName:[NSString stringWithFormat:@"%d.png",i+1] andContentType:@"png" forKey:@"postImage[]"];
            
//            [formRequest addFile:[self GetTempPath:[NSString stringWithFormat:@"%d.png",i+1]] forKey:@"postImage[]"];
        }
    }
    NSLog(@"self.title.text:%@    %@      %@",self.titleTextField.text,[NSNumber numberWithInt:self.rentType],[NSNumber numberWithInt:self.catId]);
    
    
    [formRequest setPostValue:self.titleTextField.text forKey:@"postTitle"];
    [formRequest setPostValue:self.detailTextField.text forKey:@"postText"];
    [formRequest setPostValue:self.priceTextField.text forKey:@"postPrice"];
    [formRequest setPostValue:self.contactTextField.text forKey:@"postContact"];
    [formRequest setPostValue:self.addressTextField.text forKey:@"postAddress"];
    [formRequest setPostValue:[NSNumber numberWithInt:self.rentType] forKey:@"postRentType"];
    [formRequest setPostValue:self.surburSelectionButton.titleLabel.text forKey:@"postSuburb"];
    [formRequest setPostValue:[NSNumber numberWithInt:self.catId] forKey:@"postCatID"];
     [formRequest setPostValue:self.emailTextField.text forKey:@"postEmail"];
     [formRequest setPostValue:self.passwordTextField.text forKey:@"postPassword"];
     [formRequest setPostValue:self.telephoneTextField.text forKey:@"postPhone"];
     [formRequest setPostValue:@"post" forKey:@"action"];
    
    
    
    
    formRequest.postFormat = ASIMultipartFormDataPostFormat;
    
//    formRequest.shouldUseRFC2616RedirectBehaviour = YES;
    
    
    
//    MWPhoto *photo = [self.photos objectAtIndex:0];
//    UIImage *img = photo.underlyingImage;
//    NSData *imgData = UIImagePNGRepresentation([UIImage reSizeImage:img toSize:CGSizeMake(320, 480) keepRatio:YES]);
//
    

    
    
    [formRequest setTimeOutSeconds:3000];
     formRequest.delegate = self;
    
    
    [formRequest startAsynchronous];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Posting...";
    [hud show:YES];

    
}



-(NSString *)GetTempPath:(NSString*)filename{
    NSString *tempPath = NSTemporaryDirectory();
    
    NSFileManager *fileManeger=[NSFileManager defaultManager];
    
   Boolean fileThere =    [fileManeger fileExistsAtPath: [tempPath stringByAppendingPathComponent:filename]];
    if (fileThere) {
        NSLog(@"there");
        return [tempPath stringByAppendingPathComponent:filename];
    }
    return NULL;

}


- (void)requestStarted:(ASIHTTPRequest *)request {
//     [MBProgressHUD hideHUDForView:self.view animated:NO];
    NSString *result = [[NSString alloc] initWithData:request.postBody encoding:NSUTF8StringEncoding];
    NSLog(@"Start Upload:%@[%@]",request.requestHeaders,result);
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"RESPONSE:%@",[request responseString]);
    if ([request responseString]) {
        
        NSString *responseString = [request responseString];
        
        NSArray *responseData = [responseString mutableObjectFromJSONString];
        
        NSString *item_id = [responseData objectAtIndex:1];
        
        if (item_id) {
            NSMutableDictionary *myPost = [[NSMutableDictionary alloc]initWithCapacity:1];
            [myPost setValue:item_id forKey:@"item_id"];
            
            if (self.addressTextField.text.length == 0) {
                [myPost setValue:@"未注明" forKey:@"address"];
            }else
                [myPost setValue:self.addressTextField.text forKey:@"address"];
            
            
            NSString *url_item = [NSString stringWithFormat:@"http://www.insyd.com.au/classifieds/ad/%d/%@?format=json",self.catId,item_id];
            [myPost setValue:url_item forKey:@"item_url"];
            
            
            [myPost setValue:self.titleTextField.text forKey:@"title"];
            [myPost setValue:[NSString stringWithFormat:@"%d",self.catId] forKey:@"catId"];
            
            [UtilDB saveMyPost:myPost];
            
            [WCAlertView showAlertWithTitle:@"Notice" message:@"Posting done, you can see your post in 'My Post'."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}




- (void)requestFailed:(ASIHTTPRequest *)request {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [WCAlertView showAlertWithTitle:@"Notice"
                            message:@"Posting failed, please try later.."
                 customizationBlock:^(WCAlertView *alertView) {
                     alertView.style = WCAlertViewStyleVioletHatched;
                     
                 } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                     
                 } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Photowall Delegate methods
- (void)photoWallPhotoTaped:(NSUInteger)index
{
    self.tagWhichIndex = index;
    UIActionSheet *actionSheetTemp = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:@"删除图片"
                                                        otherButtonTitles:@"看大图", nil];
    [actionSheetTemp showInView:self.view];
    
    [actionSheetTemp release];
}

- (void)photoWallMovePhotoFromIndex:(NSInteger)index toIndex:(NSInteger)newIndex
{
    
}

- (void)photoWallAddAction
{
//    [self.photoWall addPhoto:@"http://cc.cocimg.com/bbs/attachment/upload/92/1226921340986127.jpg"];
//    MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:@"http://farm4.static.flickr.com/3629/3339128908_7aecabc34b_b.jpg"]];
//    if (!self.photos) {
//        self.photos = [[NSMutableArray alloc]initWithCapacity:1];
//    }
//    [self.photos addObject:photo];
//    NSLog(@"self.photos %@",self.photos);
    
    
    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    
    imagePickerController.limitsMaximumNumberOfSelection = YES;
    imagePickerController.limitsMinimumNumberOfSelection = YES;
    imagePickerController.minimumNumberOfSelection = 1;
    imagePickerController.maximumNumberOfSelection = 4;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [imagePickerController release];
    [navigationController release];
    
}

- (void)photoWallAddFinish
{
    
}

- (void)photoWallDeleteFinish
{
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.photoWall deletePhotoByIndex:self.tagWhichIndex];
        [self.photos removeObjectAtIndex:self.tagWhichIndex];
    }else if (buttonIndex == actionSheet.firstOtherButtonIndex){
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        browser.displayActionButton = YES;
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
        nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:nc animated:YES completion:nil];
        [nc release];
        [browser release];
//        [self.photos release];
    }
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}




#pragma mark - QBImagePickerControllerDelegate

- (void)imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingMediaWithInfo:(id)info
{
    if(imagePickerController.allowsMultipleSelection) {
        if(!self.photos){
            self.photos = [[NSMutableArray alloc]initWithCapacity:1];
        }
        NSArray *mediaInfoArray = (NSArray *)info;
        for (NSDictionary *mediaInfo in mediaInfoArray) {
            NSLog(@"mediaInfo  %@",mediaInfo );
            UIImage *image = [mediaInfo objectForKey:@"UIImagePickerControllerOriginalImage"];
            MWPhoto *photo = [MWPhoto photoWithImage:image];
            
            [self.photos addObject:photo];
            [self.photoWall addLocalPhoto:image];
        }
        NSLog(@"Selected %d photos", mediaInfoArray.count);
    } else {
//        NSDictionary *mediaInfo = (NSDictionary *)info;
//        UIImage *image = [mediaInfo objectForKey:@"UIImagePickerControllerOriginalImage"];
//        
//        UIImage *newImage = [UtilTools imageWithImageSimple:image scaledToSize:CGSizeMake(120.0, 120.0)];
//        
//        [UtilTools saveImage:newImage WithName:@"test.png"];
//        UIImageView  *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(100, 200, 120, 120)];
//        [imageView setImage:newImage];
//        
//        [self.view addSubview:imageView];
//        [imageView release];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Cancelled");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString *)descriptionForSelectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"Select from photo Album...";
}

- (NSString *)descriptionForDeselectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"すべての写真の選択を解除";
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos
{
    return [NSString stringWithFormat:@"You can select %d photos.",  MAXIMUMPHOTONUMBER +1 - [[self.photoWall getPhotosInPhotoWall] count]];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"ビデオ%d本", numberOfVideos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos numberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"写真%d枚、ビデオ%d本", numberOfPhotos, numberOfVideos];
}





@end
