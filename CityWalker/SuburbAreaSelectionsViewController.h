//
//  SuburbAreaSelectionsViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-19.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol suburbviewDeledate <NSObject>

@optional
-(void)selectSurburb:(id)sender;

@end
@interface SuburbAreaSelectionsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    
    UITableView *tableView;
    UISearchDisplayController *searchDC;
    UISearchBar *searchBar;
    

}

@property(nonatomic,retain)NSMutableArray *filteredArray;
@property(nonatomic,retain)NSMutableArray *dataSource;
@property(nonatomic,retain)NSMutableArray *sectionArray;
@property (retain) UISearchDisplayController *searchDC;
@property(assign,nonatomic)id<suburbviewDeledate> suburbDelegate;
@end
