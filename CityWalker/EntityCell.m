//
//  EntityCell.m
//  CityWalker
//
//  Created by Jianing Ren on 13-5-15.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "EntityCell.h"
#import "UIImageView+WebCache.h"

#import "EntityListViewController.h"
#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

#import <QuartzCore/QuartzCore.h>
@implementation EntityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}



-(void)initCell{
    CGRect frame = self.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
//    UIImageView *bgView = [[UIImageView alloc] initWithFrame:frame];
//    [bgView setImage:[UIImage imageNamed:@"bg.png"]];
//    [self setBackgroundView:bgView];
//    [bgView release];
    
    if (self.cellType==newsCell) {
        CGRect titleFrame = CGRectMake(10,-10,300 ,60);
        
        CGRect dateLabelFrame = CGRectMake(titleFrame.origin.x, titleFrame.origin.y+titleFrame.size.height, 100, 30);
        CGRect sourceLabelFrame = CGRectMake(250, dateLabelFrame.origin.y, 60, 30);
        if (!titleLabel) {
            //        coverImage = [[UIImageView alloc]initWithFrame:photoFrame];
            titleLabel = [[UILabel alloc]initWithFrame:titleFrame];
            [self addSubview:titleLabel];
            [titleLabel release];
        }
        else
        [titleLabel setFrame:titleFrame];
        
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:UITextAlignmentLeft];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        titleLabel.numberOfLines = 2;
        titleLabel.minimumFontSize = 10;
        
        
        if (!dateLabel) {
            dateLabel = [[UILabel alloc]initWithFrame:dateLabelFrame];
            [self addSubview:dateLabel];
            [dateLabel release];
        }else
            [dateLabel setFrame:dateLabelFrame];
        
        
        [dateLabel setBackgroundColor:[UIColor clearColor]];
        [dateLabel setTextAlignment:UITextAlignmentLeft];
        dateLabel.font = [UIFont boldSystemFontOfSize:12];
        dateLabel.numberOfLines = 1;
        
        
        if (!sourceLabel) {
            sourceLabel = [[UILabel alloc]initWithFrame:sourceLabelFrame];
            [self addSubview:sourceLabel];
            [sourceLabel release];
        }else
            [sourceLabel setFrame:sourceLabelFrame];
        
        
        [sourceLabel setBackgroundColor:[UIColor clearColor]];
        [sourceLabel setTextAlignment:UITextAlignmentLeft];
        sourceLabel.font = [UIFont boldSystemFontOfSize:12];
        sourceLabel.numberOfLines = 1;      
    }else if (self.cellType==photoCell){
        CGFloat scrollviewHeigh = self.frame.size.height;
        CGFloat scrollviewwidth = self.frame.size.width;
        CGRect scrollViewFrame = CGRectMake(self.frame.origin.x,self.frame.origin.y ,scrollviewwidth, scrollviewHeigh);
        if (!imageGallary) {
            imageGallary = [[UIScrollView  alloc]initWithFrame:scrollViewFrame];
            [self addSubview:imageGallary];
            [imageGallary release];
        }
        else
            [imageGallary setFrame:scrollViewFrame];
        
            [imageGallary setScrollEnabled:YES];
        [imageGallary setBackgroundColor:[UIColor blackColor]];
        [self bringSubviewToFront:imageGallary];
    
    }
}


- (void)animationForIndexPath:(NSIndexPath *)indexPath {
//	int row = indexPath.row;
//	float radians = (120 + row*30)%360;
//	radians = 20;
//	CALayer *layer = [[self.layer sublayers] objectAtIndex:0];
////    NSLog(@"layer:%@",layer);
//	// Rotation Animation
//	CABasicAnimation *animation  = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
//	animation.fromValue =@DEGREES_TO_RADIANS(radians);
//	animation.toValue = @DEGREES_TO_RADIANS(0);
//	
////    NSLog(@"animation.fromValue:%@",animation.fromValue);
////    
////    NSLog(@"animation.ToValue:%@",animation.toValue);
//
//    
//    
//	// Opacity Animation;
//	CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
//	fadeAnimation.fromValue = @0.1f;
//	fadeAnimation.toValue = @1.f;
//	
//	// Translation Animation
//	CABasicAnimation *translationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
//	translationAnimation.fromValue = @(-300.f * ((indexPath.row%2 == 0) ? -1: 1));
//	translationAnimation.toValue = @0.f;
//	
//	// Scale Animation
//	CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//	scaleAnimation.fromValue = @0.1f;
//	scaleAnimation.toValue = @1.f;
//	
//	CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
//	animationGroup.duration = 0.4f;
//	animationGroup.animations = @[animation,fadeAnimation,translationAnimation,scaleAnimation];
//	animationGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//	[layer addAnimation:animationGroup forKey:@"spinAnimation"];
}


//- (void)layoutSubviews {
//	[super layoutSubviews];
//	
//	CGRect workingRect = self.contentView.bounds;
//	workingRect = CGRectInset(workingRect, 2, 2);
//	CGRect textRect = CGRectZero, imageRect = CGRectZero, detailRect = CGRectZero;
//	
//
//    
//	if(self.imageView) {
//		CGRectDivide(workingRect, &imageRect, &workingRect, workingRect.size.height, CGRectMinXEdge);
//	}
//	if(self.detailTextLabel) {
//		CGRectDivide(workingRect, &detailRect, &workingRect, workingRect.size.height/2, CGRectMaxYEdge);
//	}
//	textRect = workingRect;
//	[self.imageView setFrame:imageRect];
//	[self.textLabel setFrame:textRect];
//	[self.detailTextLabel setFrame:detailRect];
//	
//	[self.textLabel setBackgroundColor:[UIColor greenColor]];
//	[self.detailTextLabel setBackgroundColor:[UIColor yellowColor]];
//}




- (void)initCellWithType:(CellType)type {
//    [self.backgroundView setBackgroundColor:[UIColor blueColor]];
//    [self setBackgroundColor:[UIColor blueColor]];
    
    
    
    
    
    CGRect frame = self.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
//     CGFloat img_h = self.frame.size.height;
     CGFloat img_w = self.frame.size.width;
    if(type == newsCell) {
        
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:frame];
        [bgView setImage:[UIImage imageNamed:@"news_background.png"]];
        [self setBackgroundView:bgView];
        [bgView release];
        
        
        
        CGRect titleFrame = CGRectMake(20,0,300 ,60);
        [self setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:0.9]];
        CGRect dateLabelFrame = CGRectMake(titleFrame.origin.x, titleFrame.origin.y+titleFrame.size.height-10, 150, 30);
        CGRect sourceLabelFrame = CGRectMake(220, dateLabelFrame.origin.y, 100, 30);
        if (!titleLabel) {
            //        coverImage = [[UIImageView alloc]initWithFrame:photoFrame];
            titleLabel = [[UILabel alloc]initWithFrame:titleFrame];
            [self addSubview:titleLabel];
            [titleLabel release];
        }
        else
            [titleLabel setFrame:titleFrame];
        
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:UITextAlignmentLeft];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        titleLabel.numberOfLines = 3;
        titleLabel.minimumFontSize = 10;
        
        
        CGRect unreadTagFrame = CGRectMake(10,27,10,10);

        
        if (!unreadTag) {
            unreadTag = [[UIImageView alloc]initWithFrame:unreadTagFrame];
            [self addSubview:unreadTag];
            [unreadTag release];
        }
        else
            [unreadTag setFrame:unreadTagFrame];
        
        
  
        
        
        
        if (!dateLabel) {
            dateLabel = [[UILabel alloc]initWithFrame:dateLabelFrame];
            [self addSubview:dateLabel];
            [dateLabel release];
        }else
            [dateLabel setFrame:dateLabelFrame];
        
        
        [dateLabel setBackgroundColor:[UIColor clearColor]];
        [dateLabel setTextAlignment:UITextAlignmentLeft];
        dateLabel.font = [UIFont boldSystemFontOfSize:12];
        dateLabel.numberOfLines = 1;
        
        
        if (!sourceLabel) {
            sourceLabel = [[UILabel alloc]initWithFrame:sourceLabelFrame];
            [self addSubview:sourceLabel];
            [sourceLabel release];
        }else
            [sourceLabel setFrame:sourceLabelFrame];
        
        [sourceLabel setBackgroundColor:[UIColor clearColor]];
        [sourceLabel setTextAlignment:UITextAlignmentLeft];
        sourceLabel.font = [UIFont boldSystemFontOfSize:12];
        sourceLabel.numberOfLines = 1;
    }
    
    else if(type == photoCell) {
//        CGFloat scrollviewHeigh = self.frame.size.height; 
        CGFloat scrollviewwidth = self.frame.size.width;
//        RECTLog(self.frame);
        NSLog(@"self.cell is %@",self);
        CGRect scrollViewFrame = CGRectMake(self.frame.origin.x,self.frame.origin.y ,scrollviewwidth, 180);
        if (!imageGallary) {
            imageGallary = [[UIScrollView  alloc]initWithFrame:scrollViewFrame];
            [self addSubview:imageGallary];
            [imageGallary release];
        }
        else
            [imageGallary setFrame:scrollViewFrame];
        
        
        CGRect pageControllerFrame = CGRectMake(250, 150, 50, 30);
        if (!pageController) {
            pageController = [[UIPageControl alloc]initWithFrame:pageControllerFrame];
            [self addSubview:pageController];
            [pageController release];
        }
        else
            [pageController setFrame:pageControllerFrame];
        
        pageController.numberOfPages = 1;
        pageController.currentPage = 0;
        [pageController setBackgroundColor:[UIColor clearColor]];
        
        imageGallary.delegate = self;
        imageGallary.showsHorizontalScrollIndicator = FALSE;
        [imageGallary setScrollEnabled:YES];
        [imageGallary setPagingEnabled:YES];
        [imageGallary setBackgroundColor:[UIColor blackColor]];
        
//        [self bringSubviewToFront:imageGallary];
    }
    
    else if (type==subjectCell){

        CGRect coverFrame = CGRectMake(0, 0, 320, 218);        
        if (!coverImageView) {
            coverImageView = [[UIImageView alloc]initWithFrame:coverFrame];
            [self addSubview:coverImageView];
            [coverImageView release];
        }
        else
            [coverImageView setFrame:coverFrame];
        [coverImageView setUserInteractionEnabled:NO];
        
        
        CGRect titleFrame = CGRectMake(3, 182, 320, 40);
        
        if (!titleLabel) {
            titleLabel = [[UILabel alloc]initWithFrame:titleFrame];
           
            [coverImageView addSubview:titleLabel];
            [titleLabel release];
        }
        else
        [titleLabel setFrame:titleFrame];
        [titleLabel setUserInteractionEnabled:NO];
         [titleLabel setBackgroundColor:[UIColor blackColor]];
        titleLabel.alpha = 0.4;
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel setTextAlignment:UITextAlignmentLeft];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        titleLabel.numberOfLines = 2;
        titleLabel.minimumFontSize = 10;

        
        CGRect subectTagViewFrame = CGRectMake(img_w - 50,5, 40, 40);
        
        
        if (!subjectTypeTagImageView) {
            subjectTypeTagImageView = [[UIImageView alloc]initWithFrame:subectTagViewFrame];
            [coverImageView addSubview:subjectTypeTagImageView];
                        [subjectTypeTagImageView release];
        }
        else
            [subjectTypeTagImageView setFrame:subectTagViewFrame];
        
        [subjectTypeTagImageView setBackgroundColor:[UIColor clearColor]];
        
    }
    
    
    else if (type==categoryMainCell){
        
        [self setBackgroundColor:[UIColor darkGrayColor]];
        
        CGRect coverFrame = CGRectMake(10, 5, 50, 60);
        if (!coverImageView) {
            coverImageView = [[UIImageView alloc]initWithFrame:coverFrame];
            [self addSubview:coverImageView];
            [coverImageView release];
        }
        else
            [coverImageView setFrame:coverFrame];
        [coverImageView setUserInteractionEnabled:NO];
        
        
        CGRect titleFrame = CGRectMake(70, 10, 100, 40);
        
        if (!titleLabel) {
            titleLabel = [[UILabel alloc]initWithFrame:titleFrame];
            
            [coverImageView addSubview:titleLabel];
            [titleLabel release];
        }
        else
            [titleLabel setFrame:titleFrame];
        [titleLabel setUserInteractionEnabled:NO];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        titleLabel.alpha = 0.4;
        titleLabel.textColor = [UIColor blackColor];
        [titleLabel setTextAlignment:UITextAlignmentLeft];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        titleLabel.numberOfLines = 2;
        titleLabel.minimumFontSize = 12;
        
        
//        CGRect subectTagViewFrame = CGRectMake(img_w - 50,5, 40, 40);
        
        
//        if (!subjectTypeTagImageView) {
//            subjectTypeTagImageView = [[UIImageView alloc]initWithFrame:subectTagViewFrame];
//            [coverImageView addSubview:subjectTypeTagImageView];
//            [subjectTypeTagImageView release];
//        }
//        else
//            [subjectTypeTagImageView setFrame:subectTagViewFrame];
//        
//        [subjectTypeTagImageView setBackgroundColor:[UIColor clearColor]];
        
    }
    
    
    
    else if (type==rateCell){
        
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:frame];
        [bgView setImage:[UIImage imageNamed:@"public_bar.png"]];
        [self setBackgroundView:bgView];
        [bgView release];

        
        
        
        CGRect coverFrame = CGRectMake(5, 5, 100, 80);
        if (!coverImageView) {
            coverImageView = [[UIImageView alloc]initWithFrame:coverFrame];
            [self addSubview:coverImageView];
            [coverImageView release];
        }
        else
            [coverImageView setFrame:coverFrame];
        [coverImageView setUserInteractionEnabled:NO];
        
        
        
        
        CGRect titleFrame = CGRectMake(coverFrame.origin.x+coverFrame.size.width+3, coverFrame.origin.y, 200, 20);
        
        if (!titleLabel) {
            titleLabel = [[UILabel alloc]initWithFrame:titleFrame];
            
            [self addSubview:titleLabel];
            [titleLabel release];
        }
        else
            [titleLabel setFrame:titleFrame];
        [titleLabel setUserInteractionEnabled:NO];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        titleLabel.textColor = [UIColor redColor];
        [titleLabel setTextAlignment:UITextAlignmentLeft];
        titleLabel.font = [UIFont boldSystemFontOfSize:12];
    
        
        CGRect areatitleFram = CGRectMake(titleFrame.origin.x, titleFrame.origin.y+titleFrame.size.height+3, 30, 20);
        
        if (!areaTitleLabel) {
            areaTitleLabel = [[UILabel alloc]initWithFrame:areatitleFram];
            
            [self addSubview:areaTitleLabel];
            [areaTitleLabel release];
        }
        else
            [areaTitleLabel setFrame:areatitleFram];
        [areaTitleLabel setUserInteractionEnabled:NO];
        [areaTitleLabel setBackgroundColor:[UIColor clearColor]];
        areaTitleLabel.textColor = [UIColor redColor];
        [areaTitleLabel setTextAlignment:UITextAlignmentLeft];
        areaTitleLabel.font = [UIFont boldSystemFontOfSize:10];

        areaTitleLabel.text = @"地区  ";
        
        CGRect areaFrame = CGRectMake(areatitleFram.origin.x+areatitleFram.size.width, titleFrame.origin.y+titleFrame.size.height+3, 30, 20);
        
        if (!areaLabel) {
            areaLabel = [[UILabel alloc]initWithFrame:areaFrame];
            
            [self addSubview:areaLabel];
            [areaLabel release];
        }
        else
            [areaLabel setFrame:areaFrame];
        [areaLabel setUserInteractionEnabled:NO];
        [areaLabel setBackgroundColor:[UIColor clearColor]];
        areaLabel.textColor = [UIColor blackColor];
        [areaLabel setTextAlignment:UITextAlignmentLeft];
        areaLabel.font = [UIFont boldSystemFontOfSize:10];
        
        
        
        CGRect typetitleFrame = CGRectMake(areatitleFram.origin.x+areatitleFram.size.width+areaFrame.size.width, areaFrame.origin.y, 30, 20);
        
        if (!typedTitleLabel) {
            typedTitleLabel = [[UILabel alloc]initWithFrame:typetitleFrame];
            
            [self addSubview:typedTitleLabel];
            [typedTitleLabel release];
        }
        else
            [typedTitleLabel setFrame:typetitleFrame];
        [typedTitleLabel setUserInteractionEnabled:NO];
        [typedTitleLabel setBackgroundColor:[UIColor clearColor]];
        typedTitleLabel.textColor = [UIColor redColor];
        [typedTitleLabel setTextAlignment:UITextAlignmentLeft];
        typedTitleLabel.font = [UIFont boldSystemFontOfSize:10];
        
        typedTitleLabel.text = @"菜系 ";
        
        
        CGRect typeFrame = CGRectMake(typetitleFrame.origin.x+typetitleFrame.size.width, areaFrame.origin.y, 45, 20);
        
        if (!typedLabel) {
            typedLabel = [[UILabel alloc]initWithFrame:typeFrame];
            
            [self addSubview:typedLabel];
            [typedLabel release];
        }
        else
            [typedLabel setFrame:typeFrame];
        [typedLabel setUserInteractionEnabled:NO];
        [typedLabel setBackgroundColor:[UIColor clearColor]];
        typedLabel.textColor = [UIColor blackColor];
        [typedLabel setTextAlignment:UITextAlignmentLeft];
        typedLabel.font = [UIFont boldSystemFontOfSize:10];

        
        
        
        CGRect averageTitleFrame = CGRectMake(typeFrame.origin.x+typeFrame.size.width, typeFrame.origin.y, 30, 20);
        
        if (!averageTitleLabel) {
            averageTitleLabel = [[UILabel alloc]initWithFrame:averageTitleFrame];
            
            [self addSubview:averageTitleLabel];
            [averageTitleLabel release];
        }
        else
            [averageTitleLabel setFrame:averageTitleFrame];
        [averageTitleLabel setUserInteractionEnabled:NO];
        [averageTitleLabel setBackgroundColor:[UIColor clearColor]];
        averageTitleLabel.textColor = [UIColor redColor];
        [averageTitleLabel setTextAlignment:UITextAlignmentLeft];
        averageTitleLabel.font = [UIFont boldSystemFontOfSize:10];

        averageTitleLabel.text = @"价钱 ";
        
        
        CGRect averageFrame = CGRectMake(averageTitleFrame.origin.x+averageTitleFrame.size.width, typeFrame.origin.y, 60, 20);
        
        if (!averageLabel) {
            averageLabel = [[UILabel alloc]initWithFrame:averageFrame];
            
            [self addSubview:averageLabel];
            [averageLabel release];
        }
        else
            [averageLabel setFrame:averageFrame];
        [averageLabel setUserInteractionEnabled:NO];
        [averageLabel setBackgroundColor:[UIColor clearColor]];
        averageLabel.textColor = [UIColor blackColor];
        [averageLabel setTextAlignment:UITextAlignmentLeft];
        averageLabel.font = [UIFont boldSystemFontOfSize:10];

        
        CGRect phoneTitleFrame = CGRectMake(areatitleFram.origin.x, areatitleFram.origin.y+areatitleFram.size.height, 30, 30);
        
        if (!phoneTitleLabel) {
            phoneTitleLabel = [[UILabel alloc]initWithFrame:phoneTitleFrame];
            
            [self addSubview:phoneTitleLabel];
            [phoneTitleLabel release];
        }
        else
            [phoneTitleLabel setFrame:phoneTitleFrame];
        [phoneTitleLabel setUserInteractionEnabled:NO];
        [phoneTitleLabel setBackgroundColor:[UIColor clearColor]];
        phoneTitleLabel.textColor = [UIColor redColor];
        [phoneTitleLabel setTextAlignment:UITextAlignmentLeft];
        phoneTitleLabel.font = [UIFont boldSystemFontOfSize:12];

        phoneTitleLabel.text = @"电话 ";
        
        
        CGRect phoneFrame = CGRectMake(areaFrame.origin.x, areaFrame.origin.y+areaFrame.size.height, 110, 30);
        
        if (!phoneLabel) {
            phoneLabel = [[UILabel alloc]initWithFrame:phoneFrame];
            
            [self addSubview:phoneLabel];
            [phoneLabel release];
        }
        else
            [phoneLabel setFrame:phoneFrame];
        [phoneLabel setUserInteractionEnabled:NO];
        [phoneLabel setBackgroundColor:[UIColor clearColor]];
        phoneLabel.textColor = [UIColor blackColor];
        [phoneLabel setTextAlignment:UITextAlignmentLeft];
        phoneLabel.font = [UIFont boldSystemFontOfSize:12];

        
    }
}







//subjects
- (void)setSubjectTagPhoto:(UIImage *)tagImage withType:(NSString *)type{
    if ([type isEqualToString:@"life"]) {
        [subjectTypeTagImageView setImage:[UIImage imageNamed:@"column-icon-life.png"]];
    }
    else if ([type isEqualToString:@"food"]){
         [subjectTypeTagImageView setImage:[UIImage imageNamed:@"column-icon-food.png"]];
    }
}




-(void)setFrameToImageView{
    CALayer *layer = [coverImageView layer];
    layer.borderColor = [UIColor whiteColor].CGColor;
    layer.borderWidth = 5.0f;
    //添加四个边阴影
    coverImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    coverImageView.layer.shadowOffset = CGSizeMake(4,4);
    coverImageView.layer.shadowOpacity = 0.5;
    coverImageView.layer.shadowRadius = 2.0;//给imageview添加阴影和边框
    //添加两个边的阴影
//    coverImageView.layer.shadowColor = [UIColor redColor].CGColor;
//    coverImageView.layer.shadowOffset = CGSizeMake(4,4);
//    coverImageView.layer.shadowOpacity = 0.5;
//    coverImageView.layer.shadowRadius=2.0;
}

- (void)setSubjectCover:(NSString *)url {
    NSURL *URL = [NSURL URLWithString:url];
//    [coverImageView setImageWithURL:URL];
    [coverImageView setImageWithURL:URL placeholderImage:nil options:0];
}


- (void)setCategoryMainIcon:(NSString *)photoName{
    [coverImageView setImage:[UIImage imageNamed:photoName]];
}



//news
-(void)setTitle:(NSString *)title{
    titleLabel.text = title;
}

-(void)setDateLabel:(NSString *)date{
    dateLabel.text = date;

}
-(void)setSourceLabel:(NSString *)source{
    if ((id)source != [NSNull null]){
        sourceLabel.text = source;
    }
}



//Category
-(void)setTitleForCategory:(NSString *)title{
    titleLabel.text = title;
}

-(void)setLocation:(NSString *)source{
    CGRect locationFrame = CGRectMake(15, 50, 300, 20);
    [sourceLabel setFrame:locationFrame];
    [sourceLabel setTextColor:[UIColor blackColor]];
    sourceLabel.text = source;
}



//Public comments
-(void)setAreaLabel:(NSString *)area{

    areaLabel.text = area;
    
//    [areaLabel setColor:[UIColor redColor] fromIndex:0 length:2];
//    [areaLabel setColor:[UIColor blackColor] fromIndex:3 length:area.length];
//    
//    
//    [areaLabel setFont:[UIFont boldSystemFontOfSize:12] fromIndex:0 length:areaLabel.text.length];

    
}

-(void)setTypedLabel:(NSString *)type{
    typedLabel.text = type;
    
//    [typedLabel setColor:[UIColor redColor] fromIndex:0 length:2];
//    [typedLabel setColor:[UIColor blackColor] fromIndex:3 length:type.length];
//    
//    
//    [typedLabel setFont:[UIFont boldSystemFontOfSize:12] fromIndex:0 length:typedLabel.text.length];
    
}

-(void)setAverageLabel:(NSString *)average{
    averageLabel.text = average;
    
//    [averageLabel setColor:[UIColor redColor] fromIndex:0 length:2];
//    [averageLabel setColor:[UIColor blackColor] fromIndex:3 length:average.length];
//    
//    
//    [averageLabel setFont:[UIFont boldSystemFontOfSize:12] fromIndex:0 length:averageLabel.text.length];
}

-(void)setphoneLabel:(NSString *)phone{
    phoneLabel.text = phone;
    
//    [phoneLabel setColor:[UIColor redColor] fromIndex:0 length:2];
//    [phoneLabel setColor:[UIColor blackColor] fromIndex:3 length:phone.length];
//    
//    
//    [phoneLabel setFont:[UIFont boldSystemFontOfSize:12] fromIndex:0 length:phoneLabel.text.length];
}



-(void)removeunReadTag:(UIImageView *)tagImage{
    if (unreadTag) {
        
        [unreadTag setImage:nil];
        [unreadTag setBackgroundColor:[UIColor clearColor]];
        
//        [unreadTag retain];
        
//        [unreadTag removeFromSuperview];
        
    }
}


-(void)setUnreadTag{
    if (unreadTag) {
        [unreadTag setImage:[UIImage imageNamed:@"if_read.png"]];
        [unreadTag setUserInteractionEnabled:NO];
    }
}




-(UIScrollView *)getImageGallary{
    return imageGallary;
}



-(UIPageControl *)getpageController{
    return pageController;
}


-(void)setGallaryPhtos:(NSArray *)images{
    
    imageGallary.contentSize = CGSizeMake([images count]*320, 180.);
    pageController.numberOfPages = [images count];
//    [self updateDots];
    
    for (int i=0; i<[images count]; i++) {
        NSURL *imageURL = [NSURL URLWithString:[images objectAtIndex:i]];
        //UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
        //UIImageView *img = [[UIImageView alloc] initWithImage:image];
        UIImageView *img = [[UIImageView alloc] init];
        [img setImageWithURL:imageURL];
//        [img setImageWithURL:imageURL placeholderImage:nil options:nil];
        
        img.frame = CGRectMake(i*320, 0, 320, 180);
//        img.layer.borderColor = [[UIColor whiteColor]CGColor];
//        img.layer.borderWidth = 5.0f;
         CGRect captionViewFrame = CGRectMake(0, img.frame.size.height-40, 320, 40);
        UILabel *captionView = [[UILabel alloc]initWithFrame:captionViewFrame];
        [captionView setUserInteractionEnabled:NO];
        [captionView setBackgroundColor:[UIColor blackColor]];
        captionView.alpha = 0.4;
        captionView.textColor = [UIColor whiteColor];
        [captionView setTextAlignment:UITextAlignmentLeft];
        captionView.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:16];
        captionView.numberOfLines = 2;
        captionView.minimumFontSize = 10;

       
//        UILabel *captionTitle = [[UILabel alloc]initWithFrame:captionViewFrame];
//        [captionTitle setBackgroundColor:[UIColor clearColor]];
        captionView.text = @"小角落里的大发现";
//        captionTitle.textColor = [UIColor whiteColor];
        [img addSubview:captionView];
//        [img addSubview:captionTitle];
        [captionView release];
//        [captionTitle release];
        
        [imageGallary addSubview:img];
        
        [img release];
    }
}


- (void) updateDots
{
    if (pageController) {
        NSArray *subView = pageController.subviews;
        
        for (int i = 0; i < [subView count]; i++) {
            UIImageView *dot = [subView objectAtIndex:i];
//            dot.image = (self.currentPage == i ? imagePageStateHightlighted : imagePageStateNormal);
            dot.backgroundColor = [UIColor redColor];
            
        }
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    int page = imageGallary.contentOffset.x / 320;
    pageController.currentPage = page;
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
