//
//  PublicCommentsViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-6-2.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "ASIHTTPRequestDelegate.h"
#import "DemoTableController.h"
#import "NIDropDown.h"
@interface PublicCommentsViewController : RootViewController<popoverDeledate,NIDropDownDelegate>


{
    NSMutableArray *_dataSource;
    NSInteger _refreshCount;
    NSInteger _loadMoreCount;
    int currentPage;
    //detemine which gesture operation
    Boolean isrefreshOrloadMore;
    Boolean isfirstTimeRefresh;
    Boolean isfilterOption;
}


@property(nonatomic,retain)NSMutableArray *dataSource;
@property(nonatomic,retain)NSString *currentlyUrl;
@property(nonatomic,assign)NSMutableDictionary *filterDic;



@property(retain,nonatomic)NIDropDown *areaDropDown;
@property(retain,nonatomic)NIDropDown *cuizineDropDown;
@property(retain,nonatomic)NIDropDown *typedDropDown;

@end
