//
//  CalendarClass.h
//  Plumpton PS
//
//  Created by  on 11-11-1.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+expanded.h"
#import "NSMutableArray+convenience.h"
#import "UIView+convenience.h"
#import "UIImage+Size.h"
#import "UIImage+Tint.h"
#import "UIView+Size.h"


@interface ExtendHelper : NSObject {

}
+(void)setUserDefaultValue:(id)value forKey:(NSString *)key;
+(id)getUserDefaultValueForKey:(NSString *)key;

+(NSString *)getLocalTimeString:(NSString *)GMTTimeString;
+(NSString *)convertDateString:(NSString *)dateString toFormat:(NSString *)format;
+(NSString *)convertDateTimeString:(NSString *)dateTimeString toFormat:(NSString *)format;
+(NSString *)convertDateString:(NSString *)dateString fromFormat:(NSString *)format1 toFormat:(NSString *)format2;
+(UINavigationController *)getMainNav;
@end


@interface UIColor (HexColor) 
+ (UIColor *) colorWithHexString: (NSString *) stringToConvert withAlpha:(float)alpha;
@end
//@interface UINavigationItem (Custom)
//- (void)setTitle:(NSString *)title;
//@end
@interface UIDevice (BHI_Extensions)
+(NSString *)platform;
+(NSString*)platformString;
@end

@interface UIView (GradientView)
- (void)setGradientBackgroundColorFromColor:(UIColor *)startColor toColor:(UIColor *)endColor;
- (void)setGradientBackgroundColorWithColors:(NSArray *)colors;

@end

@interface NSData (Compression)

#pragma mark -
#pragma mark Zlib Compression routines
/*! Returns a data object containing a Zlib decompressed copy of the receivers contents.
 * \returns A data object containing a Zlib decompressed copy of the receivers contents.
 */
- (NSData *) zlibInflate;
/*! Returns a data object containing a Zlib compressed copy of the receivers contents.
 * \returns A data object containing a Zlib compressed copy of the receivers contents.
 */
- (NSData *) zlibDeflate;

#pragma mark -
#pragma mark Gzip Compression routines
/*! Returns a data object containing a Gzip decompressed copy of the receivers contents.
 * \returns A data object containing a Gzip decompressed copy of the receivers contents.
 */
- (NSData *) gzipInflate;
/*! Returns a data object containing a Gzip compressed copy of the receivers contents.
 * \returns A data object containing a Gzip compressed copy of the receivers contents.
 */
- (NSData *) gzipDeflate;

@end


@protocol UIViewControllerDelegate <NSObject>

@optional
- (void)viewControllerDidDismiss:(UIViewController *)viewContrller;
- (void)viewControllerFinishAction:(UIViewController *)viewContrller;

@end

@interface UIViewController (ExtendProtocol)

@property (nonatomic,assign)UIViewController *extendDelegate;
- (void)performExtendDelegateMethod:(SEL)selector;
@end

@interface UITapGestureRecognizer (variable)
@property (nonatomic,copy) NSDictionary *userInfo;
@end