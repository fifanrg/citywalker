//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "DemoTableController.h"

@interface DemoTableController ()

@end

@implementation DemoTableController
@synthesize _popOver = popOver;
@synthesize _typeDataSource = typeDataSource;
@synthesize popDelegate;
@synthesize _typeTag = typeTag;
@synthesize selectionDic;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"";
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [typeDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
//    cell.textLabel.text = [NSString stringWithFormat:@"cell %d",indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[typeDataSource objectAtIndex:indexPath.row]];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int index = indexPath.row;
    
    if (selectionDic) {
            [selectionDic release],selectionDic = nil;
    }
    
    if(self.popDelegate){
        
        selectionDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        [selectionDic setValue:[NSNumber numberWithInt:typeTag] forKey:@"buttonTag"];
        if (index==0) {
            [selectionDic setValue:@"default" forKey:@"value"];
        }
        
        else
        [selectionDic setValue:[typeDataSource objectAtIndex:indexPath.row] forKey:@"value"];
        
        [popDelegate selectFilterType:selectionDic];
    }
    
    if (popOver) {
        [popOver dismissPopoverAnimated:YES];
    }
    
    
}




@end
