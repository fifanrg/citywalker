//
//  RestautantDetailViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-7-9.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestautantDetailViewController : UIViewController<UIScrollViewDelegate>
{

    NSDictionary *entityDic;
    UIScrollView *mainScrollView;
    CGFloat totalHeight;
    UIPageControl *pageController;
}


@property(assign,nonatomic)NSString *urlString;


@end
