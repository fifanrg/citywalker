//
//  CategoryListViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-7-19.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "CategoryListViewController.h"
#import "WCAlertView.h"
#import "EntityCell.h"
#import "KxMenu.h"
#import "CategoryDetailViewController.h"
#import "UIGlossyButton.h"
@interface CategoryListViewController ()

@end

@implementation CategoryListViewController
@synthesize dataSource = _dataSource;
@synthesize currentlyUrl;
@synthesize filterDic;
@synthesize title;
@synthesize wholeDicDataSource = _wholeDicDataSource;
@synthesize currentPageDic = _currentPageDic;
@synthesize listUrlDic = _listUrlDic;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    
    NSLog(@"_tableView:%@",_tableView);
    
    if (self.categoryType != houseType) {
        if (!_tableView) {
            [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                                   self.view.bounds.origin.y,
                                                   self.view.frame.size.width,
                                                   rect_screen.size.height-108.0)];
        }
        
    }
    
    if (self.categoryType == houseType) {
        if (!_tableView) {
            
            [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                                   self.view.bounds.origin.y+33,
                                                   self.view.frame.size.width,
                                                   rect_screen.size.height-141.0)];
            
        }
        
//        [_tableView setFrame:CGRectMake(self.view.bounds.origin.x,
//                                        self.view.bounds.origin.y+33,
//                                        self.view.frame.size.width,
//                                        rect_screen.size.height-141.0)];
        
        [self generateFilterPanel];
        
        //当是housetype的时候，生成filterDic
        if (!filterDic) {
            filterDic = [[NSMutableDictionary alloc]initWithCapacity:1];
            [filterDic setValue:@"不限" forKey:@"type"];
            [filterDic setValue:@"不限" forKey:@"area"];
        }
        
        
//        UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(260, 0, 50, 44)];
//        
//        UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 50, 44)];
//        
//        [buttonView setTitle:@"搜索" forState:UIControlStateNormal];
//        [buttonView addTarget:self action:@selector(searchByKeyword) forControlEvents:UIControlEventTouchUpInside];
//        [leftContainer addSubview:buttonView];
//        [buttonView release];
//        
//        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
//        self.navigationItem.rightBarButtonItem = anotherButton;
//        [anotherButton release];
        

        UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(250, 10, 66, 44)];
        
        UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 66, 44)];
        
        [buttonView setBackgroundImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
        [buttonView addTarget:self action:@selector(searchByKeyword) forControlEvents:UIControlEventTouchUpInside];
        [leftContainer addSubview:buttonView];
        [buttonView release];
        
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        
        negativeSpacer.width = 10;
        
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
        [leftContainer release];
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, anotherButton, nil];
        [anotherButton release];
        
        isfilterOption = NO;
        
    }
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10.0, 0);
    
    [self createHeaderView];
    
    if (![_wholeDicDataSource objectForKey:[NSString stringWithFormat:@"%d",self.categoryType]]) {
        [self showRefreshHeader:YES];
        _dataSource = [[NSMutableArray alloc]initWithCapacity:1];
        [_wholeDicDataSource setObject:_dataSource forKey:[NSString stringWithFormat:@"%d",self.categoryType]];
        isrefreshOrloadMore = YES;
        isfirstTimeRefresh = YES;
        [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:0];
    }
    else{
        _dataSource = [_wholeDicDataSource objectForKey:[NSString stringWithFormat:@"%d",self.categoryType]];
        [self testFinishedLoadData];
    }
    
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    

}


-(void)viewDidDisappear:(BOOL)animated{
    
//    [self.tableView release];

    
    [super viewDidDisappear:animated];

}


#pragma mark - View generation methods
-(void)generateFilterPanel{
    CGRect viewFrame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.frame.size.width, 33);
    UIView *view = [[UIView alloc]initWithFrame:viewFrame];
    [view setBackgroundColor:[UIColor colorWithHexString:@"##222222" withAlpha:1]];
    
    UIGlossyButton *areaButton = [[UIGlossyButton alloc]initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width/2, 33)];
    [areaButton setTitle:@"类别" forState:UIControlStateNormal];
    
    [areaButton setNavigationButtonWithColor:[UIColor colorWithHexString:@"##222222" withAlpha:1]];
	[areaButton useWhiteLabel:YES];
	[areaButton setGradientType:kUIGlossyButtonGradientTypeLinearSmoothStandard];
	[areaButton setStrokeType:kUIGlossyButtonStrokeTypeSolid];
    
    
    
    UIGlossyButton *typeButton = [[UIGlossyButton alloc]initWithFrame:CGRectMake(areaButton.frame.origin.x+areaButton.frame.size.width, view.frame.origin.y, view.frame.size.width/2, 33)];
    [typeButton setTitle:@"选择地区" forState:UIControlStateNormal];
    
    [typeButton setNavigationButtonWithColor:[UIColor colorWithHexString:@"##222222" withAlpha:1]];
	[typeButton useWhiteLabel:YES];
	[typeButton setGradientType:kUIGlossyButtonGradientTypeLinearSmoothStandard];
	[typeButton setStrokeType:kUIGlossyButtonStrokeTypeSolid];

    

    [areaButton setTag:999];
    [typeButton setTag:998];
    
    [areaButton addTarget:self action:@selector(showcategory:) forControlEvents:UIControlEventTouchUpInside];
    [typeButton addTarget:self action:@selector(showsuburbs:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:areaButton];
    [view addSubview:typeButton];
    [areaButton release];
    [typeButton release];
    [self.view addSubview:view];
    [view release];
    
}

-(void)showcategory:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:@"不限"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"整租"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"分租"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    first.alignment = NSTextAlignmentCenter;
    [KxMenu showMenuInView:self.view
                  fromRect:button.frame
                 menuItems:menuItems];
    
}

- (void) pushMenuItem:(id)sender
{
    KxMenuItem *item = (KxMenuItem *)(sender);
    
    NSString *itemtitle = item.title;
     UIButton *button = (UIButton *)[self.view viewWithTag:999];
    [button setTitle:itemtitle forState:UIControlStateNormal];
//    if ([itemtitle isEqualToString:@"不限"]) {
//       
//        [button setTitle:@"不限" forState:UIControlStateNormal];
//    }else if ([itemtitle isEqualToString:@"整租"]){
//    [button setTitle:@"整租" forState:UIControlStateNormal];
//    }else if ([itemtitle isEqualToString:@"分租"]){
//         [button setTitle:itemtitle forState:UIControlStateNormal];
//    }
    
    
    [filterDic setValue:itemtitle forKey:@"type"];
    
}

#pragma mark - typed House methods
-(BOOL)ifhasFilterOption{
    if (filterDic) {
        NSString *areaType = [filterDic objectForKey:@"area"];
        NSString *house = [filterDic objectForKey:@"type"];
        if ([house isEqualToString:@"不限"] && [areaType isEqualToString:@"不限"] ){
           return NO;
        }
    }
    return YES;
}




-(void)showsuburbs:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    
    NSArray *menuItems =
    @[
      [KxMenuItem menuItem:@"不限"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(areaSelectmethod:)],
        
      [KxMenuItem menuItem:@"选择地区"
                     image:[UIImage imageNamed:@"check_icon"]
                    target:self
                    action:@selector(areaSelectmethod:)],
      
      ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    first.alignment = NSTextAlignmentCenter;
    [KxMenu showMenuInView:self.view
                  fromRect:button.frame
                 menuItems:menuItems];
    
 }



-(void)areaSelectmethod:(id)sender{
    
    KxMenuItem *item = (KxMenuItem *)(sender);
    
    NSString *itemtitle = item.title;
    
    if ([itemtitle isEqualToString:@"不限"]) {
        UIButton *button = (UIButton *)[self.view viewWithTag:998];
        [button setTitle:@"不限" forState:UIControlStateNormal];
         [filterDic setValue:@"不限" forKey:@"area"];
    }
    else{
    SuburbAreaSelectionsViewController *suburbAreaSelectionsViewController = [[SuburbAreaSelectionsViewController alloc]initWithNibName:@"SuburbAreaSelectionsViewController" bundle:nil];
    suburbAreaSelectionsViewController.suburbDelegate = self;
    [self.tabBarController presentViewController:suburbAreaSelectionsViewController animated:YES completion:nil];
        
    }
}


-(void)selectSurburb:(id)sender{
    NSLog(@"sender:%@",sender);
    NSString *areaTitle = sender;
    
    UIButton *button = (UIButton *)[self.view viewWithTag:998];
    [button setTitle:areaTitle forState:UIControlStateNormal];
    
    [filterDic setValue:areaTitle forKey:@"area"];
}



//如果是租房类别，提供搜索方法
-(void)searchByKeyword{
    if (!filterDic) {
        filterDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        [filterDic setValue:@"不限" forKey:@"area"];
        [filterDic setValue:@"不限" forKey:@"type"];
    }
    
    
    NSLog(@"filter Dictionary:%@",filterDic);
    NSString *areaType = [filterDic objectForKey:@"area"];
    NSString *houseType = [filterDic objectForKey:@"type"];

    [self createHeaderView];
    [self showRefreshHeader:YES];
    
    if ([areaType isEqualToString:@"不限"] && ([houseType isEqualToString:@"不限"]) ) {
        isfilterOption = NO;
        isrefreshOrloadMore = YES;
        [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:0.0];
    }else{
        isrefreshOrloadMore = YES;
        [self performSelector:@selector(startFilterRequestForHouseType) withObject:nil afterDelay:0.0];
    }
    
   

    
    

}


#pragma mark - Webservice methods Asihttp delegates
-(void)startWebRequest{
    NSURL *url = nil;
    NSString *stringUrl;

    int indexValue = self.categoryType;
    
    
    
    if ((_dataSource && ([_dataSource count]==0)&&isrefreshOrloadMore) ) {
        [self createHeaderView];
        [self showRefreshHeader:YES];
        
        //Old URL
//        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.insyd.com.au/index.php/classifieds/ads/,%d?format=json&start=0&limit=10",indexValue]];
        
        NSString *urlString = [self.listUrlDic objectForKey:[NSString stringWithFormat:@"%d",indexValue]];
        url = [NSURL URLWithString:urlString];
        [_currentPageDic setValue:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"%d",indexValue]];
        
        
    }else if((_dataSource && isrefreshOrloadMore)){
        [self createHeaderView];
        [self showRefreshHeader:YES];
        [_currentPageDic setValue:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"%d",indexValue]];
//        stringUrl = [NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-rating?format=json&start=0&limit=%d", [_dataSource count]];
//        stringUrl = [NSString stringWithFormat:@"http://www.insyd.com.au/index.php/classifieds/ads/,%d?format=json&start=0&limit=%d",indexValue ,[_dataSource count]];
//        NSLog(@"StringUrl category:%@",[stringUrl retain]);
//        url = [NSURL URLWithString:stringUrl];
        
    }else {
//        stringUrl = [NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-rating?format=json&start=%d&limit=10",[_dataSource count]];
//        stringUrl = [NSString stringWithFormat:@"http://www.insyd.com.au/index.php/classifieds/ads/,%d?format=json&start=%d&limit=10",indexValue ,[_dataSource count]];
//        url = [NSURL URLWithString:stringUrl];
//        NSLog(@"StringUrl category:%@",stringUrl);
        
        NSNumber *pageNumber = [_currentPageDic objectForKey:[NSString stringWithFormat:@"%d",indexValue]];
        int page = pageNumber.intValue;
        
        NSString *urlString = [self.listUrlDic objectForKey:[NSString stringWithFormat:@"%d",indexValue]];
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&paged=%d",urlString,page+1]];
        
    }
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    NSLog(@"url : %@",url);
//    [request setDelegate:self];
//    [request startAsynchronous];
    [request setTimeOutSeconds:4000];
    [request setNumberOfTimesToRetryOnTimeout:5];
    
    [request setCompletionBlock:^{
        NSString *responseString = [request responseString];
        
        NSDictionary *responseData = [responseString mutableObjectFromJSONString];
        
        NSLog(@"responseData :%@",responseString);
        NSArray *items = [responseData objectForKey:@"items"];
        
        if (!items ||[items count]==0) {
            
            [WCAlertView showAlertWithTitle:@"Notice"
                                    message:@"No new data found."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                             
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [self testFinishedLoadData];
            return;
        }else{
            
            NSNumber *pageNumber = [_currentPageDic objectForKey:[NSString stringWithFormat:@"%d",indexValue]];
            int page = pageNumber.intValue;
            [_currentPageDic setValue:[NSNumber numberWithInt:page+1] forKey:[NSString stringWithFormat:@"%d",indexValue]];
            
        }
        
        NSLog(@"responseData:%@",items);
        if (isrefreshOrloadMore) {
            [_dataSource removeAllObjects];
        }
        
        [_dataSource addObjectsFromArray:items];
        
        if (isrefreshOrloadMore && isfirstTimeRefresh) {
            [self testFinishedLoadData];
        }else if (isfirstTimeRefresh){
            [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:0.0];
        }else
        {
            [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:0.0];
        }
    }];
    
    
    [request setFailedBlock:^{
        [WCAlertView showAlertWithTitle:@"Notice"
                                message:@"Network error."
                     customizationBlock:^(WCAlertView *alertView) {
                         alertView.style = WCAlertViewStyleVioletHatched;
                         
                     } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                         
                     } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
    }];
    
    
    
    [request startAsynchronous];
//    [request release];
    
}



-(void)startFilterRequestForHouseType{
    NSURL *url = nil;
    
    NSString *areaType = [filterDic objectForKey:@"area"];
    NSString *houseType = [filterDic objectForKey:@"type"];
    
    NSString *urlString = nil;
    NSString *encodeStr = nil;
    
    
    int indexValue = self.categoryType;
    
//    urlString = [NSString stringWithFormat:@"http://www.insyd.com.au/classifieds?option=com_djclassifieds&view=items&se=1&se_cats[]=1&format=json"];
    
    urlString  = [NSString stringWithFormat:@"%@/category/classifieds?json",Host_address];
    
    if (![houseType isEqualToString:@"不限"]) {
        int rentType = 0;
        if([houseType isEqualToString:@"整租"]){
            rentType = 0;
        } else if([houseType isEqualToString:@"分租"]){
            rentType = 1;
        }
        
        encodeStr = [ NSString stringWithFormat:@"&type=%d",rentType];
        urlString = [NSString stringWithFormat:@"%@%@",urlString,encodeStr];
    }
    
    
    if (![areaType isEqualToString:@"不限"]) {
        areaType = [NSString stringWithFormat:@"&suburb=%@",[areaType stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        urlString = [NSString stringWithFormat:@"%@%@",urlString,areaType];
    }
    
    if((_dataSource && isrefreshOrloadMore)){
        
        
        [_currentPageDic setValue:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"%d",indexValue]];
        
        
//        int count = [_dataSource count]<=10 ? 10 : [_dataSource count];
//        stringUrl = [NSString stringWithFormat:@"%@&start=0&limit=%d",urlString,count];
//        NSLog(@"StringUrl when there is filter:%@",[stringUrl retain]);
        NSLog(@"StringUrl :%@",urlString);
        url = [NSURL URLWithString:urlString];
    }else {
//        urlString = [urlString stringByReplacingOccurrencesOfString:@"&start=0&limit=10" withString:@""];
//        stringUrl = [NSString stringWithFormat:@"%@&start=%d&limit=10",urlString,[_dataSource count]];
//        url = [NSURL URLWithString:stringUrl];
//        NSLog(@"StringUrl when there is filter:%@",stringUrl);
        
        NSNumber *pageNumber = [_currentPageDic objectForKey:[NSString stringWithFormat:@"%d",indexValue]];
        int page = pageNumber.intValue;
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&paged=%d",urlString,page+1]];

    }
    
    
    NSLog(@"url  :%@",url);
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setTimeOutSeconds:120];
    [request setNumberOfTimesToRetryOnTimeout:5];
    
    [request setCompletionBlock:^{
        NSString *responseString = [request responseString];
        
        NSDictionary *responseData = [responseString mutableObjectFromJSONString];
        
        NSArray *items = [responseData objectForKey:@"items"];
        
        if (!items ||[items count]==0) {
            
            [WCAlertView showAlertWithTitle:@"Notice"
                                    message:@"No new data found."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                             
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            
            [self testFinishedLoadData];
            return;
        }else{
            
            NSNumber *pageNumber = [_currentPageDic objectForKey:[NSString stringWithFormat:@"%d",indexValue]];
            int page = pageNumber.intValue;
            [_currentPageDic setValue:[NSNumber numberWithInt:page+1] forKey:[NSString stringWithFormat:@"%d",indexValue]];
            
        }
        
        NSLog(@"responseData:%@",items);
        if (isrefreshOrloadMore) {
            [_dataSource removeAllObjects];
        }
        
        [_dataSource addObjectsFromArray:items];
        
        if (isrefreshOrloadMore && isfirstTimeRefresh) {
            [self testFinishedLoadData];
        }else if (isfirstTimeRefresh){
            [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:0.0];
        }else
        {
            [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:0.0];
        }
    }];
    
    
    [request setFailedBlock:^{
        
        [self testFinishedLoadData];
        
        [WCAlertView showAlertWithTitle:@"Notice"
                                message:@"Network error."
                     customizationBlock:^(WCAlertView *alertView) {
                         alertView.style = WCAlertViewStyleVioletHatched;
                         
                     } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                         
                     } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
    }];
    
    
    
    [request startAsynchronous];
}



- (void)requestFinished:(ASIHTTPRequest *)request {
//    NSString *responseString = [request responseString];
//    
//    NSMutableArray *responseData = [responseString mutableObjectFromJSONString];
//    
//    if (!responseData ||[responseData count]==0) {
//        
//        [WCAlertView showAlertWithTitle:@"Notice"
//                                message:@"No new data found."
//                     customizationBlock:^(WCAlertView *alertView) {
//                         alertView.style = WCAlertViewStyleVioletHatched;
//                         
//                     } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
//                         
//                     } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
//        [self testFinishedLoadData];
//        return;
//    }
//    
//    
//    NSLog(@"responseData:%@",responseData);
//    if (isrefreshOrloadMore) {
//        [_dataSource removeAllObjects];
//    }
//    
//    [_dataSource addObjectsFromArray:responseData];
//    
//    if (isrefreshOrloadMore && isfirstTimeRefresh) {
//        [self testFinishedLoadData];
//    }else if (isfirstTimeRefresh){
//        [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:0.0];
//    }else
//    {
//        [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:0.0];
//    }
}

- (void)requestFailed:(ASIHTTPRequest *)request{
    NSLog(@"Error:%@",[request error]);
}

#pragma mark-
#pragma mark overide methods
-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	[super beginToReloadData:aRefreshPos];
    
    if (aRefreshPos == EGORefreshHeader) {
        isrefreshOrloadMore = YES;
        // pull down to refresh data
        
        if (self.categoryType == houseType) {
            Boolean iswithFilterHousetyped = [self ifhasFilterOption];
            if (iswithFilterHousetyped) {
                [self performSelector:@selector(startFilterRequestForHouseType)];
            }else
                 [self performSelector:@selector(startWebRequest)];
        }else
            
        [self performSelector:@selector(startWebRequest)];
        
    }else if(aRefreshPos == EGORefreshFooter){
        isrefreshOrloadMore = NO;
        // pull up to load more data
        
        if (self.categoryType == houseType) {
            Boolean iswithFilterHousetyped = [self ifhasFilterOption];
            if (iswithFilterHousetyped) {
                [self performSelector:@selector(startFilterRequestForHouseType)];
            }else
                [self performSelector:@selector(startWebRequest)];
        }else
        [self performSelector:@selector(startWebRequest)];
    }
}

-(void)testRealRefreshDataSource{
    
    [_tableView reloadData];
    [self finishReloadingData];
}

-(void)testRealLoadMoreData{
    
    [_tableView reloadData];
    [self finishReloadingData];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    [self setFooterView];
    
}

-(void)testFinishedLoadData{
    
    
    // after loading data, should reloadData and set the footer to the proper position
    [self.tableView reloadData];
    [self finishReloadingData];
    [self setFooterView];
}



#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataSource count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    return [view autorelease];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(EntityCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //	[cell setNeedsDisplay];
    
    int section = indexPath.section;
    
    if (section!=0) {
        [cell animationForIndexPath:indexPath];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        CGRect frame = cell.frame;
        frame.size.height = self.tableView.rowHeight;
        [cell setFrame:frame];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setBackgroundView:nil];
        [cell initCellWithType:newsCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    
    [cell setTitle:[dic objectForKey:@"title"]];

//    id location = nil;
    
//    if (self.categoryType==houseType) {
//        location = [dic objectForKey:@"location"];
//    }else
//        location = [dic objectForKey:@"address"];
//    
//    if (location == [NSNull null]) {
//        location = [NSString stringWithFormat:@"无注明"];
//    }else{
//        if ([location isEqualToString:@""]) {
//            location = [NSString stringWithFormat:@"无注明"];
//        }
//    }
    
//    [location stringByReplacingOccurrencesOfString:@"(null)" withString:@"无注明"];
    [cell setLocation: [NSString stringWithFormat:@"地区： %@", [dic objectForKey:@"suburb"]]];
    
    return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = 0.7;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"pageCurl";
    
    NSDictionary *dicInfo = [_dataSource objectAtIndex:indexPath.row];
    
    CategoryDetailViewController *categoryDetailViewController = [[CategoryDetailViewController alloc]initWithNibName:@"CategoryDetailViewController" bundle:nil];
    
    
    categoryDetailViewController.itemId = [dicInfo objectForKey:@"id"];
    categoryDetailViewController.typeId = [dicInfo objectForKey:@"cat_id"];
    
    
    categoryDetailViewController.linkUrl = [dicInfo objectForKey:@"link"];
    
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"Back";
    self.navigationItem.backBarButtonItem = backItem;
    [backItem setAction:@selector(doback:)];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
    [self.navigationController pushViewController:categoryDetailViewController animated:NO];
    [categoryDetailViewController release];
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
