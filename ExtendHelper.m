//
//  CalendarClass.m
//  Plumpton PS
//
//  Created by  on 11-11-1.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "ExtendHelper.h"

#define TIME_ZONE [NSTimeZone systemTimeZone]
#define TIME_ZONE_UTC [NSTimeZone timeZoneWithAbbreviation:@"UTC"]
#define DEFAULT_VOID_COLOR [UIColor clearColor]
#import <objc/runtime.h>
@implementation ExtendHelper
+(void)setUserDefaultValue:(id)value forKey:(NSString *)key {
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    [userData setObject:value forKey:key];
    [userData synchronize];
//    NSLog(@"USERNAME :%@",USER_NAME);
}
+(id)getUserDefaultValueForKey:(NSString *)key {
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    return [userData objectForKey:key];
}
//+(UINavigationController *)getMainNav {
//	AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//	PPRevealSideViewController *revealVC = (PPRevealSideViewController *)delegate.window.rootViewController;
//	UINavigationController *nav = (UINavigationController *)revealVC.rootViewController;
//	return nav;
//}
+ (NSString *)convertDateString:(NSString *)dateString fromFormat:(NSString *)format1 toFormat:(NSString *)format2 {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	NSTimeZone *GMTTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	[formatter setTimeZone:GMTTimeZone];
	NSLog(@"System:%@ Local:%@ Default:%@",[NSTimeZone systemTimeZone], [NSTimeZone localTimeZone], [NSTimeZone defaultTimeZone]);
	[formatter setDateFormat:format1];
	NSDate *date = [formatter dateFromString:dateString];
	NSString *returnString = nil;
	if(date) {
		[formatter setDateFormat:format2];
		[formatter setTimeZone:[NSTimeZone localTimeZone]];
		returnString = [formatter stringFromDate:date];
	}
	[formatter release];
	return returnString;
}
+(NSString *)convertDateString:(NSString *)dateString toFormat:(NSString *)format {
	return [[self class] convertDateString:dateString fromFormat:@"yyyy-MM-dd'Z'" toFormat:format];
}
+(NSString *)convertDateTimeString:(NSString *)dateTimeString toFormat:(NSString *)format {
	return [[self class] convertDateString:dateTimeString fromFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'" toFormat:format];
}
+(NSString *)getLocalTimeString:(NSString *)GMTTimeString{
    
    //    NSRange range  = [GMTTimeString rangeOfString:@"T"];
    //    int addspace = range.location;
    //    [GMTTimeString deleteCharactersInRange:range];
    //    [GMTTimeString insertString:@" " atIndex:addspace];
    //    [GMTTimeString deleteCharactersInRange:NSMakeRange([GMTTimeString length]-8, 8)];
    
    
    NSTimeZone *GMTTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
    [formate setTimeZone:GMTTimeZone];
    NSString *checkFormatString = [GMTTimeString substringFromIndex:GMTTimeString.length - 4];
    NSLog(@"CheckFormatstring is %@",checkFormatString);
    if ([checkFormatString isEqualToString:@"000Z"]) {
        [formate setDateFormat:(@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")];
    }
    else
        [formate setDateFormat:(@"yyyy-MM-dd'T'HH:mm:ss'Z'")];
    NSLog(@"GMTtimeString is %@",GMTTimeString);
    NSDate *GMTdate = [formate dateFromString:GMTTimeString];
    NSLog(@"GMtDate is %@",GMTdate);
    NSTimeZone *localTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger interval = [localTimeZone secondsFromGMTForDate: GMTdate];
    
    NSDate *localeDate = [GMTdate  dateByAddingTimeInterval: interval];
    [formate setDateFormat:(@"yyyy-MM-dd HH:mm")];
    NSString *formatedString = [formate stringFromDate:localeDate];
    [formate release];
    if (formatedString) {
        return formatedString;
    }
    else return @"";
    
    
}




@end
@implementation UINavigationItem (Custom)
//- (void)setTitle:(NSString *)title {
//
//    UILabel *titleLabel = (UILabel *)self.titleView;//(UILabel *)[self.titleView viewWithTag:1234];
//    if(titleLabel == nil || titleLabel.tag != 1234) {
//		if(titleLabel)
//			[titleLabel release],titleLabel = nil;
//        titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
//		titleLabel.alpha = 0;
//        [titleLabel setBackgroundColor:[UIColor clearColor]];
//        [titleLabel setTextAlignment:UITextAlignmentCenter];
//        [titleLabel setTextColor:[UIColor whiteColor]];
//		//[titleLabel setShadowColor:NAV_TEXT_COLOR];
//        [titleLabel setFont:[UIFont fontWithName:@"Georgia" size:24]];
//        [titleLabel sizeToFit];
//        titleLabel.tag = 1234;
//		RECTLog(self.titleView.frame);
//        self.titleView = titleLabel;
//        //[titleLabel release];
//    }
//    titleLabel.text = title;
//    [titleLabel sizeToFit];
//	titleLabel.alpha = 1;
//
//}
@end
@implementation UIColor (HexColor)
+ (UIColor *) colorWithHexString: (NSString *) stringToConvert withAlpha:(float)alpha
{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return DEFAULT_VOID_COLOR;
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    if ([cString length] != 6) return DEFAULT_VOID_COLOR;
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:alpha];
}
@end
@implementation NSDate (SircleDate)



@end
#import "sys/utsname.h"
@implementation UIDevice (BHI_Extensions)
+(NSString*)platform{
	struct utsname systemInfo;
    uname(&systemInfo);
    //get the device model and the system version
    NSString *machine =[NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    return machine;
    /*size_t size;
     sysctlbyname("hw.machine", NULL, &size, NULL, 0);
     char *machine = malloc(size);
     sysctlbyname("hw.machine", machine, &size, NULL, 0);
     //NSString *platform = [NSString stringWithCString:machine];
     NSString* platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
     free(machine);
     return platform;
	 */
}
+(NSString*)platformString{
    NSString *platform = [self platform];
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    return platform;
}
@end

@implementation UIView (GradientView)
- (void)setGradientBackgroundColorFromColor:(UIColor *)startColor toColor:(UIColor *)endColor {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor],(id)[endColor CGColor],nil];
    //gradient.cornerRadius = self.layer.cornerRadius;
    self.layer.masksToBounds = YES;
    [self.layer insertSublayer:gradient atIndex:0];
}
- (void)setGradientBackgroundColorWithColors:(NSArray *)colors {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    NSMutableArray *tempColor = [[NSMutableArray alloc] initWithCapacity:[colors count]];
    for(id c in colors) {
        if([c isKindOfClass:[UIColor class]]) {
            [tempColor addObject:(id)[c CGColor]];
        }
        else
            [tempColor addObject:c];
    }
    gradient.colors = tempColor; //[NSArray arrayWithObjects:(id)[startColor CGColor],(id)[endColor CGColor],nil];
    
    [self.layer insertSublayer:gradient atIndex:0];
    [tempColor release];
}

@end

#include <zlib.h>

@implementation NSData (Compression)

#pragma mark -
#pragma mark Zlib Compression routines
- (NSData *) zlibInflate
{
	if ([self length] == 0) return self;
    
	unsigned full_length = [self length];
	unsigned half_length = [self length] / 2;
    
	NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
	BOOL done = NO;
	int status;
    
	z_stream strm;
	strm.next_in = (Bytef *)[self bytes];
	strm.avail_in = [self length];
	strm.total_out = 0;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
    
	if (inflateInit (&strm) != Z_OK) return nil;
    
	while (!done)
	{
		// Make sure we have enough room and reset the lengths.
		if (strm.total_out >= [decompressed length])
			[decompressed increaseLengthBy: half_length];
		strm.next_out = [decompressed mutableBytes] + strm.total_out;
		strm.avail_out = [decompressed length] - strm.total_out;
        
		// Inflate another chunk.
		status = inflate (&strm, Z_SYNC_FLUSH);
		if (status == Z_STREAM_END) done = YES;
		else if (status != Z_OK) break;
	}
	if (inflateEnd (&strm) != Z_OK) return nil;
    
	// Set real length.
	if (done)
	{
		[decompressed setLength: strm.total_out];
		return [NSData dataWithData: decompressed];
	}
	else return nil;
}
- (NSData *) zlibDeflate
{
    if ([self length] == 0) return self;
	
	z_stream strm;
    
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.total_out = 0;
	strm.next_in=(Bytef *)[self bytes];
	strm.avail_in = [self length];
    
	// Compresssion Levels:
	// Z_NO_COMPRESSION
	// Z_BEST_SPEED
	// Z_BEST_COMPRESSION
	// Z_DEFAULT_COMPRESSION
    
	if (deflateInit(&strm, Z_DEFAULT_COMPRESSION) != Z_OK) return nil;
    
    // 16K chuncks for expansion
	NSMutableData *compressed = [NSMutableData dataWithLength:16384];
    
	do {
        
		if (strm.total_out >= [compressed length])
			[compressed increaseLengthBy: 16384];
		
		strm.next_out = [compressed mutableBytes] + strm.total_out;
		strm.avail_out = [compressed length] - strm.total_out;
		
		deflate(&strm, Z_FINISH);
		
	} while (strm.avail_out == 0);
	
	deflateEnd(&strm);
	
	[compressed setLength: strm.total_out];
	return [NSData dataWithData: compressed];
}

#pragma mark -
#pragma mark Gzip Compression routines
- (NSData *) gzipInflate
{
    if ([self length] == 0) return self;
	
	unsigned full_length = [self length];
	unsigned half_length = [self length] / 2;
	
	NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
	BOOL done = NO;
	int status;
	
	z_stream strm;
	strm.next_in = (Bytef *)[self bytes];
	strm.avail_in = [self length];
	strm.total_out = 0;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	
	if (inflateInit2(&strm, (15+32)) != Z_OK) return nil;
	while (!done)
	{
		// Make sure we have enough room and reset the lengths.
		if (strm.total_out >= [decompressed length])
			[decompressed increaseLengthBy: half_length];
		strm.next_out = [decompressed mutableBytes] + strm.total_out;
		strm.avail_out = [decompressed length] - strm.total_out;
		
		// Inflate another chunk.
		status = inflate (&strm, Z_SYNC_FLUSH);
		if (status == Z_STREAM_END) done = YES;
		else if (status != Z_OK) break;
	}
	if (inflateEnd (&strm) != Z_OK) return nil;
	
	// Set real length.
	if (done)
	{
		[decompressed setLength: strm.total_out];
		return [NSData dataWithData: decompressed];
	}
	else return nil;
}
- (NSData *) gzipDeflate
{
    if ([self length] == 0) return self;
	
	z_stream strm;
	
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.total_out = 0;
	strm.next_in=(Bytef *)[self bytes];
	strm.avail_in = [self length];
	
	// Compresssion Levels:
	// Z_NO_COMPRESSION
	// Z_BEST_SPEED
	// Z_BEST_COMPRESSION
	// Z_DEFAULT_COMPRESSION
	
	if (deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED,
					 (15+16), 8, Z_DEFAULT_STRATEGY) != Z_OK) return nil;
	
	// 16K chunks for expansion
	NSMutableData *compressed = [NSMutableData dataWithLength:16384];
	
	do {
		
		if (strm.total_out >= [compressed length])
			[compressed increaseLengthBy: 16384];
		
		strm.next_out = [compressed mutableBytes] + strm.total_out;
		strm.avail_out = [compressed length] - strm.total_out;
		
		deflate(&strm, Z_FINISH);
		
	} while (strm.avail_out == 0);
	
	deflateEnd(&strm);
	
	[compressed setLength: strm.total_out];
	return [NSData dataWithData:compressed];
}

@end



@implementation UIViewController (ExtendProtocol)

- (void)setExtendDelegate:(UIViewController *)extendDelegate {
	[self setZXObject:extendDelegate];
}
- (UIViewController *)extendDelegate {
	return [self getZXObject];
}
- (void)performExtendDelegateMethod:(SEL)selector {
	if(self.extendDelegate && [self.extendDelegate respondsToSelector:selector]) {
		[self.extendDelegate performSelector:selector withObject:self];
	}
}

@end

@implementation UITapGestureRecognizer (variable)
NSString * const gestureUserInfoKey = @"kDHStyleKey";
- (NSDictionary *)userInfo {
    return (NSDictionary *)objc_getAssociatedObject(self,gestureUserInfoKey);
}
- (void)setUserInfo:(NSDictionary *)userInfo {
    objc_setAssociatedObject(self, gestureUserInfoKey, userInfo,OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end