//
//  DemoTableControllerViewController.h
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"



@protocol popoverDeledate <NSObject>

@optional
-(void)selectFilterType:(id)sender;


@end



@interface DemoTableController : UITableViewController<FPPopoverControllerDelegate>
{
    FPPopoverController *popOver;
    NSArray *typeDataSource;
    int typeTag;
}
@property(nonatomic)int _typeTag;
@property(assign,nonatomic)NSMutableDictionary *selectionDic;
@property(retain,nonatomic)NSArray *_typeDataSource;
@property(retain,nonatomic) FPPopoverController *_popOver;
@property(assign,nonatomic)id<popoverDeledate> popDelegate;



@end
