//
//  EntityListViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-5-15.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "EntityListViewController.h"
#import "EntityCell.h"
#import "UtilDB.h"
#import "TFHpple.h"
#import "ENtityDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ASIHTTPRequest.h"
#import "JSONKit.h"
#import "ExtendHelper.h"
#import "UtilDB.h"
#import  <ShareSDK/ShareSDK.h>
#import "WCAlertView.h"
@interface EntityListViewController ()

@end

@implementation EntityListViewController

@synthesize dataSourceInfo = _dataSourceInfo;
@synthesize galarryArr = _galarryArr;
@synthesize currentObject;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
    
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    
    
    [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                           self.view.bounds.origin.y+33,
                                           self.view.frame.size.width,
                                           rect_screen.size.height-141.0)];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10.0, 0);
//    [self.tableView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:228/255.0 blue:223/255.0 alpha:1]];
    [self generateObjectListMenu];
    
    
    
    
//    [UtilDB checkreadTag:NULL];
    // set header
    [self createHeaderView];
    
    

    
    // the footer should be set after the data of tableView has been loaded, the frame of footer is according to the contentSize of tableView
    // here, actually begin too load your data, eg: from the netserver
//    [self performSelectorOnMainThread:@selector(downloadData) withObject:nil waitUntilDone:YES];
    
    
    
//    [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:2.0f];
    
    
    //Old URL
//    _requestObjectUrlList = [[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-news?format=json&gallery=1"],[NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-news/au?format=json"],[NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-news/global?format=json"],[NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-news/meetup?format=json"], nil];
    
    
     _requestObjectUrlList = [[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"%@/?json&gallery",Host_address],[NSString stringWithFormat:@"%@/category/news/au-news?json",Host_address],[NSString stringWithFormat:@"%@/category/news/global-news?json",Host_address],[NSString stringWithFormat:@"%@/category/news/social-news?json",Host_address],[NSString stringWithFormat:@"%@news/special-news?json",Host_address], nil];
    
    
    _relatedObjectCurrentPage = [[NSMutableDictionary alloc]initWithCapacity:1];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"0"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"1"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"2"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"3"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"4"];
    
    
    
    if ([(NSMutableArray *)[UtilDB getNewsCacheDatabySelected_index:0 ifisGallary:NO] count]!=0) {
        if(!_dataSourceInfo) {
            _dataSourceInfo = [[NSMutableDictionary alloc]init];
        }
        NSString *obj_key = [NSString stringWithFormat:@"%d",0];
        NSMutableArray *tempDict = [[NSMutableArray alloc] initWithCapacity:1];
        
        NSMutableArray *result = [UtilDB getNewsCacheDatabySelected_index:0 ifisGallary:NO];

        [tempDict addObjectsFromArray:result];
        [_dataSourceInfo setObject:tempDict forKey:obj_key];
        [tempDict release];
        
        
        NSMutableArray *result2 = [UtilDB getNewsCacheDatabySelected_index:1 ifisGallary:NO];
        NSMutableArray *result3 = [UtilDB getNewsCacheDatabySelected_index:2 ifisGallary:NO];
        NSMutableArray *result4 = [UtilDB getNewsCacheDatabySelected_index:3 ifisGallary:NO];
        
        NSMutableArray *tempDict2 = [[NSMutableArray alloc] initWithCapacity:1];
        NSMutableArray *tempDict3 = [[NSMutableArray alloc] initWithCapacity:1];
        NSMutableArray *tempDict4 = [[NSMutableArray alloc] initWithCapacity:1];
        
        [tempDict2 addObjectsFromArray:result2];
        [_dataSourceInfo setObject:tempDict2 forKey:[NSString stringWithFormat:@"1"]];
        [tempDict3 addObjectsFromArray:result3];
        [_dataSourceInfo setObject:tempDict3 forKey:[NSString stringWithFormat:@"2"]];
        [tempDict4 addObjectsFromArray:result4];
        [_dataSourceInfo setObject:tempDict4 forKey:[NSString stringWithFormat:@"3"]];
        
        
        [tempDict2 release];
        [tempDict3 release];
        [tempDict4 release];
        
        
        if (!_galarryArr) {
            _galarryArr = [[NSMutableArray alloc]initWithCapacity:1];
        }
        _galarryArr = [UtilDB getNewsCacheDatabySelected_index:0 ifisGallary:YES];
        NSLog(@"_galarry:%@",_galarryArr);
        [self testFinishedLoadData];
        [self setGallary];
        
        
        
        
    }else{
    
        [self showRefreshHeader:YES];
         [self performSelectorOnMainThread:@selector(preLoadDataModelForSelectedIndex) withObject:nil waitUntilDone:YES];
    }

    
    // Do any additional setup after loading the view from its nib.
}


//-(void)shareWX{    
//    
//    id<ISSContent> publishContent = [ShareSDK content:@"share content"
//                                       defaultContent:@""
//                                                image:[ShareSDK imageWithUrl:@"http://img.ithome.com/NewsUploadFiles/2012/2/20120220_104940_881.jpg"]
//                                                title:@"Share from app"
//                                                  url:@"https://itunes.apple.com/cn/app/bao-wei-luo-bo/id534453594?mt=8"
//                                          description:@"这是一条测试信息"
//                                            mediaType:SSPublishContentMediaTypeNews];
//    
//    
//    /**
//     *	@brief	显示分享视图
//     *
//     *	@param 	type 	平台类型
//     *  @param  container   用于显示分享界面的容器，如果只显示在iPhone客户端可以传入nil。如果需要在iPad上显示需要指定容器。
//     *	@param 	content 	分享内容
//     *	@param 	statusBarTips 	状态栏提示标识：YES：显示； NO：隐藏
//     *	@param 	authOptions 	授权选项，用于指定接口在需要授权时的一些属性（如：是否自动授权，授权视图样式等），默认可传入nil
//     *	@param 	shareOptions 	分享选项，用于定义分享视图部分属性（如：标题、一键分享列表、功能按钮等）,默认可传入nil
//     *	@param 	result 	分享返回事件处理
//     */
//    
//    
//    [ShareSDK showShareViewWithType:ShareTypeWeixiTimeline
//                          container:nil
//                            content:publishContent
//                      statusBarTips:YES authOptions:nil shareOptions:nil
//    result:^(ShareType type, SSPublishContentState state, id<ISSStatusInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
//        if (state == SSPublishContentStateSuccess)
//        {
//            NSLog(@"分享成功");
//        }
//        else if (state == SSPublishContentStateFail)
//        {
//            NSLog(@"分享失败,错误码:%d,错误描述:%@" ,[error errorCode], [error errorDescription]);
//        }
//    }];
//
//    
//}





-(void)viewDidUnload{
    

}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if (_tableView) {
        [_tableView reloadData];
    }
}


#pragma mark - segamentControl methods
- (void)generateObjectListMenu {
    relatedObjectList = @[@"头条",@"澳洲新闻",@"综合新闻",@"社区新闻",@"新闻特写"];
//    NSArray *imagrArr = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"add.png"],@"",[UIImage imageNamed:@"add.png"],[UIImage imageNamed:@"add.png"],nil];
    
	SVSegmentedControl *seg = [[SVSegmentedControl alloc] initWithSectionTitles:relatedObjectList];
    seg.tintColor = [UIColor clearColor];
    
    
//    seg.textColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    seg.textColor = [UIColor blackColor];
    seg.textShadowColor = [UIColor clearColor];
    seg.thumbEdgeInset = UIEdgeInsetsMake(4, 0, 6, 4);
    seg.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 12);
    seg.font = [UIFont boldSystemFontOfSize:13];
    
    UIColor *color =  [UIColor colorWithRed:233.0f/255.0f green:105.0f/255.0f blue:62.0f/255.0f alpha:1];
    
    NSLog(@"color.cgcolor:%@",color.CGColor);
    
    
    [color retain];
    
    seg.thumb.tintColor = color;



//    [relatedObjectList retain];
    
//    seg.backgroundImage = [UIImage imageNamed:@"testbac.png"];
    [seg setBackgroundImage:[UIImage imageNamed:@"LINE2.png"]];
	[seg setFrame:CGRectMake(0, -2, 320, 37)];
	[seg addTarget:self action:@selector(segmentSelectedIndexChanged:) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:seg];
	_objectSelectBar = seg;
	[seg release];
    
}

- (void)segmentSelectedIndexChanged:(SVSegmentedControl *)seg {
//    seg.thumb.backgroundColor = [UIColor greenColor];
     [self.tableView reloadData];
    int segindex = seg.selectedIndex;

    switch (segindex) {
        case 0:
            seg.thumb.tintColor = [[UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1] retain];
            break;
        case 1:
            seg.thumb.tintColor = [UIColor greenColor];
            break;
        case 2:
            seg.thumb.tintColor = [UIColor blueColor];
            break;
        case 3:
            seg.thumb.tintColor = [UIColor cyanColor];
            break;
        case 4:
            seg.thumb.tintColor = [UIColor grayColor];
            break;
        default:
            break;
    }
    

	NSString *obj_key = [NSString stringWithFormat:@"%d",seg.selectedIndex];
    NSLog(@"---segament click :%@",obj_key);
    if(_dataSourceInfo) {
        
        
//        NSMutableArray *newsCacheData = [UtilDB getNewsCacheDatabySelected_index:seg.selectedIndex ifisGallary:NO];
//        [_dataSourceInfo setObject:newsCacheData forKey:obj_key];
        
		if([[_dataSourceInfo objectForKey:obj_key] count] != 0) {
			[self changeObject:seg];
			return;
		}
	}
    
    
    
    [self performSelector:@selector(showRefreshHeader:)];
    [self performSelector:@selector(preLoadDataModelForSelectedIndex) withObject:nil afterDelay:0];
}


- (void)changeObject:(SVSegmentedControl *)sender {
   
	if(_dataSourceInfo) {
		NSString *obj_key = [NSString stringWithFormat:@"%d",sender.selectedIndex];
		_currentDataList = [_dataSourceInfo objectForKey:obj_key];
		_currentDataObject = [relatedObjectList objectAtIndex:sender.selectedIndex];
//		[_tableView reloadData];
        [self testFinishedLoadData];
        
	}
}



#pragma mark-
#pragma mark overide methods
-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	[super beginToReloadData:aRefreshPos];
    
    if (aRefreshPos == EGORefreshHeader) {
        // pull down to refresh data
//        [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:2.0];
        [self loadDataIfisRefresh:YES];
        
    }else if(aRefreshPos == EGORefreshFooter){
        // pull up to load more data
        [self loadDataIfisRefresh:NO];
//        [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:2.0];
    }
}

-(void)testRealRefreshDataSource{
    [_tableView reloadData];
    [self finishReloadingData];
}

-(void)testRealLoadMoreData{
        [_tableView reloadData];
        [self finishReloadingData];
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
        [self setFooterView];
}

-(void)testFinishedLoadData{
    
    // after loading data, should reloadData and set the footer to the proper position
    [self.tableView reloadData];
    [self finishReloadingData];
    [self setFooterView];
}





-(void)downloadData{
   
    //     [self performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:2.0f];s
    
}
#pragma mark -
#pragma mark AsiHttpDelegates

-(void)loadDataIfisRefresh:(BOOL)isRefreshOrLoadMore{
    NSString *obj_key = [NSString stringWithFormat:@"%d",_objectSelectBar.selectedIndex];
    _currentDataList = [_dataSourceInfo objectForKey:obj_key];
    NSString *currentUrl = [_requestObjectUrlList objectAtIndex:_objectSelectBar.selectedIndex];
    
    NSLog(@"currentUrl:%@ currentDataList:%@",currentUrl,_currentDataList);
    NSString *newUrlString;
    if (isRefreshOrLoadMore) {
//        newUrlString = [NSString stringWithFormat:@"%@&limit=%d",currentUrl,[_currentDataList count]];
        [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:obj_key];
        newUrlString = [NSString stringWithFormat:@"%@&paged=%@",currentUrl,[_relatedObjectCurrentPage objectForKey:obj_key]];
    }
    else{
        NSNumber  *indexPage = [_relatedObjectCurrentPage objectForKey:obj_key];
        int paged = indexPage.intValue;
//        [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:paged+1] forKey:obj_key];
        newUrlString = [NSString stringWithFormat:@"%@&paged=%d",currentUrl,paged+1];
    }
    
    if (newUrlString) {
		//        result= [self loadDataForObject:_obj withURl:sql];
        
        
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:newUrlString]];
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        
        NSLog(@"newURlString:%@",request);
        
        [request setCompletionBlock:^{
            NSMutableArray *result  = nil;
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
            result = [responseData objectForKey:@"items"];
            [responseData retain];
            
            NSLog(@"result: %@",responseString);
            
            if (!result || [result count]==0) {
                
                [WCAlertView showAlertWithTitle:@"Notice"
                                        message:@"No new data found."
                             customizationBlock:^(WCAlertView *alertView) {
                                 alertView.style = WCAlertViewStyleVioletHatched;
                                 
                             } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                                 
                             } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [self testFinishedLoadData];
                return;
            }

            NSNumber  *indexPage = [_relatedObjectCurrentPage objectForKey:obj_key];
            int paged = indexPage.intValue;
            [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:paged+1] forKey:obj_key];
            
            
            if (result) {
                if (isRefreshOrLoadMore) {
                    [_currentDataList removeAllObjects];
                }
                    [_currentDataList addObjectsFromArray:result];
                
                
                if ([result count]>=10) {
                    NSRange range=NSMakeRange(0, 10);
                    NSArray *array4=[result subarrayWithRange:range];
                    [UtilDB insertNewsCacheData:array4 bySelected_index:_objectSelectBar.selectedIndex ifisGallary:NO];
                }
                else
                    [UtilDB insertNewsCacheData:result bySelected_index:_objectSelectBar.selectedIndex ifisGallary:NO];
                
                [result release];
            }
            
            
            if (_objectSelectBar.selectedIndex == 0 && isRefreshOrLoadMore) {
                if (!_galarryArr) {
                    _galarryArr = [[NSMutableArray alloc]initWithCapacity:1];
                }
                _galarryArr = [responseData objectForKey:@"gallery"];
                
                [UtilDB insertNewsCacheData:_galarryArr bySelected_index:_objectSelectBar.selectedIndex ifisGallary:YES];
                [self setGallary];
            }
            
//            PP_RELEASE(responseData);
            if (isRefreshOrLoadMore) {
                 [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:2.0];
            }else{
                [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:2.0];
            }
        }];
        
        [request setFailedBlock:^{
            [WCAlertView showAlertWithTitle:@"Notice"
                                    message:@"Network error."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                             
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            
        }];
        
        
        [request startAsynchronous];
        [request release];
        
    }
}


- (void)preLoadDataModelForSelectedIndex{
    int index = _objectSelectBar.selectedIndex;
	if(!_dataSourceInfo) {
		_dataSourceInfo = [[NSMutableDictionary alloc]init];
	}
	NSString *obj_key = [NSString stringWithFormat:@"%d",index];
	NSMutableArray *tempDict = [[NSMutableArray alloc] initWithCapacity:1];
    //这里requestObjectUrlList还没初始化，应该在viewwillLoad的时候初始化
	id sql = [_requestObjectUrlList objectAtIndex:index];
    
    
    
	if (sql) {
		//        result= [self loadDataForObject:_obj withURl:sql];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&paged=1",sql]]];
        
        NSLog(@"newsListView sql :%@",request);
        
        
        
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        [request setTimeOutSeconds:20000];
        
        [request setCompletionBlock:^{
            NSMutableArray *result  = nil;
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
            result = [responseData objectForKey:@"items"];
            [responseData retain];
            
            if (result) {
                [tempDict addObjectsFromArray:result];
                //cache non-gallary data
                if ([result count]>=10) {
                    NSRange range=NSMakeRange(0, 10);
                    NSArray *array4=[result subarrayWithRange:range];
                    [UtilDB insertNewsCacheData:array4 bySelected_index:index ifisGallary:NO];
                }
                else
                    [UtilDB insertNewsCacheData:result bySelected_index:index ifisGallary:NO];
               
                 [result release];
                
               
            }
            
            [_dataSourceInfo setObject:tempDict forKey:obj_key];
            [tempDict release];
            
            if (_objectSelectBar.selectedIndex == 0) {
                if (!_galarryArr) {
                    _galarryArr = [[NSMutableArray alloc]initWithCapacity:1];
                }
                
                _galarryArr = [responseData objectForKey:@"gallery"];
                
                
                [UtilDB insertNewsCacheData:_galarryArr bySelected_index:index ifisGallary:YES];
                
                [self setGallary];
                
            }
                        
//            NSLog(@"responseData:%@",responseData);
            
//            PP_RELEASE(responseData);
            
            [self performSelectorOnMainThread:@selector(changeObject:) withObject:_objectSelectBar waitUntilDone:YES];
            [self performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:0.0];
            
        }];
            [request setFailedBlock:^{
                
                NSLog(@"request error :%@",[request error]);
        }];
        
        [request startAsynchronous];
        [request release];
        
    }
	    
}





-(void)setGallary{
//    EntityCell *cell = senderCell;
    
    
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    UIScrollView *imageGallary = [(EntityCell *)cell getImageGallary];
    
    UIPageControl *pageController = [(EntityCell *)cell getpageController];
    
    
    NSLog(@"imageGallary:%@superView:%@",imageGallary,imageGallary.superclass);
    if (_galarryArr) {
    imageGallary.contentSize = CGSizeMake([_galarryArr count]*320, 180.);
    pageController.numberOfPages = [_galarryArr count];
    //    [self updateDots];

        for (int i=0; i<[_galarryArr count]; i++) {
            
            NSDictionary *galaDic = [_galarryArr objectAtIndex:i];
            
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[galaDic objectForKey:@"large"]]];
            //UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
            //UIImageView *img = [[UIImageView alloc] initWithImage:image];
            UIImageView *img = [[UIImageView alloc] init];
            
            img.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dealTaps:)];
            singleTap.numberOfTapsRequired = 1;
            singleTap.delegate= self;
            
            
            [img addGestureRecognizer:singleTap];
            [singleTap setUserInfo:galaDic];
            [singleTap release];

            [img setImageWithURL:imageURL];

            img.frame = CGRectMake(i*320, 0, 320, 180);
            //        img.layer.borderColor = [[UIColor whiteColor]CGColor];
            //        img.layer.borderWidth = 5.0f;
            CGRect captionViewFrame = CGRectMake(0, img.frame.size.height-40, 320, 40);
            UILabel *captionView = [[UILabel alloc]initWithFrame:captionViewFrame];
            [captionView setUserInteractionEnabled:NO];
            [captionView setBackgroundColor:[UIColor blackColor]];
            captionView.alpha = 0.4;
            captionView.textColor = [UIColor whiteColor];
            [captionView setTextAlignment:UITextAlignmentLeft];
            captionView.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:16];
            captionView.numberOfLines = 2;
            captionView.minimumFontSize = 10;
            
                       
            captionView.text = [galaDic objectForKey:@"title"];
            [img addSubview:captionView];
            [captionView release];
            [imageGallary addSubview:img];
            [img release];
        }
    }
}


- (void)dealTaps:(UITapGestureRecognizer *)sender {
     NSLog(@"UserIfo:%@",sender.userInfo);
    if(sender.numberOfTapsRequired == 1) {
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.7;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        animation.type = @"pageCurl";
        
        ENtityDetailViewController *entityController = [[ENtityDetailViewController alloc]initWithNibName:@"ENtityDetailViewController" bundle:nil];
        entityController.entityUrl = [sender.userInfo objectForKey:@"link"];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title = @"Back";
        self.navigationItem.backBarButtonItem = backItem;
        [backItem setAction:@selector(doback:)];
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
        [self.navigationController pushViewController:entityController animated:NO];
        [entityController release];

    }
}


//真正根据URL去发送asihttp请求的方法，用block实现
- (NSMutableArray *)loadDataForObject:(NSString *)_obj withURl:(NSString *)url{
    
    
    
}




-(void)startWebRequest{
    NSURL *url = nil;
    
    url = [NSURL URLWithString:@"http://insyd.com.au/index.php/ws-news?format=json&gallery=1&start=0&limit=10"];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
    [request setTimeOutSeconds:200];
    [request setNumberOfTimesToRetryOnTimeout:5];
}



- (void)requestFinished:(ASIHTTPRequest *)request {
//    NSString *responseString = [request responseString];
//    
//    NSDictionary *responseData = [responseString mutableObjectFromJSONString];
//    
//
//    [self finishReloadingData];
    //    NSMutableArray *sortedArr = [self sortIssue:temparr];
    
    //    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    //    for (NSDictionary *dic in sortedArr) {
    //
    //        NSString *titleStr = [dic objectForKey:@"title"];
    //        NSString *str2= [titleStr stringByReplacingPercentEscapesUsingEncoding:enc];
    //        [dic setValue:str2 forKey:@"title"];
    //        [issueArr addObject:dic];
    //        [dic release];
    //    }
    
    
}




#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int selectedIndex = _objectSelectBar.selectedIndex;
    if (selectedIndex==0) {
        return 2;
    }
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int selectedIndex = _objectSelectBar.selectedIndex;
    NSLog(@"_currentDataCount:%d",[_currentDataList count]);
    _currentDataList = [_dataSourceInfo objectForKey:[NSString stringWithFormat:@"%d",selectedIndex]];
    if (selectedIndex==0) {
        if (section!=0) {
             return [_currentDataList count];
        }
        else
            return 1;
    }
    
    else
        return [_currentDataList count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    int selectedIndex = _objectSelectBar.selectedIndex;
    if (selectedIndex==0) {
        if (indexPath.row==0 && indexPath.section==0) {
            return 180;
        }
        else
            return 80;
    }
    
    else
        return 80;


}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    return [view autorelease];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(EntityCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //	[cell setNeedsDisplay];
    
    int section = indexPath.section;
    
    if (section!=0) {
        [cell animationForIndexPath:indexPath];
    }
	
    //	if(indexPath.row == ((NSIndexPath *)[[tableView indexPathsForVisibleRows] lastObject]).row) {
    //
    //
    //	}
}







- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger sectionIndx = indexPath.section;
     int selectedIndex = _objectSelectBar.selectedIndex;
    
    NSArray *ds = nil;
    
    ds = _currentDataList;
    
    
    if ((sectionIndx!=0 && selectedIndex == 0)|| (sectionIndx == 0 && selectedIndex!=0)) {
        static NSString *CellIdentifier = @"Cell";
        EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            CGRect frame = cell.frame;
            frame.size.height = self.tableView.rowHeight;
            [cell setFrame:frame];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell setBackgroundView:nil];
            [cell initCellWithType:newsCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *dic = [ds objectAtIndex:indexPath.row];
        
        
        NSLog(@"dictionary info:%@",dic);
        
        [cell setTitle:[dic objectForKey:@"title"]];
        [cell setDateLabel:[dic objectForKey:@"publish"]];
        [cell setSourceLabel:[dic objectForKey:@"source"]];
        
       
      BOOL isRead =  [UtilDB checkreadTag:dic withType:@"news_type"];
        NSLog(@"isRead:%d",isRead);
        if (!isRead) {
            [cell setUnreadTag];
        }else{
            [cell removeunReadTag:nil];
        }
        return cell;
    }
    
    else if (sectionIndx==0 && selectedIndex == 0){
        static NSString *PhotoCellIdentifier = @"PhotoCell";
        EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:PhotoCellIdentifier];
        if (cell == nil) {
            cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:PhotoCellIdentifier] autorelease];
            CGRect frame = cell.frame;
            frame.size.height = self.tableView.rowHeight;
            [cell setFrame:frame];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell setBackgroundView:nil];
            [cell initCellWithType:photoCell];
            
            
//            [self setGallary:cell];
        }
    
        return cell;

    }
   
    return NULL;
    
    
    //    [downloadProgress setProgressTintColor:[UIColor redColor]];
    
}







-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int index = _objectSelectBar.selectedIndex;
    NSArray *ds = nil;
    ds = _currentDataList;
    
    if (indexPath.section==0 && index == 0) {
        return;
    }
    
    else if ((indexPath.section==1 && index==0)|| (indexPath.section==0 && index!=0)) {
        
        EntityCell *cell = (EntityCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:indexPath.section]];
        
        
        [cell removeunReadTag:nil];
         NSDictionary *dic = [ds objectAtIndex:indexPath.row];
        [UtilDB insertReadRecord:dic withType:@"news_type"];
        	
        
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.7;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        animation.type = @"pageCurl";
        
        ENtityDetailViewController *entityController = [[ENtityDetailViewController alloc]initWithNibName:@"ENtityDetailViewController" bundle:nil];
        entityController.entityUrl = [[ds objectAtIndex:indexPath.row] objectForKey:@"link"];
        
        
        
//        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UI target:<#(id)#> action:<#(SEL)#>];
//        backItem.title = @"Back";
//        self.navigationItem.backBarButtonItem = backItem;
        [self.navigationItem.backBarButtonItem setAction:@selector(doback:)];
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
        [self.navigationController pushViewController:entityController animated:NO];
        [entityController release];
        
//        [[self.view layer] addAnimation:animation forKey:@"animation"];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    
    [super dealloc];
}

@end
