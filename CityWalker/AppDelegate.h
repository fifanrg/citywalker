//
//  AppDelegate.h
//  CityWalker
//
//  Created by Jianing Ren on 13-4-10.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;


@property (retain, nonatomic) IBOutlet UITabBarController *tabBarController;


@end
