//
//  ENtityDetailViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-17.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageDownloaderDelegate.h"
#import "SDWebImageManagerDelegate.h"
@interface ENtityDetailViewController : UIViewController<SDWebImageManagerDelegate>

{
    NSDictionary *entityDic;
    UIScrollView *mainScrollView;
    CGFloat totalHeight;
    NSMutableArray *elementArray;
}

@property(nonatomic,retain)NSString *entityUrl;
@property(nonatomic,assign)NSMutableDictionary *shareDic;


@end
