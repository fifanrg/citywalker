//
//  UtilTools.h
//  CityWalker
//
//  Created by Jianing Ren on 13-4-10.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilTools : NSObject


+(UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize;

+(void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName;

+(CGFloat)getProportionalHeightFromImage:(UIImage *)image;
@end
