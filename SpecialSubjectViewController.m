//
//  SpecialSubjectViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-5-20.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "SpecialSubjectViewController.h"
#import "EntityCell.h"
#import "ENtityDetailViewController.h"
#import "ASIHTTPRequest.h"
#import "WCAlertView.h"
@interface SpecialSubjectViewController ()

@end

@implementation SpecialSubjectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
    CGRect rect_screen = [[UIScreen mainScreen]bounds];

    
    [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                           self.view.bounds.origin.y+33,
                                           self.view.frame.size.width,
                                           rect_screen.size.height-141.0)];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10.0, 0);
    
    [self generateObjectListMenu];

     self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    [UtilDB checkreadTag:NULL];
    // set header
    [self createHeaderView];
    
    // the footer should be set after the data of tableView has been loaded, the frame of footer is according to the contentSize of tableView
    // here, actually begin too load your data, eg: from the netserver
    //    [self performSelectorOnMainThread:@selector(downloadData) withObject:nil waitUntilDone:YES];
    
    [self showRefreshHeader:YES];
    _didDisplayed = NO;
    
    
    _relatedObjectCurrentPage = [[NSMutableDictionary alloc]initWithCapacity:1];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"0"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"1"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"2"];
    [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:@"3"];
    
    
    _requestObjectUrlList = [[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"%@/category/food?json",Host_address],[NSString stringWithFormat:@"%@/category/fashion?json",Host_address],[NSString stringWithFormat:@"%@/category/life?json",Host_address], [NSString stringWithFormat:@"%@/category/column?json",Host_address], nil];
    
        
//     [NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-column/feature?format=json"],
    
    [self performSelector:@selector(preLoadDataModelForSelectedIndex) withObject:nil afterDelay:2.0f];


//    self.view.frame = CGRectMake(_myScrollView.frame.origin.x, _myScrollView.frame.origin.y, _myScrollView.frame.size.width, _myScrollView.contentSize.height);
    // Do any additional setup after loading the view from its nib.
    
}


#pragma mark - Webrequest methods
- (void)preLoadDataModelForSelectedIndex{
    int index = _objectSelectBar.selectedIndex;
	if(!_dataSourceInfo) {
		_dataSourceInfo = [[NSMutableDictionary alloc]init];
	}
	NSString *obj_key = [NSString stringWithFormat:@"%d",index];
	NSMutableArray *tempDict = [[NSMutableArray alloc] initWithCapacity:1];
    //这里requestObjectUrlList还没初始化，应该在viewwillLoad的时候初始化
	id sql = [_requestObjectUrlList objectAtIndex:index];
    
    
    
	if (sql) {
		//        result= [self loadDataForObject:_obj withURl:sql];
        NSLog(@"sql %@",sql);
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&limit=7",sql]]];
        [request setShouldContinueWhenAppEntersBackground:YES];
        [request setTimeOutSeconds:300];
        
        
        
        [request setCompletionBlock:^{
            NSMutableArray *result  = nil;
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
//            NSLog(@"responseData:%@",responseData);
            result = [responseData objectForKey:@"items"];
            [responseData retain];
            
            if (result) {
                [tempDict addObjectsFromArray:result];
                [result release];
            }
            
            [_dataSourceInfo setObject:tempDict forKey:obj_key];
            [tempDict release];
            
            
            [self performSelectorOnMainThread:@selector(changeObject:) withObject:_objectSelectBar waitUntilDone:YES];
            [self performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:0.0];
            
        }];
        
        
        [request setFailedBlock:^{
            NSLog(@"requestFail:%@",request.error);
            
        }];
        
        
        [request startAsynchronous];
        [request release];
    }
    
}


-(void)loadDataIfisRefresh:(BOOL)isRefreshOrLoadMore{
    NSString *obj_key = [NSString stringWithFormat:@"%d",_objectSelectBar.selectedIndex];
    _currentDataList = [_dataSourceInfo objectForKey:obj_key];
    NSString *currentUrl = [_requestObjectUrlList objectAtIndex:_objectSelectBar.selectedIndex];
    
//    NSLog(@"currentUrl:%@ currentDataList:%@",currentUrl,_currentDataList);
    NSString *newUrlString;
    if (isRefreshOrLoadMore) {
        [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:1] forKey:obj_key];
        newUrlString = [NSString stringWithFormat:@"%@&paged=%@",currentUrl,[_relatedObjectCurrentPage objectForKey:obj_key]];
    }
    else{
        NSNumber  *indexPage = [_relatedObjectCurrentPage objectForKey:obj_key];
        int paged = indexPage.intValue;
//        [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:paged+1] forKey:obj_key];
        newUrlString = [NSString stringWithFormat:@"%@&paged=%d",currentUrl,paged+1];
    }
    
    if (newUrlString) {
		//        result= [self loadDataForObject:_obj withURl:sql];
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:newUrlString]];
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        [request setCompletionBlock:^{
            NSMutableArray *result  = nil;
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
            result = [responseData objectForKey:@"items"];
            [responseData retain];
            
            if (!result || [result count]==0) {
                [WCAlertView showAlertWithTitle:@"Notice"
                                        message:@"No new data found."
                             customizationBlock:^(WCAlertView *alertView) {
                                 alertView.style = WCAlertViewStyleVioletHatched;
                                 
                             } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                                 
                             } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [self testFinishedLoadData];
                return;
            }
            
            
            NSNumber  *indexPage = [_relatedObjectCurrentPage objectForKey:obj_key];
            int paged = indexPage.intValue;
            [_relatedObjectCurrentPage setValue:[NSNumber numberWithInt:paged+1] forKey:obj_key];
            
            
            
            if (result) {
                if (isRefreshOrLoadMore) {
                    [_currentDataList removeAllObjects];
                }
                [_currentDataList addObjectsFromArray:result];
                [result release];
                
            }
            
            if (isRefreshOrLoadMore) {
                [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:2.0];
            }else{
                [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:2.0];
                
            }
        }];
        
        
        [request setFailedBlock:^{
            
            
        }];
        
        
        [request startAsynchronous];
        [request release];
    }
}



#pragma mark - segamentControl methods
- (void)generateObjectListMenu {
    relatedObjectList = @[@"写实主义",@"尚品潮流",@"乐活频道",@"特别策划"];
    
	SVSegmentedControl *seg = [[SVSegmentedControl alloc] initWithSectionTitles:relatedObjectList];
    seg.tintColor = [UIColor clearColor];
    
    
    //    seg.textColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    seg.textColor = [UIColor blackColor];
    seg.textShadowColor = [UIColor clearColor];
    seg.thumbEdgeInset = UIEdgeInsetsMake(4, 4, 6, 4);
    seg.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 12);
    seg.font = [UIFont boldSystemFontOfSize:13];
    
    UIColor *color =  [UIColor colorWithRed:233.0f/255.0f green:105.0f/255.0f blue:62.0f/255.0f alpha:1];
    
    NSLog(@"color.cgcolor:%@",color.CGColor);
    
    
    [color retain];
    
    seg.thumb.tintColor = color;
    
    
    
//    [relatedObjectList retain];
    
    seg.backgroundImage = [UIImage imageNamed:@"LINE1.png"];
	[seg setFrame:CGRectMake(0, -2, 320, 37)];
	[seg addTarget:self action:@selector(segmentSelectedIndexChanged:) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:seg];
	_objectSelectBar = seg;
	[seg release];
    
}

- (void)segmentSelectedIndexChanged:(SVSegmentedControl *)seg {
    //    seg.thumb.backgroundColor = [UIColor greenColor];
    [self.tableView reloadData];
    int segindex = seg.selectedIndex;
    
    switch (segindex) {
        case 0:
            seg.thumb.tintColor = [[UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1] retain];
            break;
        case 1:
            seg.thumb.tintColor = [UIColor greenColor];
            break;
        case 2:
            seg.thumb.tintColor = [UIColor blueColor];
            break;
        case 3:
            seg.thumb.tintColor = [UIColor cyanColor];
            break;
        case 4:
            seg.thumb.tintColor = [UIColor grayColor];
            break;
        default:
            break;
    }
    
    NSLog(@"segmentSelectedIndexChanged");
	NSString *obj_key = [NSString stringWithFormat:@"%d",seg.selectedIndex];
    NSLog(@"---segament click :%@",obj_key);
    if(_dataSourceInfo) {
		if([_dataSourceInfo objectForKey:obj_key]) {
			[self changeObject:seg];
			return;
		}
	}
    
    [self performSelector:@selector(showRefreshHeader:)];
    [self performSelector:@selector(preLoadDataModelForSelectedIndex) withObject:nil afterDelay:0];
}


- (void)changeObject:(SVSegmentedControl *)sender {
    NSLog(@"changeObject");
    
	if(_dataSourceInfo) {
		NSString *obj_key = [NSString stringWithFormat:@"%d",sender.selectedIndex];
		_currentDataList = [_dataSourceInfo objectForKey:obj_key];
		_currentDataObject = [relatedObjectList objectAtIndex:sender.selectedIndex];
//		[_tableView reloadData];
         [self testFinishedLoadData];
	}
}


#pragma mark-
#pragma mark overide methods
-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	[super beginToReloadData:aRefreshPos];
    
    if (aRefreshPos == EGORefreshHeader) {
        // pull down to refresh data
        //        [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:2.0];
        [self loadDataIfisRefresh:YES];
    }else if(aRefreshPos == EGORefreshFooter){
        // pull up to load more data
        [self loadDataIfisRefresh:NO];
        //      [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:2.0];
    }
}
-(void)testRealRefreshDataSource{
    //    NSInteger count = _dataSource?_dataSource.count:0;
    //
    //    [_dataSource removeAllObjects];
    //    _refreshCount ++;
    //
    //    for (int i = 0; i < count; i++) {
    //        NSString *newString = [NSString stringWithFormat:@"%d_new label number %d", _refreshCount,i];
    //        [_dataSource addObject:newString];
    //    }
    
    //    NSLog(@"_dataSOurce is %@",_dataSource);
    
    // after refreshing data, call finishReloadingData to reset the header/footer view
    [_tableView reloadData];
    [self finishReloadingData];
}

-(void)testRealLoadMoreData{
    //    NSInteger count = _dataSource?_dataSource.count:0;
    //    NSString *stringFormat;
    //    if (_refreshCount == 0) {
    //        stringFormat = @"label number %d";
    //    }else {
    //        stringFormat = [NSString stringWithFormat:@"%d_new label number ", _refreshCount];
    //        stringFormat = [stringFormat stringByAppendingString:@"%d"];
    //    }
    //
    //    for (int i = 0; i < 20; i++) {
    //        NSString *newString = [NSString stringWithFormat:stringFormat, i+count];
    //        if (_dataSource == nil) {
    //            _dataSource = [[NSMutableArray alloc] initWithCapacity:4];
    //
    //        }
    //        [_dataSource addObject:newString];
    //    }
    //
    //    _loadMoreCount ++;
    
    
    
    // after refreshing data, call finishReloadingData to reset the header/footer view
    [_tableView reloadData];
    [self finishReloadingData];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    [self setFooterView];
    
}

-(void)testFinishedLoadData{
    
    NSLog(@"testFinishedLoadData");
    

    
    // after loading data, should reloadData and set the footer to the proper position
    [self.tableView reloadData];
    [self finishReloadingData];
    [self setFooterView];
}

//真正根据URL去发送asihttp请求的方法，用block实现
- (NSMutableArray *)loadDataForObject:(NSString *)_obj withURl:(NSString *)url{
    return NULL;
    
}







-(void)downloadData{
    
    //     [self performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:2.0f];s
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(EntityCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //	[cell setNeedsDisplay];
    
    int section = indexPath.section;
    
    if (section!=0) {
        [cell animationForIndexPath:indexPath];
    }
	
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        return [_currentDataList count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 222;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    return [view autorelease];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    
    NSArray *ds = nil;
    
    ds = _currentDataList;

    
        static NSString *CellIdentifier = @"Cell";
        EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            
            cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            CGRect frame = cell.frame;
            frame.size.height = self.tableView.rowHeight;
            [cell setFrame:frame];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell setBackgroundView:nil];
            [cell initCellWithType:subjectCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    
     NSDictionary *dic = [ds objectAtIndex:indexPath.row];
    NSString *coverUrlString = [NSString stringWithFormat:@"%@%@",Host_address,[dic objectForKey:@"small"]];
    
    NSLog(@"coverUrlString:%@",coverUrlString);
    [cell setSubjectCover:coverUrlString];
    [cell setTitle:[dic objectForKey:@"title"]];
    

    [cell setFrameToImageView];
    
        return cell;
    
    
    
    //    [downloadProgress setProgressTintColor:[UIColor redColor]];
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
//    int index = _objectSelectBar.selectedIndex;
    NSArray *ds = nil;
    ds = _currentDataList;

    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = 0.7;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"pageCurl";
    
    ENtityDetailViewController *entityController = [[ENtityDetailViewController alloc]initWithNibName:@"ENtityDetailViewController" bundle:nil];
    entityController.entityUrl = [[ds objectAtIndex:indexPath.row] objectForKey:@"link"];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"Back";
    self.navigationItem.backBarButtonItem = backItem;
    [backItem setAction:@selector(doback:)];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
    [self.navigationController pushViewController:entityController animated:NO];
    [entityController release];
    

}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
