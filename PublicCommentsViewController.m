//
//  PublicCommentsViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-6-2.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "PublicCommentsViewController.h"
#import "EntityCell.h"
#import "ENtityDetailViewController.h"
#import "DemoTableController.h"
#import "FPPopoverController.h"
#import "WCAlertView.h"
#import "RestautantDetailViewController.h"
#import "UIGlossyButton.h"
@interface PublicCommentsViewController ()

@end

@implementation PublicCommentsViewController
@synthesize dataSource = _dataSource;
@synthesize currentlyUrl;
@synthesize filterDic;
@synthesize cuizineDropDown,typedDropDown,areaDropDown;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    
    
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
    [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                           self.view.bounds.origin.y+33,
                                           self.view.frame.size.width,
                                           rect_screen.size.height-141.0)];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10.0, 0);
    //    [self.tableView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:228/255.0 blue:223/255.0 alpha:1]];

    [self generateFilterPanel];
    //    [UtilDB checkreadTag:NULL];
    
//    UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(260, 0, 50, 44)];
//    
//    UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 50, 44)];
//    
//    [buttonView setTitle:@"搜索" forState:UIControlStateNormal];
//    [buttonView addTarget:self action:@selector(filterByKeyWord) forControlEvents:UIControlEventTouchUpInside];
//    [leftContainer addSubview:buttonView];
//    [buttonView release];
//    
//    
//    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
//    self.navigationItem.rightBarButtonItem = anotherButton;
//    [anotherButton release];

    
    
    UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(250, 10, 66, 44)];
    
    UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 66, 44)];
    
    [buttonView setBackgroundImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [buttonView addTarget:self action:@selector(filterByKeyWord) forControlEvents:UIControlEventTouchUpInside];
    [leftContainer addSubview:buttonView];
    [buttonView release];
    
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    
    negativeSpacer.width = 10;
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
    [leftContainer release];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, anotherButton, nil];
    [anotherButton release];
    
    
    //初始化筛选dictionary
    if (!filterDic) {
        filterDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        [filterDic setValue:@"default" forKey:@"areaType"];
        [filterDic setValue:@"default" forKey:@"dishType"];
        [filterDic setValue:@"default" forKey:@"priceType"];
    }

    
    
    
    // set header
    [self createHeaderView];
    [self showRefreshHeader:YES];
    
    
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc]initWithCapacity:1];
    }
    
    [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:0];
   
    
    isrefreshOrloadMore = YES;
    isfirstTimeRefresh = YES;
    isfilterOption = NO;

}

-(void)generateFilterPanel{
    CGRect viewFrame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.frame.size.width, 33);
    UIView *view = [[UIView alloc]initWithFrame:viewFrame];
    [view setBackgroundColor:[UIColor colorWithHexString:@"#222222" withAlpha:1]];
    UIGlossyButton *areaButton = [[UIGlossyButton alloc]initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width/3, 33)];
    [areaButton setTitle:@"选择地区" forState:UIControlStateNormal];
    

    [areaButton setNavigationButtonWithColor:[UIColor colorWithHexString:@"#222222" withAlpha:1]];
	[areaButton useWhiteLabel:YES];
	[areaButton setGradientType:kUIGlossyButtonGradientTypeLinearSmoothStandard];
	[areaButton setStrokeType:kUIGlossyButtonStrokeTypeSolid];
    
    
    UIGlossyButton *typeButton = [[UIGlossyButton alloc]initWithFrame:CGRectMake(areaButton.frame.origin.x+areaButton.frame.size.width, view.frame.origin.y, view.frame.size.width/3, 33)];
    [typeButton setTitle:@"选择菜系" forState:UIControlStateNormal];
    
    [typeButton setNavigationButtonWithColor:[UIColor colorWithHexString:@"#222222" withAlpha:1]];
	[typeButton useWhiteLabel:YES];
	[typeButton setGradientType:kUIGlossyButtonGradientTypeLinearSmoothStandard];
	[typeButton setStrokeType:kUIGlossyButtonStrokeTypeSolid];
    
    
    UIGlossyButton *averageButton = [[UIGlossyButton alloc]initWithFrame:CGRectMake(typeButton.frame.origin.x+typeButton.frame.size.width, view.frame.origin.y, view.frame.size.width/3, 33)];
    [averageButton setTitle:@"人均消费" forState:UIControlStateNormal];
    
    [averageButton setNavigationButtonWithColor:[UIColor colorWithHexString:@"#222222" withAlpha:1]];
	[averageButton useWhiteLabel:YES];
	[averageButton setGradientType:kUIGlossyButtonGradientTypeLinearSmoothStandard];
	[averageButton setStrokeType:kUIGlossyButtonStrokeTypeSolid];



    
    [areaButton setTag:999];
    [typeButton setTag:998];
    [averageButton setTag:997];
    
    
    [areaButton addTarget:self action:@selector(popOverSelection:) forControlEvents:UIControlEventTouchUpInside];
    [typeButton addTarget:self action:@selector(popOverSelection:) forControlEvents:UIControlEventTouchUpInside];
    [averageButton addTarget:self action:@selector(popOverSelection:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:areaButton];
    [view addSubview:typeButton];
    [view addSubview:averageButton];
    [areaButton release];
    [typeButton release];
    [averageButton release];
    [self.view addSubview:view];
    [view release];

}

-(void)popOverSelection:(id)sender{
    UIButton *senderButton  = (UIButton *)sender;
    
    ///999typed area, 998 typed cuizine, 997 typed price
    NSArray *typeArray = nil;
    switch (senderButton.tag) {
        case 999:
            typeArray = [NSArray arrayWithObjects:@"默认",@"CITY",@"东区",@"南区",@"西区",@"北区",@"其他", nil];
            break;
            
        case 998:
            typeArray = [NSArray arrayWithObjects:@"默认",@"意大利菜",@"日本菜",@"泰国菜",@"法国菜",@"西班牙菜",@"德国菜",@"东南亚",@"韩国菜",@"中餐",@"海鲜",@"牛排",@"茶饮",@"烧烤",@"火锅",@"快餐",@"素食",@"印度菜",@"咖啡",@"其他",nil];
            break;
        case 997:
            typeArray = [NSArray arrayWithObjects:@"默认",@"5-10",@"10-20",@"20-50",@"50-100",@"100-180",nil];
            break;
        default:
            break;
    }
    
    DemoTableController *controller = [[DemoTableController alloc] initWithStyle:UITableViewStylePlain];
    
    controller.popDelegate = self;
    
    controller._typeDataSource =typeArray;
    controller._typeTag = senderButton.tag;
    
    FPPopoverController *popover = [[FPPopoverController alloc] initWithViewController:controller];
    [controller release];
    
    
    controller._popOver = popover;
    
    
    //popover.arrowDirection = FPPopoverArrowDirectionAny;
    popover.tint = FPPopoverRedTint;

    
    popover.contentSize = CGSizeMake(320/2, 300);
    popover.arrowDirection = FPPopoverArrowDirectionAny;
    
    //sender is the UIButton view
    [popover presentPopoverFromView:sender];
    [sender release];
    
}


#pragma mark - popover delegate
-(void)selectFilterType:(id)sender{

    NSMutableDictionary *selection = (NSMutableDictionary *)sender;
    
    int buttonTag = ((NSNumber *)[selection objectForKey:@"buttonTag"]).intValue;
    NSString *value = (NSString *)[selection objectForKey:@"value"];
    UIButton *tagButton = (UIButton *)[self.view viewWithTag:buttonTag];
    
    if (!filterDic) {
        filterDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        [filterDic setValue:@"default" forKey:@"areaType"];
        [filterDic setValue:@"default" forKey:@"dishType"];
        [filterDic setValue:@"default" forKey:@"priceType"];
    }
    
    
    if ([value isEqualToString:@"default"]) {
        switch (buttonTag) {
            case 999:
                [tagButton setTitle:@"选择地图" forState:UIControlStateNormal];
                [filterDic setValue:@"default" forKey:@"areaType"];
                break;
            case 998:
                [tagButton setTitle:@"选择菜系" forState:UIControlStateNormal];
                [filterDic setValue:@"default" forKey:@"dishType"];
                break;
            case 997:
                [tagButton setTitle:@"人均消费" forState:UIControlStateNormal];
                 [filterDic setValue:@"default" forKey:@"priceType"];
                break;
            default:
                break;
        }
    }
    else {
       [tagButton setTitle:value forState:UIControlStateNormal];
        
        
        switch (buttonTag) {
            case 999:
                [filterDic setValue:value forKey:@"areaType"];
                break;
            case 998:
                [filterDic setValue:value forKey:@"dishType"];
                break;
            case 997:
                 [filterDic setValue:value forKey:@"priceType"];
                break;
            default:
                break;
        }
    }
    
    
    
    
}

-(void)filterByKeyWord{
    if (!filterDic) {
        filterDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        [filterDic setValue:@"default" forKey:@"areaType"];
        [filterDic setValue:@"default" forKey:@"dishType"];
        [filterDic setValue:@"default" forKey:@"priceType"];
    }
    
    
        NSLog(@"filter Dictionary:%@",filterDic);
        NSString *areaType = [filterDic objectForKey:@"areaType"];
        NSString *dishType = [filterDic objectForKey:@"dishType"];
        NSString *priceType = [filterDic objectForKey:@"priceType"];
        [self createHeaderView];
        [self showRefreshHeader:YES];
    
        if ([areaType isEqualToString:@"default"] && ([dishType isEqualToString:@"default"]) && ([priceType isEqualToString:@"default"])) {
            isfilterOption = NO;
            isrefreshOrloadMore = YES;
            [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:0.0];
        }else{
            isrefreshOrloadMore = YES;
            [self performSelector:@selector(startWebRequestWithFilter) withObject:nil afterDelay:0.0];
        }
    
}



#pragma mark - Webservice methods
-(void)startWebRequest{
    NSURL *url = nil;
    NSString *stringUrl;
    if ((_dataSource && ([_dataSource count]==0)&&isrefreshOrloadMore) ) {
         url = [NSURL URLWithString:@"http://insyd.com.au/index.php/ws-rating?format=json&start=0&limit=10"];
        
    }else if((_dataSource && isrefreshOrloadMore)){
       stringUrl = [NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-rating?format=json&start=0&limit=%d", [_dataSource count]];
        NSLog(@"StringUrl publiccomment:%@",[stringUrl retain]);
        
        
        url = [NSURL URLWithString:stringUrl];
    }else {
        stringUrl = [NSString stringWithFormat:@"http://insyd.com.au/index.php/ws-rating?format=json&start=%d&limit=10",[_dataSource count]];
         url = [NSURL URLWithString:stringUrl];
        NSLog(@"StringUrl publiccomment:%@",stringUrl);
    }
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    [request setTimeOutSeconds:4000];
    [request setNumberOfTimesToRetryOnTimeout:5];
    
}


-(BOOL)ifhasFilterOption{
    if (filterDic) {
        NSLog(@"filterDic:%@",filterDic);
        NSString *areaType = [filterDic objectForKey:@"areaType"];
        NSString *dishType = [filterDic objectForKey:@"dishType"];
        NSString *priceType = [filterDic objectForKey:@"priceType"];
        if ([areaType isEqualToString:@"default"] && ([dishType isEqualToString:@"default"]) && ([priceType isEqualToString:@"default"])) {
            return NO;
        }
    }
    return YES;
}


-(void)startWebRequestWithFilter{
    
    NSURL *url = nil;
    NSString *stringUrl;
    
    NSString *areaType = [filterDic objectForKey:@"areaType"];
    NSString *dishType = [filterDic objectForKey:@"dishType"];
    NSString *priceType = [filterDic objectForKey:@"priceType"];
    
    NSString *areaTypeString = nil;
    NSString *dishTypeString = nil;
    NSString *priceTypeString = nil;
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.insyd.com.au/rating/itemlist/filter?"];
    if (![areaType isEqualToString:@"default"]) {
        areaTypeString = [NSString stringWithFormat:@"array15[]=%@&",[areaType stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        urlString = [NSString stringWithFormat:@"%@%@",urlString,areaTypeString];
    }
    
    if (![dishType isEqualToString:@"default"]) {
        dishTypeString = [NSString stringWithFormat:@"array16[]=%@&",[dishType stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
         urlString = [NSString stringWithFormat:@"%@%@",urlString,dishTypeString];
    }
    
    if (![priceType isEqualToString:@"default"]) {
        NSArray *priceRange = [priceType componentsSeparatedByString:@"-"];
        
        if ([priceRange count]==2) {
            NSString *from = [priceRange objectAtIndex:0];
            NSString *to = [priceRange objectAtIndex:1];
            priceTypeString = [NSString stringWithFormat:@"searchword21-from=%@&searchword21-to=%@&",from,to];
//            priceTypeString = [priceTypeString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            urlString = [NSString stringWithFormat:@"%@%@",urlString,priceTypeString];
        }
    }

     if((_dataSource && isrefreshOrloadMore)){
         
         int count = [_dataSource count]<=10 ? 10 : [_dataSource count];
         
        stringUrl = [NSString stringWithFormat:@"%@format=json&start=0&limit=%d",urlString,count];
        NSLog(@"StringUrl when there is filter:%@",[stringUrl retain]);
        
        url = [NSURL URLWithString:stringUrl];
    }else {
        stringUrl = [NSString stringWithFormat:@"%@format=json&start=%d&limit=10",urlString,[_dataSource count]];
        url = [NSURL URLWithString:stringUrl];
        NSLog(@"StringUrl when there is filter:%@",stringUrl);
    }
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    [request setTimeOutSeconds:4000];
    [request setNumberOfTimesToRetryOnTimeout:5];

}



- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    
    NSMutableArray *responseData = [responseString mutableObjectFromJSONString];
    
    if ([responseData count]==0) {
        
        [WCAlertView showAlertWithTitle:@"Notice"
                                message:@"No new data found."
                     customizationBlock:^(WCAlertView *alertView) {
                         alertView.style = WCAlertViewStyleVioletHatched;
                         
                     } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                         
                     } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [self testFinishedLoadData];
        return;
    }
    
    NSLog(@"responseData:%@",responseData);
    if (isrefreshOrloadMore) {
        [_dataSource removeAllObjects];
    }
    
    [_dataSource addObjectsFromArray:responseData];
    
    if (isrefreshOrloadMore && isfirstTimeRefresh) {
        [self testFinishedLoadData];
    }else if (isfirstTimeRefresh){
         [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:0.0];
    }else
    {
         [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:0.0];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request{
    NSLog(@"Error:%@",[request error]);
    [WCAlertView showAlertWithTitle:@"Notice"
                            message:@"Error,Please try later."
                 customizationBlock:^(WCAlertView *alertView) {
                     alertView.style = WCAlertViewStyleVioletHatched;
                     
                 } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                     
                 } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    
}

#pragma mark-
#pragma mark overide methods
-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	[super beginToReloadData:aRefreshPos];
    
    Boolean iswithfilter =[self ifhasFilterOption];
    
    
    if (aRefreshPos == EGORefreshHeader) {
        isrefreshOrloadMore = YES;
        // pull down to refresh data
//        [self performSelector:@selector(testRealRefreshDataSource) withObject:nil afterDelay:2.0];
        if (!iswithfilter) {
            [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:0.0];
        }else {//如果是有搜索条件的
             [self performSelector:@selector(startWebRequestWithFilter) withObject:nil afterDelay:0.0];
        }
    }else if(aRefreshPos == EGORefreshFooter){
        isrefreshOrloadMore = NO;
        // pull up to load more data
        if (!iswithfilter){
            [self performSelector:@selector(startWebRequest) withObject:nil afterDelay:0.0];
        }else{//如果是有搜索条件的
            [self performSelector:@selector(startWebRequestWithFilter) withObject:nil afterDelay:0.0];
        }
         //        [self performSelector:@selector(testRealLoadMoreData) withObject:nil afterDelay:2.0];
    }
}

-(void)testRealRefreshDataSource{
    //    NSInteger count = _dataSource?_dataSource.count:0;
    //
    //    [_dataSource removeAllObjects];
    //    _refreshCount ++;
    //
    //    for (int i = 0; i < count; i++) {
    //        NSString *newString = [NSString stringWithFormat:@"%d_new label number %d", _refreshCount,i];
    //        [_dataSource addObject:newString];
    //    }
    
    //    NSLog(@"_dataSOurce is %@",_dataSource);
    
    // after refreshing data, call finishReloadingData to reset the header/footer view
    [_tableView reloadData];
    [self finishReloadingData];
}

-(void)testRealLoadMoreData{
    //    NSInteger count = _dataSource?_dataSource.count:0;
    //    NSString *stringFormat;
    //    if (_refreshCount == 0) {
    //        stringFormat = @"label number %d";
    //    }else {
    //        stringFormat = [NSString stringWithFormat:@"%d_new label number ", _refreshCount];
    //        stringFormat = [stringFormat stringByAppendingString:@"%d"];
    //    }
    //
    //    for (int i = 0; i < 20; i++) {
    //        NSString *newString = [NSString stringWithFormat:stringFormat, i+count];
    //        if (_dataSource == nil) {
    //            _dataSource = [[NSMutableArray alloc] initWithCapacity:4];
    //
    //        }
    //        [_dataSource addObject:newString];
    //    }
    //
    //    _loadMoreCount ++;
    
    
    
    // after refreshing data, call finishReloadingData to reset the header/footer view
    [_tableView reloadData];
    [self finishReloadingData];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    [self setFooterView];
    
}

-(void)testFinishedLoadData{
    
//    [self startWebRequest];
    
    NSLog(@"testFinishedLoadData");
    
    // after loading data, should reloadData and set the footer to the proper position
    [self.tableView reloadData];
    [self finishReloadingData];
    [self setFooterView];
}

//真正根据URL去发送asihttp请求的方法，用block实现
- (NSMutableArray *)loadDataForObject:(NSString *)_obj withURl:(NSString *)url{
    return NULL;
    
}




-(void)downloadData{
    
    //     [self performSelector:@selector(testFinishedLoadData) withObject:nil afterDelay:2.0f];s
    
}



#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [_dataSource count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    return [view autorelease];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(EntityCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //	[cell setNeedsDisplay];
    
    int section = indexPath.section;
    
    if (section!=0) {
        [cell animationForIndexPath:indexPath];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        static NSString *CellIdentifier = @"Cell";
        EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            
            cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            CGRect frame = cell.frame;
            frame.size.height = self.tableView.rowHeight;
            [cell setFrame:frame];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell setBackgroundView:nil];
            [cell initCellWithType:rateCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
    
        
        NSDictionary *itemDic = [_dataSource objectAtIndex:indexPath.row];
    
        
        
    
        [cell setAreaLabel:[itemDic objectForKey:@"rating_suburb"]];
        [cell setSubjectCover:[NSString stringWithFormat:@"http://www.insyd.com.au/%@",[itemDic objectForKey:@"rating_thumbnail"]]];
    
        [cell setTitle:[itemDic objectForKey:@"title"]];
        [cell setphoneLabel:[itemDic objectForKey:@"rating_phone"]];
        [cell setTypedLabel:[itemDic objectForKey:@"rating_cuisines"]];
        [cell setAverageLabel:[itemDic objectForKey:@"rating_price"]];
    
        return cell;

    
    

    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.7;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        animation.type = @"pageCurl";
    
        NSDictionary *dicInfo = [_dataSource objectAtIndex:indexPath.row];
    
        RestautantDetailViewController *entityController = [[RestautantDetailViewController alloc]initWithNibName:@"RestautantDetailViewController" bundle:nil];
    
        entityController.urlString = [dicInfo objectForKey:@"link"];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title = @"Back";
        self.navigationItem.backBarButtonItem = backItem;
        [backItem setAction:@selector(doback:)];
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
        [self.navigationController pushViewController:entityController animated:NO];
        [entityController release];
        
        //        [[self.view layer] addAnimation:animation forKey:@"animation"];
}




- (void)dealloc {
    if (filterDic) {
        [filterDic release],filterDic = nil;
    }
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
