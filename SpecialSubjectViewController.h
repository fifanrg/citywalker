//
//  SpecialSubjectViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-20.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "ASIHTTPRequestDelegate.h"
#import "SVSegmentedControl.h"
@interface SpecialSubjectViewController : RootViewController
{
    NSMutableDictionary *_dataSourceInfo;
    NSMutableArray *_currentDataList;
    NSString *_currentDataObject;
    NSArray *relatedObjectList;
    SVSegmentedControl *_objectSelectBar;
    NSArray *_requestObjectUrlList;
    BOOL _didDisplayed;
    
    
    NSMutableDictionary *_relatedObjectCurrentPage;
    
}


@property(nonatomic,retain)NSString *currentObject;
@property(nonatomic,retain)NSMutableDictionary *dataSourceInfo;

@end
