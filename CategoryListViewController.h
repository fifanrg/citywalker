//
//  CategoryListViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-7-19.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "ASIHTTPRequestDelegate.h"
#import "DemoTableController.h"
#import "SuburbAreaSelectionsViewController.h"

typedef  enum{
    houseType = 1,
    carType = 2,
    businessType = 3,
    bookType = 4,
    newProductType = 5,
    employmentType = 6,
    fleaMarketType = 7,
    serviveType = 8
}CategoryType;



@interface CategoryListViewController : RootViewController<suburbviewDeledate>

{
    NSMutableArray *_dataSource;
    NSInteger _refreshCount;
    NSInteger _loadMoreCount;
    int currentPage;

    
    Boolean isrefreshOrloadMore;
    Boolean isfirstTimeRefresh;
    Boolean isfilterOption;
    
    NSMutableDictionary *_wholeDicDataSource;
    
    
    NSMutableDictionary *_currentPageDic;
    
    NSMutableDictionary *_listUrlDic;
    
    
}
@property(nonatomic,retain)NSString *title;
@property(nonatomic)CategoryType categoryType;
@property(nonatomic,retain)NSMutableArray *dataSource;
@property(nonatomic,retain)NSString *currentlyUrl;
@property(nonatomic,retain)NSMutableDictionary *filterDic;


@property(nonatomic,retain)NSMutableDictionary *wholeDicDataSource;
@property(nonatomic,retain)NSMutableDictionary *currentPageDic;
@property(nonatomic,retain)NSMutableDictionary *listUrlDic;
@end
