//
//  CategoryMainViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-7-19.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
@interface CategoryMainViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_dataSource;
     UITableView *_tableView;
    
    NSMutableDictionary *_wholeDataSource;
    
    NSMutableDictionary *_currentPageDic;
    
    NSMutableDictionary *_listUrlDic;
}


@property(retain,nonatomic)UITableView *tableView;
@property(retain,nonatomic)NSMutableArray *dataSource;
@property(retain,nonatomic)NSMutableDictionary *wholeDataSource;

@end
