//
//  UtilTools.m
//  CityWalker
//
//  Created by Jianing Ren on 13-4-10.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "UtilTools.h"

@implementation UtilTools


#pragma mark - 压缩图片
+(UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}


#pragma mark 保存图片到document
+(void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName
{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    // Now we get the full path to the file
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    // and then we write it out
    [imageData writeToFile:fullPathToFile atomically:NO];
}


+(CGFloat)getProportionalHeightFromImage:(UIImage *)image{
    CGFloat width = image.size.width;
    CGFloat rotio = width/300.0f;
    CGFloat height = image.size.height;
    return (height/rotio);
    
}

@end
