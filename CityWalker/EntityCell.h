//
//  EntityCell.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-15.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntityListViewController.h"
#import "AttributedLabel.h"
typedef  enum{
    newsCell,
    photoCell,
    subjectCell,
    rateCell,
    categoryMainCell
}CellType;
@interface EntityCell : UITableViewCell<UIScrollViewDelegate>

{
    UILabel *titleLabel;
    UILabel *dateLabel;
    UILabel *sourceLabel;
    
    UIScrollView *imageGallary;
    NSArray *imageArr;
    UIPageControl *pageController;
    
    //to tag if this entity has been read before
    UIImageView *unreadTag;
    
    
    //typed subjectCell
    UIImageView *coverImageView;
    UIImageView *subjectTypeTagImageView;
    
    
    //typed rateCell
    UILabel *areaLabel;
    UILabel *typedLabel;
    UILabel *averageLabel;
    UILabel *phoneLabel;
    
    UILabel *areaTitleLabel;
    UILabel *typedTitleLabel;
    UILabel *averageTitleLabel;
    UILabel *phoneTitleLabel;
    

}

@property(nonatomic)CellType cellType;
@property(nonatomic,retain)UILabel *titleLabel;


-(void)setTitle:(NSString *)title;
-(void)setDateLabel:(NSString *)date;
-(void)setSourceLabel:(NSString *)source;
-(void)initCell;
-(void)setGallaryPhtos:(NSArray *)images;
- (void)initCellWithType:(CellType)type;
-(void)setFrameToImageView;

-(UIScrollView *)getImageGallary;
-(UIPageControl *)getpageController;


//Remove unreadTag
-(void)removeunReadTag:(UIImageView *)tagImage;


-(void)setUnreadTag;

//subject methods
- (void)setSubjectTagPhoto:(UIImage *)tagImage withType:(NSString *)type;
- (void)setSubjectCover:(NSString *)url;


//RateComment Mthods
-(void)setAreaLabel:(NSString *)area;
-(void)setTypedLabel:(NSString *)type;
-(void)setAverageLabel:(NSString *)average;
-(void)setphoneLabel:(NSString *)phone;


//Category Methods
- (void)setCategoryMainIcon:(NSString *)photoName;
-(void)setLocation:(NSString *)source;

- (void)animationForIndexPath:(NSIndexPath *)indexPath;
@end
