//
//  AppDelegate.m
//  CityWalker
//
//  Created by Jianing Ren on 13-4-10.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "EntityListViewController.h"
#import "SuburbAreaSelectionsViewController.h"
#import "SpecialSubjectViewController.h"
#import "PublicCommentsViewController.h"
#import "CategoryMainViewController.h"
#import "UtilDB.h"
#import "MyPostViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
@implementation AppDelegate
@synthesize tabBarController = _tabBarController;
- (void)dealloc
{
    [_window release];
    [_viewController release];
    [_tabBarController release];
    [super dealloc];
}

//微信分享
-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{

    return [ShareSDK handleOpenURL:url wxDelegate:self];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    [ShareSDK registerApp:@"a1aa87c91e8"];
    
    
    [ShareSDK connectWeChatWithAppId:@"wxaa8ac9f4b2876b5a" wechatCls:[WXApi class]];
    
    
    
    
    
//    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
//    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
//    
//    EntityListViewController *newsController = [[[EntityListViewController alloc]initWithNibName:@"EntityListViewController" bundle:nil]autorelease];
//    
//    
//    
//    UINavigationController *firstNavigationController = [[UINavigationController alloc]initWithRootViewController:newsController];
////    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
//    self.window.rootViewController =firstNavigationController;
    
    EntityListViewController *newsViewController = [[[EntityListViewController alloc] initWithNibName:@"EntityListViewController" bundle:nil] autorelease];
    
    UINavigationController *firstNavigationController = [[UINavigationController alloc]initWithRootViewController:newsViewController];
    
//    newsViewController.title= @"新闻";
    
    firstNavigationController.tabBarItem.title = @"新闻";
    
    [firstNavigationController.tabBarItem setImage:[UIImage imageNamed:@"add"]];
//    newsViewController.navigationItem.title = @"悉尼在线";
    
    firstNavigationController.navigationBar.tintColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    
    
    [UtilDB initDB];
    
//    SuburbAreaSelectionsViewController *suburbAreaSelectionsViewController = [[[SuburbAreaSelectionsViewController alloc] initWithNibName:@"SuburbAreaSelectionsViewController" bundle:nil] autorelease];
//    
//    UINavigationController *secondNavigationController = [[UINavigationController alloc]initWithRootViewController:suburbAreaSelectionsViewController];

    
    
    
    CategoryMainViewController *categoryMainViewController = [[[CategoryMainViewController alloc] initWithNibName:@"CategoryMainViewController" bundle:nil] autorelease];
    
    UINavigationController *secondNavigationController = [[UINavigationController alloc]initWithRootViewController:categoryMainViewController];
    
    
    secondNavigationController.tabBarItem.title = @"分类信息";

//    categoryMainViewController.navigationItem.title = @"悉尼在线";
    
    secondNavigationController.navigationBar.tintColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];

    
    
    
    SpecialSubjectViewController *specialSubjectViewController = [[[SpecialSubjectViewController alloc] initWithNibName:@"SpecialSubjectViewController" bundle:nil] autorelease];
    
    UINavigationController *thirdNavigationController = [[UINavigationController alloc]initWithRootViewController:specialSubjectViewController];
    
    specialSubjectViewController.title = @"专栏";
    
//    specialSubjectViewController.navigationItem.title =@"悉尼在线";
    
    thirdNavigationController.navigationBar.tintColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    
    
    PublicCommentsViewController *publicCommentsViewController = [[[PublicCommentsViewController alloc] initWithNibName:@"PublicCommentsViewController" bundle:nil] autorelease];
    
    UINavigationController *fourthNavigationController = [[UINavigationController alloc]initWithRootViewController:publicCommentsViewController];
    
//    publicCommentsViewController.navigationItem.title =@"悉尼在线";
    
    fourthNavigationController.title = @"大众点评";
    
    
    fourthNavigationController.navigationBar.tintColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    
    
    
    
    MyPostViewController *myPostViewController = [[[MyPostViewController alloc] initWithNibName:@"MyPostViewController" bundle:nil] autorelease];
    
    UINavigationController *fifthNavigationController = [[UINavigationController alloc]initWithRootViewController:myPostViewController];
    
//    myPostViewController.navigationItem.title =@"悉尼在线";
    
    fifthNavigationController.title = @"我的发布";
    
    
    fifthNavigationController.navigationBar.tintColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    
    
    
    [firstNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigator@2x.png"] forBarMetrics:UIBarMetricsDefault];
     [secondNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigator@2x.png"] forBarMetrics:UIBarMetricsDefault];
     [thirdNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigator@2x.png"] forBarMetrics:UIBarMetricsDefault];
     [fourthNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigator@2x.png"] forBarMetrics:UIBarMetricsDefault];
     [fifthNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigator@2x.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    
    [_tabBarController setDelegate:self];
    
    _tabBarController.viewControllers = [NSArray arrayWithObjects:firstNavigationController,secondNavigationController,thirdNavigationController,fourthNavigationController,fifthNavigationController,nil];
    [firstNavigationController release];
    [secondNavigationController release];
    [thirdNavigationController release];
    [fourthNavigationController release];
    [fifthNavigationController release];
    
    self.window.rootViewController = _tabBarController;
    
//    self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund.png"]];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
