//
//  UtilDB.m
//  CityWalker
//
//  Created by Jianing Ren on 13-5-16.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "UtilDB.h"
#import "FMDatabase.h"

@implementation UtilDB




+(BOOL)ifDBExist{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *savePath = [path stringByAppendingPathComponent:DB_PATH];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:savePath];
}


+(void)initDB{
    
    BOOL ifDBexist = [self ifDBExist];
    if (!ifDBexist) {
        
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *savePath = [path stringByAppendingPathComponent:DB_PATH];
        sqlite3 *database;
        
        if (sqlite3_open([savePath UTF8String], &database) == SQLITE_OK)
        {
            NSLog(@"open success");
            FMDatabase *db = [FMDatabase databaseWithPath:DB_PATH];
            db.logsErrors = YES;
            if (![db open]) {
                NSLog(@"Database open fail.");
                return;
            }
            
            [db setShouldCacheStatements:YES];
            if(![self isTableOK:@"suburbs"])
            {
                [db executeUpdate:@"CREATE TABLE suburbs(id INTEGER primary key autoincrement, name INTEGER,index_id TEXT) "];
                NSLog(@"创建完成");
            }
            
            if(![self isTableOK:@"entity"])
            {   
                [db executeUpdate:@"CREATE TABLE entity(id INTEGER primary key autoincrement, item_id INTEGER,category TEXT) "];
                NSLog(@"创建完成");
            }
            if(![self isTableOK:@"cache_news"])
            {
                [db executeUpdate:@"CREATE TABLE cache_news(id_primary INTEGER primary key autoincrement,id INTEGER, selected_index INTEGER,title TEXT,link TEXT, publish TEXT, source TEXT , is_pic_link INTEGER, large TEXT)"];
                NSLog(@"创建完成");
            }
            
//            if(![self isTableOK:@"cache_category"])
//            {
//                [db executeUpdate:@"CREATE TABLE cache_category(id INTEGER primary key autoincrement, item_id INTEGER,category TEXT) "];
//                NSLog(@"创建完成");
//            }
//            if(![self isTableOK:@"cache_special"])
//            {
//                [db executeUpdate:@"CREATE TABLE cache_special(id INTEGER primary key autoincrement, item_id INTEGER,category TEXT) "];
//                NSLog(@"创建完成");
//            }
//            if(![self isTableOK:@"cache_rate"])
//            {   
//                [db executeUpdate:@"CREATE TABLE cache_rate(id INTEGER primary key autoincrement, item_id INTEGER,category TEXT) "];
//                NSLog(@"创建完成");
//            }
            
            
            if(![self isTableOK:@"my_post"])
            {
                [db executeUpdate:@"CREATE TABLE my_post(id INTEGER primary key autoincrement, item_id INTEGER,title TEXT, address TEXT, item_url TEXT,catId TEXT)"];
                NSLog(@"创建完成");
            }
            [db close];
            
        }
        else {
            sqlite3_close(database);
            NSLog(@"open failed");
        }
    }
}







+(BOOL) isTableOK:(NSString *)tableName
{
    FMDatabase *db = [FMDatabase databaseWithPath:DB_PATH];
    FMResultSet *rs = [db executeQuery:@"select count(*) as 'count' from sqlite_master where type ='table' and name = ?", tableName];
    while ([rs next])
    {
        // just print out what we've got in a number of formats.
        NSInteger count = [rs intForColumn:@"count"];
        NSLog(@"isTableOK %d", count);
        
        if (0 == count)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    return NO;
}

+(BOOL)checkreadTag:(NSDictionary *)dic withType:(NSString *)category{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
//        [db beginTransaction];
        
            NSString *itemId = [dic objectForKey:@"id"];
            FMResultSet *rs = [db executeQuery:@"SELECT * from entity where item_id = ? and category = ?",itemId,category];
            NSArray *resultArr =  [rs resultArray];
            if ( !resultArr || [resultArr count]==0){
                return NO;
            }
       
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            
        }

//        [db commit];
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
    
    return YES;
}

+(void)insertReadRecord:(NSDictionary *)dic withType:(NSString *)category{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        [db beginTransaction];
        NSString *itemId = [dic objectForKey:@"id"];
        FMResultSet *rs = [db executeQuery:@"SELECT * from entity where item_id = ? and category = ?",itemId,category];
        NSArray *resultArr =  [rs resultArray];
        if ( !resultArr || [resultArr count]==0){
            [db executeUpdate:@"INSERT INTO entity (item_id,category) VALUES (?,?)",itemId,category];
        }
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            
        }
        [db commit];
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
}


//Insert newsCache data, only keep 20 records for cache porpuse
+(void)insertNewsCacheData:(NSArray *)newsCache bySelected_index:(int)selectedIndex ifisGallary:(BOOL)isGallary{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        [db beginTransaction];
        
//        [db executeUpdate:@"CREATE TABLE cache_news(id INTEGER primary key ,selected_index INTEGER,title TEXT,link TEXT, public_up TEXT, news_source TEXT) "];
        if (newsCache){
            if (!isGallary) {
                [db executeUpdate:@"delete from cache_news where selected_index = ? and is_pic_link = 0",[NSNumber numberWithInt:selectedIndex]];
            }else{
                 [db executeUpdate:@"delete from cache_news where selected_index = ? and is_pic_link = 1",[NSNumber numberWithInt:selectedIndex]];
            }
            for (NSDictionary *entityDic in newsCache) {
                if (!isGallary) {
                     [db executeUpdate:@"INSERT INTO cache_news (id,selected_index,title,link,publish,source,is_pic_link) VALUES (?,?,?,?,?,?,?)",[entityDic objectForKey:@"id"],[NSNumber numberWithInt:selectedIndex],[entityDic objectForKey:@"title"],[entityDic objectForKey:@"link"],[entityDic objectForKey:@"publish"],[entityDic objectForKey:@"source"],[NSNumber numberWithInt:0]];
                }else{
                  [db executeUpdate:@"INSERT INTO cache_news (id,selected_index,title,link,publish,source,is_pic_link,large) VALUES (?,?,?,?,?,?,?,?)",[entityDic objectForKey:@"id"],[NSNumber numberWithInt:selectedIndex],[entityDic objectForKey:@"title"],[entityDic objectForKey:@"link"],[entityDic objectForKey:@"publish"],[entityDic objectForKey:@"source"],[NSNumber numberWithInt:1],[entityDic objectForKey:@"large"]];
                }
            }
        }
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        }
        [db commit];
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
}



+(NSMutableArray *)getNewsCacheDatabySelected_index:(int)selectedIndex ifisGallary:(BOOL)isGallary{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    NSMutableArray *cacheNews = nil;
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        
        [db beginTransaction];
        cacheNews  = [[NSMutableArray alloc]init];
        FMResultSet *rs;
        if (!isGallary) {
             rs = [db executeQuery:@"SELECT * from cache_news where selected_index = ? and is_pic_link = 0",[NSNumber numberWithInt:selectedIndex]];
        }else{
             rs = [db executeQuery:@"SELECT * from cache_news where selected_index = ? and is_pic_link = 1",[NSNumber numberWithInt:selectedIndex]];
        }
        while ([rs next]){
            NSDictionary *dictionaryCacheNews = [rs resultDictionary];
            NSLog(@"get cacheNews:%@",dictionaryCacheNews);
            [cacheNews addObject:dictionaryCacheNews];
            [dictionaryCacheNews release];
        }
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
    return cacheNews;


}



+(NSArray *)getSuburbInfo{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    NSMutableArray *allsuburbs = nil;
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        
       [db beginTransaction];
       allsuburbs  = [[NSMutableArray alloc]init];
       FMResultSet *rs = [db executeQuery:@"SELECT * from suburbs "];
        while ([rs next]){
            NSMutableDictionary *suburb = [[NSMutableDictionary alloc]init];
            [suburb setValue:[rs stringForColumn:@"name"] forKey:@"name"];
            [suburb setValue:[rs stringForColumn:@"index_id"] forKey:@"index_id"];
            [allsuburbs addObject:suburb];
            [suburb release];
        }
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
    return allsuburbs;
}



+(void)insertSuburbData:(NSArray *)suburbs{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        
        [db beginTransaction];
      
        for (NSString *suburbString in suburbs) {
            int index = -1;
            if ([suburbString hasPrefix:@"A"] || [suburbString hasPrefix:@"a"]) {
                index = 0;
            }else if ([suburbString hasPrefix:@"B"] || [suburbString hasPrefix:@"b"]){
                index = 1;
            }else if ([suburbString hasPrefix:@"C"] || [suburbString hasPrefix:@"c"]){
                index = 2;
            }else if ([suburbString hasPrefix:@"D"] || [suburbString hasPrefix:@"d"]){
                index = 3;
            }else if ([suburbString hasPrefix:@"E"] || [suburbString hasPrefix:@"e"]){
                index = 4;
            }else if ([suburbString hasPrefix:@"F"] || [suburbString hasPrefix:@"f"]){
                index = 5;
            }else if ([suburbString hasPrefix:@"G"] || [suburbString hasPrefix:@"g"]){
                index = 6;
            }else if ([suburbString hasPrefix:@"H"] || [suburbString hasPrefix:@"h"]){
                index = 7;
            }else if ([suburbString hasPrefix:@"I"] || [suburbString hasPrefix:@"i"]){
                index = 8;
            }else if ([suburbString hasPrefix:@"J"] || [suburbString hasPrefix:@"j"]){
                index = 9;
            }else if ([suburbString hasPrefix:@"K"] || [suburbString hasPrefix:@"K"]){
                index = 10;
            }else if ([suburbString hasPrefix:@"L"] || [suburbString hasPrefix:@"l"]){
                index = 11;
            }else if ([suburbString hasPrefix:@"M"] || [suburbString hasPrefix:@"m"]){
                index = 12;
            }else if ([suburbString hasPrefix:@"N"] || [suburbString hasPrefix:@"n"]){
                index = 13;
            }else if ([suburbString hasPrefix:@"O"] || [suburbString hasPrefix:@"o"]){
                index = 14;
            }else if ([suburbString hasPrefix:@"P"] || [suburbString hasPrefix:@"p"]){
                index = 15;
            }else if ([suburbString hasPrefix:@"Q"] || [suburbString hasPrefix:@"q"]){
                index = 16;
            }else if ([suburbString hasPrefix:@"R"] || [suburbString hasPrefix:@"r"]){
                index = 17;
            }else if ([suburbString hasPrefix:@"S"] || [suburbString hasPrefix:@"s"]){
                index = 18;
            }else if ([suburbString hasPrefix:@"T"] || [suburbString hasPrefix:@"t"]){
                index = 19;
            }else if ([suburbString hasPrefix:@"U"] || [suburbString hasPrefix:@"u"]){
                index = 20;
            }else if ([suburbString hasPrefix:@"V"] || [suburbString hasPrefix:@"v"]){
                index = 21;
            }else if ([suburbString hasPrefix:@"W"] || [suburbString hasPrefix:@"w"]){
                index = 22;
            }else if ([suburbString hasPrefix:@"X"] || [suburbString hasPrefix:@"x"]){
                index = 23;
            }else if ([suburbString hasPrefix:@"Y"] || [suburbString hasPrefix:@"y"]){
                index = 24;
            }else if ([suburbString hasPrefix:@"Z"] || [suburbString hasPrefix:@"z"]){
                index = 25;
            }
            else{
                index = 26;
            }
          
//             [db executeUpdate:@"INSERT INTO t_structure (field_name,field_type,parent_field,root_field,field_order,datatype_name,UsageOnCreate,UsageOnDestroy,UsageOnGet,UsageOnUpdate,isCustomField,MaxLength) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",[fieldDic objectForKey:@"Name"],[fieldDic objectForKey:@"DataType"]];
            
            NSString *indexString = [NSString stringWithFormat:@"%d",index];
            
           [db executeUpdate:@"INSERT INTO suburbs (name,index_id) VALUES (?,?)",suburbString,indexString];
            
        }
        
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            
        }
        
        [db commit];
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
}


+(NSMutableArray *)getMyPost{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    NSMutableArray *allPosts = nil;
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        [db beginTransaction];
        allPosts  = [[NSMutableArray alloc]init];
        FMResultSet *rs = [db executeQuery:@"SELECT * from my_post"];
        while ([rs next]){
            NSMutableDictionary *post = [[NSMutableDictionary alloc]init];
            [post setValue:[rs stringForColumn:@"item_id"] forKey:@"item_id"];
            [post setValue:[rs stringForColumn:@"title"] forKey:@"title"];
            [post setValue:[rs stringForColumn:@"address"] forKey:@"address"];
            [post setValue:[rs stringForColumn:@"item_url"] forKey:@"item_url"];
            [post setValue:[rs stringForColumn:@"catId"] forKey:@"catId"];
            [allPosts addObject:post];
            [post release];
        }
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
    return allPosts;
}



+(void)saveMyPost:(NSMutableDictionary *)saveDic{
    FMDatabase* db = [FMDatabase databaseWithPath:DB_PATH];
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        db.logsErrors = YES;
        //insert
        [db beginTransaction];
        NSString *itemId = [saveDic objectForKey:@"item_id"];
        NSString *title = [saveDic objectForKey:@"title"];
        NSString *address = [saveDic objectForKey:@"address"];
        NSString *itemUrl = [saveDic objectForKey:@"item_url"];
        NSString *catId = [saveDic objectForKey:@"catId"];
        
//        CREATE TABLE my_post(id INTEGER primary key autoincrement, item_id INTEGER,address TEXT, item_url TEXT
        [db executeUpdate:@"INSERT INTO my_post (item_id,title,address,item_url,catId) VALUES (?,?,?,?,?)",itemId,title,address,itemUrl,catId];
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        }
        [db commit];
        [db close];
    }else{
        NSLog(@"Could not open db when initialzing structure.");
    }
    
}






+(float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    return sizeToFit.height;
}


+(NSString *)setStringValue:(id)value{
    if (value == [NSNull null]) {
        value = [NSString stringWithFormat:@"无注明"];
    }else{
        if ([value isEqualToString:@""]) {
            value = [NSString stringWithFormat:@"无注明"];
        }else{
          value =  [value stringByReplacingOccurrencesOfString:@"<br />" withString:@""];
        }
    }
    return value;
}


+(NSString *)returnDateString:(NSString *)dateString{
    NSRange _range = [dateString rangeOfString:@" "];
    if (_range.location != NSNotFound) {
        NSArray  * array= [dateString componentsSeparatedByString:@" "];
        NSString *string = [array objectAtIndex:0];
        
        NSRange _range2 = [string rangeOfString:@"-"];
        if (_range2.location != NSNotFound) {
            NSArray *array2 = [string componentsSeparatedByString:@"-"];
            if ([array2 count]==3) {
                NSString *rightFormateDate = [NSString stringWithFormat:@"%@年%@月%@日",[array2 objectAtIndex:0],[array2 objectAtIndex:1],[array2 objectAtIndex:2]];
                return rightFormateDate;
            }else
                return string;
        }else
            return string;
    }else {
        return dateString;
    }
}


+ (NSString *)escape:(NSString *)text
{
    return [(NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                NULL,
                                                                (CFStringRef)text,
                                                                NULL,
                                                                (CFStringRef)@"!*'();@&=+$,?%#[]",
                                                                CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)) autorelease];
}

@end
