//
//  CategoryDetailViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-7-23.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "CategoryDetailViewController.h"
#import "ASIHTTPRequest.h"
#import "TFHpple.h"
#import "UIImageView+WebCache.h"
#import "UtilDB.h"
#import "UtilTools.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "WCAlertView.h"
#import<CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "AttributedLabel.h"

@interface CategoryDetailViewController ()

@end

@implementation CategoryDetailViewController
@synthesize itemId;
@synthesize typeId;
@synthesize linkUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    CGFloat height = self.view.frame.origin.y;
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    
    mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, height, self.view.frame.size.width, rect_screen.size.height)];
    
    [mainScrollView setScrollEnabled:YES];
    [self.view addSubview:mainScrollView];
    
    [mainScrollView release];
    
    NSLog(@"MainScrollView :%@",mainScrollView);
    
    totalHeight = 50.0;
    
//    UISegmentedControl *leftSeg = [[UISegmentedControl alloc] initWithItems:@[@"Back"]];
//    [leftSeg setFrame:CGRectMake(0, 0, 100, 32)];
//    [leftSeg addTarget:self action:@selector(leftButtonItemMethod:) forControlEvents:UIControlEventValueChanged];
//    leftSeg.segmentedControlStyle = UISegmentedControlStyleBar;
//    leftSeg.momentary = YES;
//    
//    UIBarButtonItem *segmentBarItemleft = [[UIBarButtonItem alloc] initWithCustomView:leftSeg];
//    [leftSeg release];
//    self.navigationItem.leftBarButtonItem = segmentBarItemleft;
//    [segmentBarItemleft release];
    
    
    
    [self startWebrequest];
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
}




-(void)startWebrequest{
    //request http resource and return Json dictionary
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    
    
    NSString *urlString = nil;
    
//    itemId = @"6159";
//    typeId = @"1";
    if (linkUrl) {
        
//        urlString = [NSString stringWithFormat:@"http://www.insyd.com.au/classifieds/ad/%@/%@?format=json",typeId,itemId];
        
        
        urlString = linkUrl;
        
        
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:urlString]];
        [request setShouldContinueWhenAppEntersBackground:YES];
        [request setTimeOutSeconds:200];
        NSLog(@"entityUrl:%@",urlString);
        
        [request setCompletionBlock:^{
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
            NSArray *array = [responseData objectForKey:@"items"];
            NSDictionary *datadic = [array objectAtIndex:0];
            NSLog(@"responseData:%@",array);
            if (responseData) {
                
//                NSLog(@"title :%@",[responseData objectForKey:@"items"]);
                [self generateUIbyDic:datadic];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }];
        [request setFailedBlock:^{
            NSLog(@"error:%@",request.error);
            [WCAlertView showAlertWithTitle:@"Notice"
                                    message:@"Error,Please try later."
                         customizationBlock:^(WCAlertView *alertView) {
                             alertView.style = WCAlertViewStyleVioletHatched;
                             
                         } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                             
                         } cancelButtonTitle:@"Okay" otherButtonTitles:nil];

        }];
        
        
        
        [request startAsynchronous];
        [request release];
    }
}



-(void)generateUIbyDic:(NSDictionary *)dataDic{
   NSString *imagesString  = [dataDic objectForKey:@"image"];
    
    NSArray *gallaryArray = [imagesString componentsSeparatedByString:@";"];
    
    CGRect gallaryScrollViewFrame = CGRectMake(10, 10, 300, 180);
    
    if (![imagesString isEqualToString:@""] && [gallaryArray count]>1) {
        
        
        UIScrollView *gallaryScrollView  = [[UIScrollView alloc]initWithFrame:gallaryScrollViewFrame];
        gallaryScrollView.contentSize = CGSizeMake([gallaryArray count]*300, 180.);
        
        CGRect pageControllerFrame = CGRectMake(120, 160, 50, 30);
        
        
        gallaryScrollView.delegate = self;
        gallaryScrollView.showsHorizontalScrollIndicator = FALSE;
        [gallaryScrollView setScrollEnabled:YES];
        [gallaryScrollView setPagingEnabled:YES];
        
        NSLog(@"pageController:%@",pageController);
        for (int i=0; i<[gallaryArray count]; i++) {
            NSString *decodeString = [UtilDB escape:[gallaryArray objectAtIndex:i]];
            NSLog(@"decodeString:%@",decodeString);
            NSURL *imageURL = [NSURL URLWithString:decodeString];
            
            UIImageView *img = [[UIImageView alloc] init];
            [img setImageWithURL:imageURL];
            
            img.frame = CGRectMake(i*300, 0, 300, 180);
            //        img.layer.borderColor = [[UIColor whiteColor]CGColor];
            //        img.layer.borderWidth = 5.0f;
            CGRect captionViewFrame = CGRectMake(0, img.frame.size.height-40, 300, 40);
            UILabel *captionView = [[UILabel alloc]initWithFrame:captionViewFrame];
            [captionView setUserInteractionEnabled:NO];
            [captionView setBackgroundColor:[UIColor blackColor]];
            captionView.alpha = 0.4;
            captionView.textColor = [UIColor whiteColor];
            [captionView setTextAlignment:UITextAlignmentLeft];
            captionView.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:16];
            captionView.numberOfLines = 2;
            captionView.minimumFontSize = 10;
            
            [img addSubview:captionView];
            [captionView release];
            [gallaryScrollView addSubview:img];
            [img release];
        }
        
        [mainScrollView addSubview:gallaryScrollView];
        [gallaryScrollView release];
        
        
        if (!pageController) {
            pageController = [[UIPageControl alloc]initWithFrame:pageControllerFrame];
            [mainScrollView addSubview:pageController];
            [mainScrollView bringSubviewToFront:pageController];
            [pageController release];
        }
        else
            [pageController setFrame:pageControllerFrame];
        
        pageController.numberOfPages = [gallaryArray count];
        pageController.currentPage = 0;
        [pageController setBackgroundColor:[UIColor clearColor]];
         
        
    }else{
        UIScrollView *gallaryScrollView  = [[UIScrollView alloc]initWithFrame:gallaryScrollViewFrame];
        [gallaryScrollView setBackgroundColor:[UIColor blackColor]];
        UIImageView *noimageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"no-image.png"]];
        [noimageView setFrame:CGRectMake(0, 0, 300, 180)];
        gallaryScrollView.contentSize = CGSizeMake(300, 180.);
        [gallaryScrollView addSubview:noimageView];
        [noimageView release];
        
        [mainScrollView addSubview:gallaryScrollView];
        [gallaryScrollView release];
    
    }
    
    //    NSString *detailTitle = [dataDic objectForKey:@"title"];
    CGFloat titlePheight = [UtilDB heightForString:[dataDic objectForKey:@"title"] fontSize:15 andWidth:300.0f];
    CGFloat originY = gallaryScrollViewFrame.size.height ;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, originY+10, 300, titlePheight+5)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:UITextAlignmentLeft];
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    titleLabel.numberOfLines = 2;
    titleLabel.minimumFontSize = 10;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = [dataDic objectForKey:@"title"];
    
    
    
    AttributedLabel *areaLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+10, 170, 30)];
    AttributedLabel *suisineLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10+areaLabel.frame.size.width+20, originY+titleLabel.frame.size.height+10, 100, 30)];
    AttributedLabel *priceLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+areaLabel.frame.size.height, 200, 30)];
    
    NSString *dateFormat = [UtilDB returnDateString:[dataDic objectForKey:@"publish"]];
 
    NSLog(@"%@ ",dateFormat);
    areaLabel.text = [NSString stringWithFormat:@"发布时间  %@",dateFormat];
    [areaLabel setColor:[UIColor redColor] fromIndex:0 length:4];
    [areaLabel setColor:[UIColor blackColor] fromIndex:5 length:dateFormat.length];
    
    [areaLabel setBackgroundColor:[UIColor clearColor]];
    [areaLabel setTextAlignment:UITextAlignmentLeft];
    
    areaLabel.numberOfLines = 2;
    areaLabel.minimumFontSize = 10;
    
    [areaLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:areaLabel.text.length];
    
    
    NSString *priceString = [UtilDB setStringValue:[dataDic objectForKey:@"price"]];
    suisineLabel.text = [NSString stringWithFormat:@"价钱  %@",priceString];
    
    [suisineLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [suisineLabel setColor:[UIColor blackColor] fromIndex:3 length:priceString.length];

    [suisineLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:suisineLabel.text.length];
    
    [suisineLabel setBackgroundColor:[UIColor clearColor]];
    [suisineLabel setTextAlignment:UITextAlignmentLeft];
    suisineLabel.numberOfLines = 2;
    suisineLabel.minimumFontSize = 10;

    
    NSString *contactString = [UtilDB setStringValue:[dataDic objectForKey:@"contact"]];
    priceLabel.text = [NSString stringWithFormat:@"联系人  %@",contactString];
    
    
    [priceLabel setColor:[UIColor redColor] fromIndex:0 length:3];
    [priceLabel setColor:[UIColor blackColor] fromIndex:3 length:contactString.length];
    
    [priceLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:priceLabel.text.length];

    [priceLabel setBackgroundColor:[UIColor clearColor]];
    [priceLabel setTextAlignment:UITextAlignmentLeft];
    priceLabel.numberOfLines = 2;
    priceLabel.minimumFontSize = 10;

    
    
    
    AttributedLabel *phoneLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+priceLabel.frame.size.height+areaLabel.frame.size.height-10, 300, 30)];
    
    NSString *addressString = [UtilDB setStringValue:[dataDic objectForKey:@"address"]];
    
    phoneLabel.text = [NSString stringWithFormat:@"地址 %@",addressString];
    
    
    [phoneLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [phoneLabel setColor:[UIColor blackColor] fromIndex:3 length:addressString.length];
    
    [phoneLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:phoneLabel.text.length];
    
    [phoneLabel setBackgroundColor:[UIColor clearColor]];
    [phoneLabel setTextAlignment:UITextAlignmentLeft];
    phoneLabel.minimumFontSize = 10;
       
    
    
    
    CGFloat adressPheight = [UtilDB heightForString:[dataDic objectForKey:@"contact"] fontSize:14 andWidth:300.0f];
    
    AttributedLabel *addressLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+priceLabel.frame.size.height+10+phoneLabel.frame.size.height, 300, adressPheight)];
    
    NSString *contacts = [UtilDB setStringValue:[dataDic objectForKey:@"contact"]];
     addressLabel.text = [NSString stringWithFormat:@"联系 %@",contacts];
    
    [addressLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [addressLabel setColor:[UIColor blackColor] fromIndex:3 length:contacts.length];
    
    [addressLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:addressLabel.text.length];

    
    [addressLabel setBackgroundColor:[UIColor clearColor]];
    [addressLabel setTextAlignment:UITextAlignmentLeft];
    addressLabel.numberOfLines = 2;
   
    
    
    [mainScrollView addSubview:titleLabel];
    [titleLabel release];
    [mainScrollView addSubview:areaLabel];
    [areaLabel release];
    [mainScrollView addSubview:suisineLabel];
    [suisineLabel release];
    [mainScrollView addSubview:priceLabel];
    [priceLabel release];
    [mainScrollView addSubview:phoneLabel];
    [phoneLabel release];
    [mainScrollView addSubview:addressLabel];
    [addressLabel release];
    
    CGFloat commentStartOriginY = addressLabel.frame.origin.y + addressLabel.frame.size.height+10;
    
    UILabel *commentLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, commentStartOriginY, 300, 20)];
    
    [commentLabel setBackgroundColor:[UIColor colorWithRed:86.0/255.0 green:95.0/255.0 blue:108.0/255.0 alpha:0.8]];
    [commentLabel setTextAlignment:UITextAlignmentLeft];
    commentLabel.font = [UIFont boldSystemFontOfSize:14];
    commentLabel.numberOfLines = 2;
    commentLabel.minimumFontSize = 10;
    commentLabel.textColor = [UIColor whiteColor];
    
    commentLabel.text = @"描 述";
    
    [mainScrollView addSubview:commentLabel];
    [commentLabel release];
    
    NSString *description = [dataDic objectForKey:@"content"];
    description = [description stringByReplacingOccurrencesOfString:@"<br />" withString:@""];
    commentStartOriginY = commentStartOriginY+20;
 
    
    CGFloat descriptionPheight = [UtilDB heightForString:[dataDic objectForKey:@"content"] fontSize:15 andWidth:300.0f];
    
    
    UILabel *commentContentLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, commentStartOriginY, 300, descriptionPheight+20)];
    
    [commentContentLabel setTextAlignment:UITextAlignmentLeft];
    commentContentLabel.font = [UIFont boldSystemFontOfSize:15];
    commentContentLabel.numberOfLines = 10000;
    commentContentLabel.minimumFontSize = 10;
    commentContentLabel.textColor = [UIColor colorWithRed:136.0/255.0 green:143.0/255.0 blue:151.0/256.0 alpha:1];
    commentContentLabel.text = description;
    [commentContentLabel setBackgroundColor:[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:0.8]];
    
    [mainScrollView addSubview:commentContentLabel];
    [commentContentLabel release];
    
    commentStartOriginY = commentStartOriginY+commentContentLabel.frame.size.height+20;
    
    
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, commentStartOriginY+94)];
}




- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    UIScrollView *gallaryScrollView = sender;
    
    int page = gallaryScrollView.contentOffset.x / 300;
    pageController.currentPage = page;
    
}




-(void)leftButtonItemMethod:(id)sender {
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    int index = segmentedControl.selectedSegmentIndex;
    
    if (index==0) {
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.7;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        
        animation.type = @"pageCurl";
        animation.subtype = kCATransitionFromLeft;
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
        [self.navigationController popViewControllerAnimated:NO];
        
    }
}








- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
