//
//  EntityListViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-15.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "ASIHTTPRequestDelegate.h"
#import "SVSegmentedControl.h"
typedef enum{
    ListNews,
    ListSpecial
} EntityType;

@interface EntityListViewController : RootViewController<ASIHTTPRequestDelegate,UIGestureRecognizerDelegate>
{
    NSMutableDictionary *_dataSourceInfo;
    
    NSMutableArray *_currentDataList;
	NSString *_currentDataObject;
    NSArray *relatedObjectList;
    SVSegmentedControl *_objectSelectBar;
    NSArray *_requestObjectUrlList;
    
    NSMutableDictionary *_relatedObjectCurrentPage;
    
    NSMutableArray *_galarryArr;
    BOOL _didDisplayed;
}

@property(nonatomic,retain)NSMutableArray *galarryArr;
@property(nonatomic,retain)NSString *currentObject;
@property(nonatomic,retain)NSMutableDictionary *dataSourceInfo;
@property(nonatomic)EntityType entityType;
@end
