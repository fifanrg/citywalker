//
//  ViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-4-10.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBImagePickerController.h"
#import "UIViewController+KNSemiModal.h"
#import <QuartzCore/QuartzCore.h>
@interface ViewController : UIViewController<QBImagePickerControllerDelegate>{

    
    

}
    

- (IBAction)pickSinglePhoto:(id)sender;

- (IBAction)pickMultiplePhotos:(id)sender;

- (IBAction)pickSixphotos:(id)sender;

@end
