//
//  photoGallaryCell.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-15.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface photoGallaryCell : UITableViewCell
{
    UIScrollView *imageGallary;
    NSArray *imageArr;
}



-(void)setGallaryPhtos:(NSArray *)imageArr;



@end
