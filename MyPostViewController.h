//
//  MyPostViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-9-17.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilDB.h"



@interface MyPostViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_dataSource;
    UITableView *_tableView;
    
}


@property(retain,nonatomic)UITableView *tableView;
@property(retain,nonatomic)NSMutableArray *dataSource;

@end
