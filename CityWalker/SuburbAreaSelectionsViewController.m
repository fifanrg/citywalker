//
//  SuburbAreaSelectionsViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-5-19.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "SuburbAreaSelectionsViewController.h"
#import "UtilDB.h"
@interface SuburbAreaSelectionsViewController ()

@end

@implementation SuburbAreaSelectionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    if (!tableView) {
        tableView = [[UITableView alloc]initWithFrame:CGRectMake(self.view.bounds.origin.x,self.view.bounds.origin.y, 320, rect_screen.size.height-20) style:UITableViewStylePlain];
        [self.view addSubview:tableView];
        [tableView release];
        
    }
    
    if (!searchBar) {
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)] ;
        searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
        searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        searchBar.keyboardType = UIKeyboardTypeDefault;
        
        searchBar.delegate = self;
        tableView.tableHeaderView = searchBar;
        
        
        // Create the search display controller
        self.searchDC = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self] ;
        self.searchDC.searchResultsDataSource = self;
        self.searchDC.searchResultsDelegate = self;
        
    }    
    
    
    
    NSMutableArray *filterearray =  [[NSMutableArray alloc] init];
	self.filteredArray = filterearray;
	[filterearray release];
    
    
    NSMutableArray *totalDataSource =  [[NSMutableArray alloc] init];
	self.dataSource = totalDataSource;
	[totalDataSource release];
    
    
    tableView.dataSource = self;
    tableView.delegate = self;
    
    [self initialData];
    
    
    
    
//    UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(250, 10, 30, 30)];
//    
//    UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 30, 30)];
//    
//    [buttonView setBackgroundImage:[UIImage imageNamed:@"hamburger_info.png"] forState:UIControlStateNormal];
//    [buttonView addTarget:self action:@selector(dismissInfo) forControlEvents:UIControlEventTouchUpInside];
//    [leftContainer addSubview:buttonView];
//    [buttonView release];
//    
//    
//    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
//                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
//                                       target:nil action:nil];
//    negativeSpacer.width = 10;
//    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
//    [leftContainer release];
//    //    self.navigationItem.rightBarButtonItem = anotherButton;
//    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, anotherButton, nil];
//    [anotherButton release];
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}





-(void)dismissInfo{
    [self dismissViewControllerAnimated:YES completion:NULL];
}



-(void)initialData{
    self.sectionArray = [NSMutableArray array];
	for (int i = 0; i < 27; i++) [self.sectionArray addObject:[NSMutableArray array]];

    
      NSArray *allsuburbs =  [UtilDB getSuburbInfo];
    for (int i=0; i<[allsuburbs count]; i++) {
        
        NSDictionary *suburbDic = [allsuburbs objectAtIndex:i];
        int indexValue =  [((NSString *)[suburbDic objectForKey:@"index_id"]) intValue];
        NSString *suburbname = [suburbDic objectForKey:@"name"];
        [((NSMutableArray *)[self.sectionArray objectAtIndex:indexValue]) addObject:suburbname];
        
        [self.dataSource addObject:suburbname];
        
    }
    
    
//    [((NSMutableArray *)[self.sectionArray objectAtIndex:0]) addObjectsFromArray:aSection];
//      [((NSMutableArray *)[self.sectionArray objectAtIndex:1]) addObjectsFromArray:bSection];
//      [((NSMutableArray *)[self.sectionArray objectAtIndex:2]) addObjectsFromArray:cSection];
//      [((NSMutableArray *)[self.sectionArray objectAtIndex:3]) addObjectsFromArray:dSection];
    
   
    
   
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
	if(aTableView == tableView) return 27;
	return 1;
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)aTableView
{
    
	if (aTableView == tableView)  // regular table
	{
		NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
		for (int i = 0; i < 27; i++)
			if ([[self.sectionArray objectAtIndex:i] count])
				[indices addObject:[[ALPHA substringFromIndex:i] substringToIndex:1]];
		return indices;
	}
	else return nil; // search table
}


- (NSInteger)tableView:(UITableView *)atableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	if (title == UITableViewIndexSearch)
	{
		[tableView scrollRectToVisible:searchBar.frame animated:NO];
		return -1;
	}
	return [ALPHA rangeOfString:title].location;
}



- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section
{
	if (aTableView == tableView)
	{
		if ([[self.sectionArray objectAtIndex:section] count] == 0) return nil;
		return [NSString stringWithFormat:@"%@", [[ALPHA substringFromIndex:section] substringToIndex:1]];
	}
	else return nil;
}


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    
	if (aTableView == tableView) return [[self.sectionArray objectAtIndex:section] count];
	else
//		[self.filteredArray removeAllObjects];
       return  [self.filteredArray count];
    
    
    
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)asearchBar{
	searchBar.prompt = @"Please type your intersted area.";
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self.filteredArray removeAllObjects];
    
    for (NSString *stringInDatasource in self.dataSource) {
        NSString *lowercaseString = [stringInDatasource lowercaseString];
        NSString *lowercaseSearchText = [searchText lowercaseString];
        
        if ([lowercaseString hasPrefix:lowercaseSearchText]) {
            [self.filteredArray addObject:stringInDatasource];
        }
    }
}

// Via Jack Lucky
- (void)searchBarCancelButtonClicked:(UISearchBar *)asearchBar
{
	[searchBar setText:@""];
	searchBar.prompt = nil;
	[searchBar setFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
	tableView.tableHeaderView = searchBar;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }

    
	NSString *contactName;
	
	// Retrieve the crayon and its color
//    contactName = [[self.sectionArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	if (aTableView == tableView)
		contactName = [[self.sectionArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	else
		contactName = [self.filteredArray objectAtIndex:indexPath.row];
    
    
	cell.textLabel.text = [NSString stringWithCString:[contactName UTF8String] encoding:NSUTF8StringEncoding];
	
	NSLog(@"cell.textLabel.text%@",cell.textLabel.text);
	return cell;
}



- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int index = indexPath.row;
    



    if (self.suburbDelegate) {
        if (aTableView == tableView)  // regular table
        {
            [self.suburbDelegate selectSurburb:[[self.sectionArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
        }else{
            [self.suburbDelegate selectSurburb:[self.filteredArray objectAtIndex:index]];
        }
    }
   
    
    [self dismissInfo];

}

//-(void)viewDidUnload{
//    [searchBar release];
//    [self.searchDC release];
//
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
