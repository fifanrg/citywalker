//
//  ViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-4-10.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "ViewController.h"
#import "UtilTools.h"
#import "PostActivityViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickSinglePhoto:(id)sender {
//    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
//    imagePickerController.delegate = self;
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
//    [self presentViewController:navigationController animated:YES completion:NULL];
//    [imagePickerController release];
//    [navigationController release];

    PostActivityViewController *newcontroller = [[PostActivityViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newcontroller];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [newcontroller release];
    
    
    
}

- (IBAction)pickMultiplePhotos:(id)sender {
//    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
//    imagePickerController.delegate = self;
//    imagePickerController.allowsMultipleSelection = YES;
//    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
//    [self presentViewController:navigationController animated:YES completion:NULL];
//    [imagePickerController release];
//    [navigationController release];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
    UIButton *imagePickButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 100, 100)];
    [imagePickButton setTitle:@"Pick single photo" forState:UIControlStateNormal];
    [imagePickButton addTarget:self action:@selector(closeWindow) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:imagePickButton];
    [imagePickButton release];
    
    
    [self presentSemiView:view withOptions:@{
	 KNSemiModalOptionKeys.pushParentBack : @(NO),
	 KNSemiModalOptionKeys.parentAlpha : @(0.3),
	 KNSemiModalOptionKeys.animationDuration : @(0.3)
	 }];
    
}

-(void)closeWindow{
    [self dismissSemiModalView];
}


- (IBAction)pickSixphotos:(id)sender {
    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    
    imagePickerController.limitsMaximumNumberOfSelection = YES;
    imagePickerController.limitsMinimumNumberOfSelection = YES;
    imagePickerController.minimumNumberOfSelection = 1;
    imagePickerController.maximumNumberOfSelection = 3;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [imagePickerController release];
    [navigationController release];
}

#pragma mark - QBImagePickerControllerDelegate

- (void)imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingMediaWithInfo:(id)info
{
    if(imagePickerController.allowsMultipleSelection) {
        
        
    } else {
        NSDictionary *mediaInfo = (NSDictionary *)info;
        UIImage *image = [mediaInfo objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        UIImage *newImage = [UtilTools imageWithImageSimple:image scaledToSize:CGSizeMake(120.0, 120.0)];
        
        [UtilTools saveImage:newImage WithName:@"test.png"];
        UIImageView  *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(100, 200, 120, 120)];
        [imageView setImage:newImage];
        
        [self.view addSubview:imageView];
        [imageView release];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Cancelled");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString *)descriptionForSelectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"Select from photo Album...";
}

- (NSString *)descriptionForDeselectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"すべての写真の選択を解除";
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos
{
    return [NSString stringWithFormat:@"You can select %d photos.", numberOfPhotos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"ビデオ%d本", numberOfVideos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos numberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"写真%d枚、ビデオ%d本", numberOfPhotos, numberOfVideos];
}






@end
