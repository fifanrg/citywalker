//
//  UIView+UIView_Additional.h
//  Demo
//
//  Created by Jianing Ren on 12-10-25.
//  Copyright (c) 2012年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIView_Additional)
- (void)setSize:(CGSize)size;
- (void)setOrigin:(CGPoint)origin;
- (void)setSize:(CGSize)size keepCenter:(BOOL)_keepCenter;

@end
