//
//  CategoryDetailViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-7-23.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetailViewController : UIViewController<UIScrollViewDelegate>
{
    NSDictionary *entityDic;
    UIScrollView *mainScrollView;
    CGFloat totalHeight;
    UIPageControl *pageController;

}


@property(assign,nonatomic)NSString *typeId;
@property(assign,nonatomic)NSString *itemId;


@property(assign,nonatomic)NSString *linkUrl;
@end
