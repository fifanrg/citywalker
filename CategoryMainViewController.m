//
//  CategoryMainViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-7-19.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "CategoryMainViewController.h"
#import "EntityCell.h"
#import "UtilDB.h"
#import "MBProgressHUD.h"
#import "WCAlertView.h"
#import "CategoryListViewController.h"
#import "PostActivityViewController.h"
@interface CategoryMainViewController ()

@end

@implementation CategoryMainViewController
@synthesize dataSource = _dataSource;
@synthesize tableView = _tableView;
@synthesize wholeDataSource = _wholeDataSource;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
    if (!_tableView) {
        CGRect rect_screen = [[UIScreen mainScreen]bounds];
        _tableView.backgroundColor = [UIColor clearColor];
        
        [self initialData];
        
        [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                               self.view.bounds.origin.y,
                                               self.view.frame.size.width,
                                               rect_screen.size.height-108.0)];
        
        
//        UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(260, 0, 50, 44)];
//        
//        UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 50, 44)];
//        
//        [buttonView setTitle:@"发布" forState:UIControlStateNormal];
//        [buttonView addTarget:self action:@selector(postAd) forControlEvents:UIControlEventTouchUpInside];
//        [leftContainer addSubview:buttonView];
//        [buttonView release];
//        
//        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
//        self.navigationItem.rightBarButtonItem = anotherButton;
//        [anotherButton release];
        
        
        
        UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(250, 10, 66, 44)];
        
        UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 66, 44)];
        
        [buttonView setBackgroundImage:[UIImage imageNamed:@"post"] forState:UIControlStateNormal];
        [buttonView addTarget:self action:@selector(postAd) forControlEvents:UIControlEventTouchUpInside];
        [leftContainer addSubview:buttonView];
        [buttonView release];
        
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        
        negativeSpacer.width = 10;
        
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
        [leftContainer release];
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, anotherButton, nil];
        [anotherButton release];
        
        
        [self getSuburbInfoFromDatabase];
        
    }
    
    
    if (!_wholeDataSource) {
        _wholeDataSource = [[NSMutableDictionary alloc]initWithCapacity:1];
    }
    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


-(void)postAd{
    PostActivityViewController *newcontroller = [[PostActivityViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newcontroller];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [newcontroller release];

}


-(void)initTableViewWithRect:(CGRect)aRect{
    _tableView = [[UITableView alloc] initWithFrame:aRect  style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIColor *bgColor=[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
    
    
    [_tableView setBackgroundColor:bgColor];
    
    [self.view addSubview: _tableView];
    [_tableView release];
    
    
}

-(NSArray *)getSuburbInfoFromDatabase{
    
       NSArray *suburbs =  [UtilDB getSuburbInfo];
    if ([suburbs count]==0) {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading data...";
        [hud show:YES];
        
        NSURL *url =nil;
        
        url = [NSURL URLWithString:@"http://www.insyd.com.au/?option=com_djclassifieds&view=additem&task=getFields&cat_id=1&format=json"];
        
        ASIHTTPRequest *req = [[ASIHTTPRequest alloc]initWithURL:url];
        [req setShouldContinueWhenAppEntersBackground:YES];
        [req setTimeOutSeconds:200];

        [req setCompletionBlock:^{
            
            NSLog(@"req:%@",[req responseStatusMessage]);
           
            NSString *responseString = [req responseString];
            
            NSMutableArray *responseData = [responseString mutableObjectFromJSONString];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([responseData count]==0) {
                
                [WCAlertView showAlertWithTitle:@"Notice"
                                        message:@"Loading data error."
                             customizationBlock:^(WCAlertView *alertView) {
                                 alertView.style = WCAlertViewStyleVioletHatched;
                                 
                             } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                                 
                             } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                return;
            }
            else{
                
                NSDictionary *dic = [responseData objectAtIndex:1];
                NSString *suburString = [dic objectForKey:@"search_value1"];
                
                NSArray *subArrayFull = [suburString componentsSeparatedByString:@";"];
                NSRange  range= NSMakeRange(31, [subArrayFull count] - 32);
                
                NSArray *subStringArray = [subArrayFull subarrayWithRange:range];
                
                
                if (subStringArray) {
                    [UtilDB insertSuburbData:subStringArray];
                }
                
               
            }
            
           
            
        }];

        [req startAsynchronous];
        
    }

    if([suburbs count]>0){
    return suburbs;
    }
    return NULL;
}


-(void)initialData{
    
    if (!_dataSource) {
        
        _dataSource = [[NSMutableArray alloc]initWithCapacity:1];
        
        
         NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"rent.png",@"img",@"房 屋 出 租",@"name",@"1",@"type_id",nil];
         NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"car.png",@"img",@"汽 车 买 卖",@"name",@"2",@"type_id", nil];
        NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"business.png",@"img",@"生 意 转 让",@"name",@"3", @"type_id",nil];
         NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"教 材 转 让",@"name",@"4", @"type_id",nil];
         NSDictionary *dic5 = [NSDictionary dictionaryWithObjectsAndKeys:@"add.png",@"img",@"新 货 市 场",@"name",@"5", @"type_id",nil];
         NSDictionary *dic6 = [NSDictionary dictionaryWithObjectsAndKeys:@"job.png",@"img",@"招 聘 信 息",@"name",@"6",@"type_id", nil];
         NSDictionary *dic7 = [NSDictionary dictionaryWithObjectsAndKeys:@"used.png",@"img",@"二 手 市 场",@"name",@"7",@"type_id", nil];
         NSDictionary *dic8 = [NSDictionary dictionaryWithObjectsAndKeys:@"service.png",@"img",@"服 务 信 息",@"name",@"8", @"type_id",nil];
        
        [_dataSource addObject:dic1];
        [_dataSource addObject:dic2];
        [_dataSource addObject:dic3];
        [_dataSource addObject:dic4];
        [_dataSource addObject:dic5];
        [_dataSource addObject:dic6];
        [_dataSource addObject:dic7];
        [_dataSource addObject:dic8];
        
        
        _currentPageDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"1"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"2"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"3"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"4"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"5"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"6"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"7"];
        [_currentPageDic setValue:[[NSNumber alloc] initWithInt:1] forKey:@"8"];
                
        
        
        _listUrlDic = [[NSMutableDictionary alloc]initWithCapacity:1];
        
        
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/rent?json",Host_address] forKey:@"1"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/car?json",Host_address] forKey:@"2"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/business?json",Host_address] forKey:@"3"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/material?json",Host_address] forKey:@"4"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/new?json",Host_address] forKey:@"5"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/job?json",Host_address] forKey:@"6"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/used?json",Host_address] forKey:@"7"];
        [_listUrlDic setValue:[NSString stringWithFormat:@"%@/category/classifieds/service?json",Host_address] forKey:@"8"];
        
    }

}


#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    return [view autorelease];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        CGRect frame = cell.frame;
        frame.size.height = self.tableView.rowHeight;
        [cell setFrame:frame];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setBackgroundView:nil];
        [cell initCellWithType:categoryMainCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    
    NSLog(@"dic:%@",dic);
    [cell setCategoryMainIcon:[dic objectForKey:@"img"]];
    [cell setTitle:[dic objectForKey:@"name"]];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    NSDictionary *dicInfo = [_dataSource objectAtIndex:indexPath.row];
    
    CategoryListViewController *categoryListViewController = [[CategoryListViewController alloc]initWithNibName:@"CategoryListViewController" bundle:nil];

    categoryListViewController.title = [dicInfo objectForKey:@"name"];
    
//    type_id
     int categoryTypeId = ((NSString *)[dicInfo objectForKey:@"type_id"]).intValue;
    categoryListViewController.categoryType = categoryTypeId;
    
    categoryListViewController.wholeDicDataSource = _wholeDataSource;
    categoryListViewController.currentPageDic = _currentPageDic;
    categoryListViewController.listUrlDic = _listUrlDic;
    
    [self.navigationController pushViewController:categoryListViewController animated:YES];
    [categoryListViewController release];
    
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
