//
//  ENtityDetailViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-5-17.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "ENtityDetailViewController.h"
#import "ASIHTTPRequest.h"
#import "TFHpple.h"
#import "UIImageView+WebCache.h"
#import "UtilDB.h"
#import "UtilTools.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import <ShareSDK/ShareSDK.h>
#import "WCAlertView.h"
@interface ENtityDetailViewController ()

@end

@implementation ENtityDetailViewController
@synthesize entityUrl;
@synthesize shareDic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];

    
   
    
    
    if (entityUrl) {
        CGFloat height = self.view.frame.origin.y;
        CGRect rect_screen = [[UIScreen mainScreen]bounds];
        
        mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, height, self.view.frame.size.width, rect_screen.size.height)];
        
        [mainScrollView setScrollEnabled:YES];
        [self.view addSubview:mainScrollView];
        
        [mainScrollView release];

        NSLog(@"MainScrollView :%@",mainScrollView);
        
        totalHeight = 50.0;
        
       
//        UISegmentedControl *leftSeg = [[UISegmentedControl alloc] initWithItems:@[@"Back"]];
//		[leftSeg setFrame:CGRectMake(0, 0, 100, 32)];
//		[leftSeg addTarget:self action:@selector(leftButtonItemMethod:) forControlEvents:UIControlEventValueChanged];
//		leftSeg.segmentedControlStyle = UISegmentedControlStyleBar;
//		leftSeg.momentary = YES;
//        
//        UIBarButtonItem *segmentBarItemleft = [[UIBarButtonItem alloc] initWithCustomView:leftSeg];
//		[leftSeg release];
//		self.navigationItem.leftBarButtonItem = segmentBarItemleft;
//		[segmentBarItemleft release];
        
        
        
        [self startWebrequest];
        
        
        
        
        UIView *leftContainer = [[UIView alloc]initWithFrame:CGRectMake(250, 10, 66, 44)];
        
        UIButton *buttonView = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 66, 44)];
        
        [buttonView setBackgroundImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        [buttonView addTarget:self action:@selector(shareWX) forControlEvents:UIControlEventTouchUpInside];
        [leftContainer addSubview:buttonView];
        [buttonView release];
        
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        
        negativeSpacer.width = 10;
        
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
        [leftContainer release];
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, anotherButton, nil];
        [anotherButton release];

        
        
        
       
    }
    // Do any additional setup after loading the view from its nib.
}


//微信分享
-(void)shareWX{
    
    
    if (shareDic) {
        if ([shareDic objectForKey:@"weixin_url"] && [shareDic objectForKey:@"entity_link"] && [shareDic objectForKey:@"title"]) {
            
//            NSString *pic_String = [shareDic objectForKey:@"weixin_url"];
            NSString *entity_link = [shareDic objectForKey:@"entity_link"];
            
            NSString *pic_String = [shareDic objectForKey:@"weixin_url"];
            
            
            id<ISSContent> publishContent = [ShareSDK content:@"City Walker 分享"
                                               defaultContent:@""
                                                        image:[ShareSDK imageWithUrl:pic_String]
                                                        title:[shareDic objectForKey:@"title"]
                                                          url:entity_link
                                                  description:@"分享自City Walker"
                                                    mediaType:SSPublishContentMediaTypeNews];
            
            
            /**
             *	@brief	显示分享视图
             *
             *	@param 	type 	平台类型
             *  @param  container   用于显示分享界面的容器，如果只显示在iPhone客户端可以传入nil。如果需要在iPad上显示需要指定容器。
             *	@param 	content 	分享内容
             *	@param 	statusBarTips 	状态栏提示标识：YES：显示； NO：隐藏
             *	@param 	authOptions 	授权选项，用于指定接口在需要授权时的一些属性（如：是否自动授权，授权视图样式等），默认可传入nil
             *	@param 	shareOptions 	分享选项，用于定义分享视图部分属性（如：标题、一键分享列表、功能按钮等）,默认可传入nil
             *	@param 	result 	分享返回事件处理 chu li ni ma ji a a a a a a a a a a a !!!!!!!
             */
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"分享中...";
            [hud show:YES];
            
            
            [ShareSDK showShareViewWithType:ShareTypeWeixiTimeline
                                  container:nil
                                    content:publishContent
                              statusBarTips:YES authOptions:nil shareOptions:nil
                                     result:^(ShareType type, SSPublishContentState state, id<ISSStatusInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                                         if (state == SSPublishContentStateSuccess)
                                         {
//                                             NSLog(@"分享成功");
                                             [WCAlertView showAlertWithTitle:@"提示"
                                                                     message:@"分享成功。"
                                                          customizationBlock:^(WCAlertView *alertView) {
                                                              alertView.style = WCAlertViewStyleVioletHatched;
                                                              
                                                          } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                                                              
                                                          } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                             
                                             
                                         }
                                         else if (state == SSPublishContentStateFail)
                                         {
                                             [WCAlertView showAlertWithTitle:@"提示"
                                                                     message:[NSString stringWithFormat:@"分享失败：原因： %@",[error errorDescription]]
                                                          customizationBlock:^(WCAlertView *alertView) {
                                                              alertView.style = WCAlertViewStyleVioletHatched;
                                                              
                                                          } completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
                                                              
                                                          } cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                            
                                             NSLog(@"分享失败,错误码:%d,错误描述:%@" ,[error errorCode], [error errorDescription]);
                                         }
                                     }];
        }
    }
    
    
    
    
    
    
}

-(void)leftButtonItemMethod:(id)sender {
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    int index = segmentedControl.selectedSegmentIndex;
    
    if (index==0) {
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.7;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        
        animation.type = @"pageCurl";
        animation.subtype = kCATransitionFromLeft;
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
        [self.navigationController popViewControllerAnimated:NO];

    }
}


-(void)startWebrequest{
    //request http resource and return Json dictionary
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    if (entityUrl) {
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:entityUrl]];
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        NSLog(@"entityUrl:%@",entityUrl);
        
        [request setCompletionBlock:^{
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
            
            
            if (responseData) {
                
                NSArray *array = [responseData objectForKey:@"items"];
                NSDictionary *datadic = [array objectAtIndex:0];
                NSLog(@"responseData:%@",datadic);
                [self generateUIByDic:datadic];
            }
             [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
        [request setFailedBlock:^{
            NSLog(@"error:%@",request.error);
            
        }];
        
        
        
        [request startAsynchronous];
        [request release];
        
        
    }
}



-(void)generateUIByDic:(NSDictionary *)dic{

    if(!shareDic){
        shareDic = [[NSMutableDictionary alloc]initWithCapacity:1];
    }
    
    [shareDic setValue:[dic objectForKey:@"logo"] forKey:@"weixin_url"];
    [shareDic setValue:[dic objectForKey:@"link"] forKey:@"entity_link"];
    [shareDic setValue:[dic objectForKey:@"title"] forKey:@"title"];
    
    
    CGFloat pheight = [UtilDB heightForString:[dic objectForKey:@"title"] fontSize:17 andWidth:300.0f];
    CGFloat originY = mainScrollView.frame.origin.y;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(18, originY+5, 290, pheight+5)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:UITextAlignmentCenter];
    titleLabel.font = [UIFont boldSystemFontOfSize:17];
    titleLabel.numberOfLines = 2;
    titleLabel.minimumFontSize = 10;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = [dic objectForKey:@"title"];
     
    
    UILabel *scrLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, originY+pheight+15, 150, 15)];
    [scrLabel setBackgroundColor:[UIColor clearColor]];
    [scrLabel setTextAlignment:UITextAlignmentLeft];
    scrLabel.font = [UIFont boldSystemFontOfSize:13];
    scrLabel.numberOfLines = 2;
    scrLabel.minimumFontSize = 10;
    scrLabel.textColor = [UIColor blackColor];
    scrLabel.text = [dic objectForKey:@"source"];
    
    
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(30+scrLabel.frame.size.width, originY+pheight+15, 150, 15)];
    [dateLabel setBackgroundColor:[UIColor clearColor]];
    [dateLabel setTextAlignment:UITextAlignmentLeft];
    dateLabel.font = [UIFont boldSystemFontOfSize:13];
    dateLabel.numberOfLines = 2;
    dateLabel.minimumFontSize = 10;
    dateLabel.textColor = [UIColor blackColor];
    dateLabel.text = [dic objectForKey:@"publish"];
    
    
    UIImageView *titleImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bar.png"]];
    CGRect imageFrame = CGRectMake(10, 2, 300, titleLabel.frame.size.height+scrLabel.frame.size.height+10);
    
    
    [titleImageView setFrame:imageFrame];
    [mainScrollView addSubview:titleImageView];
    [titleImageView release];
    
    
    
    [mainScrollView addSubview:titleLabel];
    [titleLabel release];
    [mainScrollView addSubview:scrLabel];
    [scrLabel release];
    [mainScrollView addSubview:dateLabel];
    [dateLabel release];
    
    
    RECTLog(titleLabel.frame);
    

    totalHeight = titleImageView.frame.origin.y+titleImageView.frame.size.height+5;
    
    NSString *htmlContent =  [NSString stringWithFormat:@"<body>%@</body>",[dic objectForKey:@"content"]];
    
    NSLog(@"htmlContent:%@",htmlContent);
    [self parseContentAndGenerateDynamicUI:htmlContent];
    
}


-(void)parseContentAndGenerateDynamicUI:(NSString *)content{
    
//    NSString *htmlContect =
//    @"<body>"
//    @"<p>FirstSection---不久之前，CCAV以一部《舌尖上的中国》，牢牢地将国人钉死在“吃货”的耻辱柱上。但是，很多人却不以为“耻”，反以为“荣”！坚持流着哈喇子搜索世间美食。为了鼓励“吃货们”的坚持不懈，本期Citywalker特意为大家818历史上和吃有关的奇闻趣事。</p>"
//    @"<p style='text-align: center;'><img src='images/column/2013.5.16/chihuo.jpg' border='0' alt=''/></p>"
//    @"<p><strong>SecondSection---吃货的血案</strong></p>"
//    @"<p style=’text-align: center;‘><img src='images/column/2013.5.16/wuzetian.jpg' border='0' alt='' /><img src='images/column/2013.5.16/bing.jpg' border='0' alt=''/></p>"
//    @"<p>ThirdSection----作为本篇的开头，我们要讲述一个凄惨的故事——一张饼引发的血案。话说唐朝武则天执政时期，有位名叫张衡的四品官员，在朝廷摸爬滚打多年，终于觅得了晋升三品的机会。要知道在那个年代，只有三品以上官员才算是国家正式编制的公务员。因此，张衡遇事小心翼翼，生怕发生任何变故。但是，事儿还是来了！一天，张大人退朝回府，路上经过长安商业区，没吃早饭的他被路边香喷喷的大饼馋得直流口水，于是命人买了几张，坐在马背上啃了起来。殊不知，街边买个零食这样的小事，竟被唐朝纪检委揪住不放，以有损国家干部形象之罪上报给了女皇。武则天闻之也觉得很丢脸，马上批示：“流外出身，永不续入三品。”就这样，张衡因为一张饼而丢了一个官。</p>"
//    @"</body>";
    
    NSData *htmlData = [content  dataUsingEncoding: NSUTF8StringEncoding];
    TFHpple *xpathParser = [[TFHpple alloc]initWithHTMLData:htmlData];
    NSArray *elements  = [xpathParser searchWithXPathQuery:@"//body"];
    if (!elementArray) {
        elementArray = [[NSMutableArray alloc]initWithCapacity:1];
    }
    for (TFHppleElement *element in elements) {
        for (TFHppleElement *node in element.children) {
            if ([node.tagName isEqualToString:@"p"]) {
                for (int i=0;i<[node.children count];i++) {
                    TFHppleElement *subnode = [node.children objectAtIndex:i];
                    //处理P段落的地方
                    //                        NSLog(@"Node.content:%@ subnode.children %@",subnode,subnode.children);
                    //P段落或者有图
                    if ([subnode.children count]==0) {
                        //                        NSLog(@"Node.content:%@ subnode.attributes %@",subnode,subnode.attributes);
                        //段落
                        if ([subnode.attributes count]==0) {
//                            NSLog(@"Text p:%@",subnode.content);
                            NSString *pString = subnode.content;
                            CGFloat pheight = [UtilDB heightForString:pString fontSize:15 andWidth:300.0f];
                            
                            UILabel *stringView = [[UILabel alloc]initWithFrame:CGRectMake(10, totalHeight, 300, pheight)];
                            [stringView setBackgroundColor:[UIColor clearColor]];
                            stringView.lineBreakMode = UILineBreakModeWordWrap;
                             stringView.numberOfLines = 0; 
//                            [stringView setEditable:NO];
//                            [stringView setScrollEnabled:NO];
                            stringView.font = [UIFont systemFontOfSize:15];
                            
                            stringView.text = pString;
                            [stringView setTag:i];
                            [elementArray addObject:stringView];
                            [stringView release];
                            totalHeight = totalHeight+stringView.frame.size.height+20;
                            [mainScrollView addSubview:stringView];
                            [stringView release];
                            
                            
                        }else{
                            //图
                            NSString *stringPhotoUrl = [((NSDictionary *)subnode.attributes) objectForKey:@"src"];
                            NSString *fullPath = [NSString stringWithFormat:@"%@",stringPhotoUrl];
                            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, totalHeight, 310, 0)];
                            [imageView setTag:i];
                            [elementArray addObject:imageView];
                            [imageView release];
//                            [mainScrollView addSubview:imageView];
//                            [imageView release];
                            
                            [self dealWithWebImage:fullPath andImageView:imageView];
                            
                            
                            
                            
                        }
                    }else {
                        if ([subnode.children objectAtIndex:0]) {
                            TFHppleElement *stringTag = [subnode.children objectAtIndex:0];
                            //粗体的tag
                            NSString *tagString = stringTag.content;
                            CGFloat pheight = [UtilDB heightForString:tagString fontSize:15 andWidth:300.0f];
                            
                            UILabel *tagLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, totalHeight, 300, pheight)];
                            [tagLabel setBackgroundColor:[UIColor clearColor]];
                            tagLabel.lineBreakMode = UILineBreakModeWordWrap;
                            tagLabel.numberOfLines = 0;
                            //                            [stringView setEditable:NO];
                            //                            [stringView setScrollEnabled:NO];
                            tagLabel.font =   [UIFont fontWithName:@"Helvetica-Bold" size:15];
                            
                            tagLabel.text = tagString;
                            [elementArray addObject:tagLabel];
                            [tagLabel release];
                            totalHeight = totalHeight+tagLabel.frame.size.height+20;
                            [mainScrollView addSubview:tagLabel];
                            [tagLabel release];

                        }
                    }
                }
            }
        }
    }
    
    
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGFloat screenHeight = rect_screen.size.height;
    
    totalHeight = (totalHeight<=screenHeight) ? screenHeight : totalHeight;

     [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, totalHeight+94)];
    
    [xpathParser release];

}


-(UIImageView *)dealWithWebImage:(NSString *)webImageUrl andImageView:(UIImageView *)imageView{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
//    UIImage *cachedImage = [manager imageWithURL:[NSURL URLWithString:webImageUrl]];
    UIImage *cachedImage = [manager imageWithURL:[NSURL URLWithString: [webImageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
   
    if (cachedImage)
    {
         CGFloat proportionalHeight = [UtilTools getProportionalHeightFromImage:cachedImage];
        [imageView setFrame:CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y, imageView.frame.size.width, proportionalHeight)];
        [imageView setImage:cachedImage];
        totalHeight = totalHeight+proportionalHeight+20;
        [mainScrollView addSubview:imageView];
        [imageView release];
    }
    else
    {
//        [cachedImage addObserver:imageView forKeyPath:@"size" options:NSKeyValueObservingOptionNew context:nil];
//        [manager downloadWithURL:[NSURL URLWithString:webImageUrl] delegate:self];

        
        [manager downloadWithURL:[NSURL URLWithString:[webImageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] delegate:self options:SDWebImageRetryFailed userInfo:nil success:
         ^(UIImage *image, BOOL cached) {
//             cached = NO;
             [self updateElementPosition:elementArray andimage:image andContainerView:imageView];
         }
         
        failure:^(NSError *error) {
            
        }];
    }
    return NULL;
}


-(void)updateElementPosition:(NSArray *)eleArr andimage:(UIImage *)Image andContainerView:(UIImageView *)imageView{
    CGFloat proportionalHeight = [UtilTools getProportionalHeightFromImage:Image];
    float gap = proportionalHeight - imageView.frame.size.height+20;
    [imageView setFrame:CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y, imageView.frame.size.width, proportionalHeight)];
    [imageView setImage:Image];
    
    totalHeight = totalHeight+proportionalHeight+20;
    [mainScrollView addSubview:imageView];
    [imageView release];
    
   
    int index = [eleArr indexOfObject:imageView];
    for (int i = index+1; i<[eleArr count]; i++) {
        UIView *elementView = [eleArr objectAtIndex:i];
        CGFloat originX = elementView.frame.origin.x;
        CGFloat originY = elementView.frame.origin.y;
        [elementView setOrigin:CGPointMake(originX, originY+gap)];
    }
    
     [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, totalHeight+94)];
    
//    PP_RELEASE(elementArray);
}


//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    if ([keyPath isEqualToString:@"size"]) {
//        NSLog(@"%@", change);
//    }
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
