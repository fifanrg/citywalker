//
//  MyPostViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-9-17.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "MyPostViewController.h"
#import "EntityCell.h"
#import "CategoryDetailViewController.h"
@interface MyPostViewController ()

@end

@implementation MyPostViewController
@synthesize dataSource = _dataSource;
@synthesize tableView = _tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];
    
    NSMutableArray *myPosts =  [UtilDB getMyPost];
    
    if (!myPosts || [myPosts count]==0) {
        
        UILabel *emptyPost = [[UILabel alloc]initWithFrame:CGRectMake(70, 100, 200, 100)];
        emptyPost.text = @"你暂时还没有任何发布。";
        emptyPost.textColor = [UIColor blackColor];
        [emptyPost setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:emptyPost];
        [emptyPost release];
        
    }
    else{
        if (!_tableView) {
            CGRect rect_screen = [[UIScreen mainScreen]bounds];
            
            
            [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                                   self.view.bounds.origin.y,
                                                   self.view.frame.size.width,
                                                   rect_screen.size.height-108.0)];
            
            _dataSource = myPosts;

        }
    }
    
    
    // Do any additional setup after loading the view from its nib.
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSMutableArray *myPosts =  [UtilDB getMyPost];
    
    if (!myPosts || [myPosts count]==0) {
        
        UILabel *emptyPost = [[UILabel alloc]initWithFrame:CGRectMake(70, 100, 200, 100)];
        emptyPost.text = @"你暂时还没有任何发布。";
        emptyPost.textColor = [UIColor blackColor];
        [emptyPost setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:emptyPost];
        [emptyPost release];
        
    }
    else{
        if (!_tableView) {
            CGRect rect_screen = [[UIScreen mainScreen]bounds];
            
            
            [self initTableViewWithRect:CGRectMake(self.view.bounds.origin.x,
                                                   self.view.bounds.origin.y,
                                                   self.view.frame.size.width,
                                                   rect_screen.size.height-108.0)];
            
            _dataSource = myPosts;
        }else
            
            _dataSource = myPosts;
            [_tableView reloadData];
    }
}


-(void)initTableViewWithRect:(CGRect)aRect{
    _tableView = [[UITableView alloc] initWithFrame:aRect  style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIColor *bgColor=[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
    
    [_tableView setBackgroundColor:bgColor];
    
    [self.view addSubview: _tableView];
    [_tableView release];
    
}


#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataSource count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectZero];
    return [view autorelease];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[EntityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        CGRect frame = cell.frame;
        frame.size.height = self.tableView.rowHeight;
        [cell setFrame:frame];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setBackgroundView:nil];
        [cell initCellWithType:newsCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    
    [cell setTitle:[dic objectForKey:@"title"]];
   
    
    NSString *location = [dic objectForKey:@"address"];
    
    //    [location stringByReplacingOccurrencesOfString:@"(null)" withString:@"无注明"];
    [cell setLocation: [NSString stringWithFormat:@"地区： %@", location]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
   
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = 0.7;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"pageCurl";
    
    NSDictionary *dicInfo = [_dataSource objectAtIndex:indexPath.row];
    
    CategoryDetailViewController *categoryDetailViewController = [[CategoryDetailViewController alloc]initWithNibName:@"CategoryDetailViewController" bundle:nil];
    
    
    categoryDetailViewController.itemId = [dicInfo objectForKey:@"item_id"];
    categoryDetailViewController.typeId = [dicInfo objectForKey:@"catId"];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"Back";
    self.navigationItem.backBarButtonItem = backItem;
    [backItem setAction:@selector(doback:)];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
    [self.navigationController pushViewController:categoryDetailViewController animated:NO];
    [categoryDetailViewController release];
    
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
