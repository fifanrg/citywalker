//
//  RestautantDetailViewController.m
//  CityWalker
//
//  Created by Jianing Ren on 13-7-9.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import "RestautantDetailViewController.h"
#import "ASIHTTPRequest.h"
#import "TFHpple.h"
#import "UIImageView+WebCache.h"
#import "UtilDB.h"
#import "UtilTools.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "AttributedLabel.h"


@interface RestautantDetailViewController ()

@end

@implementation RestautantDetailViewController
@synthesize urlString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)startWebrequest{
    //request http resource and return Json dictionary
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    if (urlString) {
        ASIHTTPRequest *request = [[ASIHTTPRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://insyd.com.au%@",urlString]]];
        [request setShouldContinueWhenAppEntersBackground:YES];
        
        NSLog(@"entityUrl:%@",urlString);
        
        [request setCompletionBlock:^{
            NSString *responseString = [request responseString];
            
            NSDictionary *responseData = [responseString mutableObjectFromJSONString];
            
            NSLog(@"responseData:%@",responseData);
            if (responseData) {
                
                [self generateUIbyDic:responseData];
              
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }];
        [request setFailedBlock:^{
            NSLog(@"error:%@",request.error);
            
        }];
        
        
        
        [request startAsynchronous];
        [request release];
    }
}


//-(void)setGallaryPhtos:(NSArray *)images{
//    
//    imageGallary.contentSize = CGSizeMake([images count]*320, 180.);
//    pageController.numberOfPages = [images count];
//    //    [self updateDots];
//    
//    for (int i=0; i<[images count]; i++) {
//        NSURL *imageURL = [NSURL URLWithString:[images objectAtIndex:i]];
//        //UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
//        //UIImageView *img = [[UIImageView alloc] initWithImage:image];
//        UIImageView *img = [[UIImageView alloc] init];
//        [img setImageWithURL:imageURL];
//        
//        img.frame = CGRectMake(i*320, 0, 320, 180);
//        //        img.layer.borderColor = [[UIColor whiteColor]CGColor];
//        //        img.layer.borderWidth = 5.0f;
//        CGRect captionViewFrame = CGRectMake(0, img.frame.size.height-40, 320, 40);
//        UILabel *captionView = [[UILabel alloc]initWithFrame:captionViewFrame];
//        [captionView setUserInteractionEnabled:NO];
//        [captionView setBackgroundColor:[UIColor blackColor]];
//        captionView.alpha = 0.4;
//        captionView.textColor = [UIColor whiteColor];
//        [captionView setTextAlignment:UITextAlignmentLeft];
//        captionView.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:16];
//        captionView.numberOfLines = 2;
//        captionView.minimumFontSize = 10;
//        
//        
//        //        UILabel *captionTitle = [[UILabel alloc]initWithFrame:captionViewFrame];
//        //        [captionTitle setBackgroundColor:[UIColor clearColor]];
//        captionView.text = @"小角落里的大发现";
//        //        captionTitle.textColor = [UIColor whiteColor];
//        [img addSubview:captionView];
//        //        [img addSubview:captionTitle];
//        [captionView release];
//        //        [captionTitle release];
//        
//        [imageGallary addSubview:img];
//        
//        [img release];
//    }
//}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    UIScrollView *gallaryScrollView = sender;
    
    int page = gallaryScrollView.contentOffset.x / 300;
    pageController.currentPage = page;
    
}


-(void)generateUIbyDic:(NSDictionary *)dataDic{
    NSArray *gallaryArray = [dataDic objectForKey:@"gallery"];
    CGRect gallaryScrollViewFrame = CGRectMake(10, 10, 300, 180);
    
    
   
    if ([gallaryArray count]>0) {
       UIScrollView *gallaryScrollView  = [[UIScrollView alloc]initWithFrame:gallaryScrollViewFrame];
        
        CGRect pageControllerFrame = CGRectMake(125, 160, 50, 30);
        
        
        gallaryScrollView.delegate = self;
        gallaryScrollView.showsHorizontalScrollIndicator = FALSE;
        [gallaryScrollView setScrollEnabled:YES];
        [gallaryScrollView setPagingEnabled:YES];  
        
        gallaryScrollView.contentSize = CGSizeMake([gallaryArray count]*300, 180.);
        
        for (int i=0; i<[gallaryArray count]; i++) {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://insyd.com.au%@",[gallaryArray objectAtIndex:i]]];
            //UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
            //UIImageView *img = [[UIImageView alloc] initWithImage:image];
            UIImageView *img = [[UIImageView alloc] init];
            [img setImageWithURL:imageURL];

            img.frame = CGRectMake(i*300, 0, 300, 180);
            //        img.layer.borderColor = [[UIColor whiteColor]CGColor];
            //        img.layer.borderWidth = 5.0f;
            CGRect captionViewFrame = CGRectMake(0, img.frame.size.height-40, 300, 40);
            UILabel *captionView = [[UILabel alloc]initWithFrame:captionViewFrame];
            [captionView setUserInteractionEnabled:NO];
            [captionView setBackgroundColor:[UIColor blackColor]];
            captionView.alpha = 0.4;
            captionView.textColor = [UIColor whiteColor];
            [captionView setTextAlignment:UITextAlignmentLeft];
            captionView.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:16];
            captionView.numberOfLines = 2;
            captionView.minimumFontSize = 10;

            [img addSubview:captionView];
            [captionView release];
            [gallaryScrollView addSubview:img];
            [img release];
        }
        [mainScrollView addSubview:gallaryScrollView];
        [gallaryScrollView release];
        
        
        if (!pageController) {
            pageController = [[UIPageControl alloc]initWithFrame:pageControllerFrame];
            [mainScrollView addSubview:pageController];
            [mainScrollView bringSubviewToFront:pageController];
            [pageController release];
        }
        else
            [pageController setFrame:pageControllerFrame];
        
        pageController.numberOfPages = [gallaryArray count];
        pageController.currentPage = 0;
        [pageController setBackgroundColor:[UIColor clearColor]];

        
    }
    
    
//    NSString *detailTitle = [dataDic objectForKey:@"title"];
     CGFloat titlePheight = [UtilDB heightForString:[dataDic objectForKey:@"title"] fontSize:17 andWidth:300.0f];
     CGFloat originY = gallaryScrollViewFrame.size.height ;
     
     UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, originY+10, 300, titlePheight+5)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:UITextAlignmentLeft];
        titleLabel.font = [UIFont boldSystemFontOfSize:17];
        titleLabel.numberOfLines = 2;
        titleLabel.minimumFontSize = 10;
        titleLabel.textColor = [UIColor redColor];
        titleLabel.text = [dataDic objectForKey:@"title"];

    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10,titleLabel.frame.origin.y+titleLabel.frame.size.height-2, 300, 3)];
    
    
    [imageView setImage:[UIImage imageNamed:@"seperator"]];
    
    [mainScrollView addSubview:imageView];
    [imageView release];
    

     AttributedLabel *areaLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+10, 100, 30)];
     AttributedLabel *suisineLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10+areaLabel.frame.size.width+10, originY+titleLabel.frame.size.height+10, 100, 30)];
     AttributedLabel *priceLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10+areaLabel.frame.size.width+suisineLabel.frame.size.width+10+10, originY+titleLabel.frame.size.height+10, 100, 30)];
    
    
//    NSString *priceString = [UtilDB setStringValue:[dataDic objectForKey:@"price"]];
//    suisineLabel.text = [NSString stringWithFormat:@"价钱  %@",priceString];
//    
//    [suisineLabel setColor:[UIColor redColor] fromIndex:0 length:2];
//    [suisineLabel setColor:[UIColor blackColor] fromIndex:3 length:priceString.length];    
//    [suisineLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:suisineLabel.text.length];
//    
//    [suisineLabel setBackgroundColor:[UIColor clearColor]];
//    [suisineLabel setTextAlignment:UITextAlignmentLeft];
//    suisineLabel.numberOfLines = 2;
//    suisineLabel.minimumFontSize = 10;
    
    NSString *areaString = [UtilDB setStringValue:[dataDic objectForKey:@"rating_suburb"]];
    
    areaLabel.text = [NSString stringWithFormat:@"地区 %@",areaString];
    [areaLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [areaLabel setColor:[UIColor blackColor] fromIndex:3 length:areaString.length];
    
    [areaLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:areaLabel.text.length];
    
    [areaLabel setBackgroundColor:[UIColor clearColor]];
    [areaLabel setTextAlignment:UITextAlignmentLeft];
    areaLabel.numberOfLines = 2;
    areaLabel.minimumFontSize = 10;

    
    
    UIImageView *imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(10,areaLabel.frame.origin.y+areaLabel.frame.size.height-4 , 300, 3)];
    
    
    [imageView2 setImage:[UIImage imageNamed:@"seperator"]];
    
    [mainScrollView addSubview:imageView2];
    [imageView2 release];
    

    
    
    NSString *suisineString = [UtilDB setStringValue:[dataDic objectForKey:@"rating_cuisines"]];
    suisineLabel.text = [NSString stringWithFormat:@"菜系 %@",suisineString];
    [suisineLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [suisineLabel setColor:[UIColor blackColor] fromIndex:3 length:suisineString.length];
    
    [suisineLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:suisineLabel.text.length];
    [suisineLabel setBackgroundColor:[UIColor clearColor]];
    [suisineLabel setTextAlignment:UITextAlignmentLeft];
    suisineLabel.numberOfLines = 2;
    suisineLabel.minimumFontSize = 10;

    

    NSString *priceString = [UtilDB setStringValue:[dataDic objectForKey:@"rating_price"]];
    priceLabel.text = [NSString stringWithFormat:@"人均 $%@",priceString];
    [priceLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [priceLabel setColor:[UIColor blackColor] fromIndex:3 length:priceString.length];
    [priceLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:priceLabel.text.length];
   
    
    [priceLabel setBackgroundColor:[UIColor clearColor]];
    [priceLabel setTextAlignment:UITextAlignmentLeft];

    priceLabel.numberOfLines = 2;
    priceLabel.minimumFontSize = 10;

    

    AttributedLabel *phoneLabel = [[AttributedLabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+priceLabel.frame.size.height+10, 300, 30)];
    
    NSString *phoneString = [UtilDB setStringValue:[dataDic objectForKey:@"rating_phone"]];
    phoneLabel.text = [NSString stringWithFormat:@"电话 %@",phoneString];
    [phoneLabel setColor:[UIColor redColor] fromIndex:0 length:2];
    [phoneLabel setColor:[UIColor blackColor] fromIndex:3 length:phoneString.length];
    [phoneLabel setFont:[UIFont boldSystemFontOfSize:14] fromIndex:0 length:phoneLabel.text.length];

    
    [phoneLabel setBackgroundColor:[UIColor clearColor]];
    [phoneLabel setTextAlignment:UITextAlignmentLeft];

    phoneLabel.numberOfLines = 2;
    phoneLabel.minimumFontSize = 10;

    
    UIImageView *imageView3 = [[UIImageView alloc]initWithFrame:CGRectMake(10,phoneLabel.frame.origin.y+phoneLabel.frame.size.height-4 , 300, 3)];
    
    
    [imageView3 setImage:[UIImage imageNamed:@"seperator"]];
    
    [mainScrollView addSubview:imageView3];
    [imageView3 release];
    



    CGFloat adressPheight = [UtilDB heightForString:[dataDic objectForKey:@"rating_address"] fontSize:15 andWidth:275.0f];

    
    UILabel *addressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, originY+titleLabel.frame.size.height+priceLabel.frame.size.height+7+phoneLabel.frame.size.height, 28, 30)];
    
    [addressTitleLabel setBackgroundColor:[UIColor clearColor]];
    [addressTitleLabel setTextAlignment:UITextAlignmentLeft];
    addressTitleLabel.font = [UIFont boldSystemFontOfSize:14];
    addressTitleLabel.numberOfLines = 2;
    addressTitleLabel.minimumFontSize = 10;
    addressTitleLabel.textColor = [UIColor redColor];
    addressTitleLabel.text = @"地址";
    
    
    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10+addressTitleLabel.frame.origin.x+addressTitleLabel.frame.size.width, originY+titleLabel.frame.size.height+priceLabel.frame.size.height+10+phoneLabel.frame.size.height, 275, adressPheight)];
    
    
    
    [addressLabel setBackgroundColor:[UIColor clearColor]];
    [addressLabel setTextAlignment:UITextAlignmentLeft];
    addressLabel.font = [UIFont boldSystemFontOfSize:14];
    addressLabel.numberOfLines = 2;
    addressLabel.minimumFontSize = 10;
    addressLabel.textColor = [UIColor blackColor];
    
    NSString *addressString = [UtilDB setStringValue:[dataDic objectForKey:@"rating_address"]];
    addressLabel.text = addressString;


    [mainScrollView addSubview:titleLabel];
    [titleLabel release];
    [mainScrollView addSubview:areaLabel];
    [areaLabel release];
    [mainScrollView addSubview:suisineLabel];
    [suisineLabel release];
    [mainScrollView addSubview:priceLabel];
    [priceLabel release];
    [mainScrollView addSubview:phoneLabel];
    [phoneLabel release];
    
    [mainScrollView addSubview:addressTitleLabel];
    [addressTitleLabel release];
    [mainScrollView addSubview:addressLabel];
    [addressLabel release];

    CGFloat commentStartOriginY = addressLabel.frame.origin.y + addressLabel.frame.size.height+10;

    UILabel *commentLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, commentStartOriginY, 300, 20)];
    
    [commentLabel setBackgroundColor:[UIColor grayColor]];
    [commentLabel setTextAlignment:UITextAlignmentLeft];
    commentLabel.font = [UIFont boldSystemFontOfSize:14];
    commentLabel.numberOfLines = 2;
    commentLabel.minimumFontSize = 10;
    commentLabel.textColor = [UIColor whiteColor];

    commentLabel.text = @"点评";

    [mainScrollView addSubview:commentLabel];
    [commentLabel release];

    NSArray *comments = [dataDic objectForKey:@"jcomments"];
    commentStartOriginY = commentStartOriginY+30;
    if([comments count]==0){
        
        UILabel *nocommentLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, commentStartOriginY, 300, 20)];
        
        [nocommentLabel setBackgroundColor:[UIColor clearColor]];
        [nocommentLabel setTextAlignment:UITextAlignmentCenter];
        nocommentLabel.font = [UIFont boldSystemFontOfSize:15];
        nocommentLabel.numberOfLines = 2;
        nocommentLabel.minimumFontSize = 10;
        nocommentLabel.textColor = [UIColor blackColor];
        
        
        [mainScrollView addSubview:nocommentLabel];
        [nocommentLabel release];
        
    }else{
        
        for(int i=0;i<[comments count];i++){
            
            NSDictionary *commentDic = [comments objectAtIndex:i];
            
//            CGFloat adressPheight = [UtilDB heightForString:[commentDic objectForKey:@"comment"] fontSize:14 andWidth:300.0f];
            UILabel *userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, commentStartOriginY, 160, 20)];
            
            [userNameLabel setBackgroundColor:[UIColor clearColor]];
            [userNameLabel setTextAlignment:UITextAlignmentLeft];
            userNameLabel.font = [UIFont boldSystemFontOfSize:13];
            userNameLabel.numberOfLines = 2;
            userNameLabel.minimumFontSize = 10;
            userNameLabel.textColor = [UIColor blackColor];
            userNameLabel.text = [commentDic objectForKey:@"name"];
            UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10+userNameLabel.frame.size.width, commentStartOriginY, 140, 20)];
            
            [dateLabel setBackgroundColor:[UIColor clearColor]];
            [dateLabel setTextAlignment:UITextAlignmentLeft];
            dateLabel.font = [UIFont boldSystemFontOfSize:13];
            dateLabel.numberOfLines = 2;
            dateLabel.minimumFontSize = 10;
            dateLabel.textColor = [UIColor blackColor];
            dateLabel.text = [commentDic objectForKey:@"datetime"];
            commentStartOriginY = commentStartOriginY+20;
            
            
            CGFloat commentPheight = [UtilDB heightForString:[commentDic objectForKey:@"comment"] fontSize:12 andWidth:300.0f];
//            commentPheight = commentPheight+20;
             UILabel *commentLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, commentStartOriginY, 300, commentPheight)];
            commentStartOriginY = commentStartOriginY+commentPheight+20;
            [commentLabel setTextAlignment:UITextAlignmentLeft];
            commentLabel.font = [UIFont boldSystemFontOfSize:12];
            commentLabel.numberOfLines = 10;
            commentLabel.minimumFontSize = 10;
            commentLabel.textColor = [UIColor blackColor];
            commentLabel.text = [commentDic objectForKey:@"comment"];
            
            commentLabel.backgroundColor = [UIColor clearColor];
            
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, commentStartOriginY-3, 300, 3)];
            
            
            [imageView setImage:[UIImage imageNamed:@"xuxian"]];
            
            commentStartOriginY = commentStartOriginY+3;
            
            
            [mainScrollView addSubview:userNameLabel];
            [mainScrollView addSubview:dateLabel];
            [mainScrollView addSubview:commentLabel];
            [mainScrollView addSubview:imageView];
            [imageView release];
            [userNameLabel release];
            [dateLabel release];
            [commentLabel release];
        }
    }


     [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, commentStartOriginY+94)];
}


-(void)leftButtonItemMethod:(id)sender {
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    int index = segmentedControl.selectedSegmentIndex;
    
    if (index==0) {
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.7;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        
        animation.type = @"pageCurl";
        animation.subtype = kCATransitionFromLeft;
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"animation"];
        [self.navigationController popViewControllerAnimated:NO];
        
    }
}



- (void)viewDidLoad
{
    CGFloat height = self.view.frame.origin.y;
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    
    mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, height, self.view.frame.size.width, rect_screen.size.height)];
    
    [mainScrollView setScrollEnabled:YES];
    [self.view addSubview:mainScrollView];
    
    [mainScrollView release];
    
    NSLog(@"MainScrollView :%@",mainScrollView);
    
    totalHeight = 50.0;
    
//    UISegmentedControl *leftSeg = [[UISegmentedControl alloc] initWithItems:@[@"Back"]];
//    [leftSeg setFrame:CGRectMake(0, 0, 100, 32)];
//    [leftSeg addTarget:self action:@selector(leftButtonItemMethod:) forControlEvents:UIControlEventValueChanged];
//    leftSeg.segmentedControlStyle = UISegmentedControlStyleBar;
//    leftSeg.momentary = YES;
    
//    UIBarButtonItem *segmentBarItemleft = [[UIBarButtonItem alloc] initWithCustomView:leftSeg];
//    [leftSeg release];
//    self.navigationItem.leftBarButtonItem = segmentBarItemleft;
//    [segmentBarItemleft release];
    
    
    [self startWebrequest];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_backgrund"]];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
