//
//  PostActivityViewController.h
//  CityWalker
//
//  Created by Jianing Ren on 13-4-11.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBImagePickerController.h"
#import "MWPhotoBrowser.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "NIDropDown.h"
#import "SuburbAreaSelectionsViewController.h"
#import "ASIHTTPRequestDelegate.h"
@interface PostActivityViewController : UIViewController<QBImagePickerControllerDelegate,MWPhotoBrowserDelegate,UITextFieldDelegate,NIDropDownDelegate,suburbviewDeledate,ASIHTTPRequestDelegate>






@end
