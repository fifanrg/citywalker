//
//  UtilDB.h
//  CityWalker
//
//  Created by Jianing Ren on 13-5-16.
//  Copyright (c) 2013年 Jianing Ren. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import <CoreLocation/CoreLocation.h>
@interface UtilDB : NSObject
{
    sqlite3 *database;
    NSString *path;
}


+(void)initDB;


+(BOOL)checkreadTag:(NSDictionary *)dic withType:(NSString *)category;


+(void)insertReadRecord:(NSDictionary *)dic withType:(NSString *)category;

+(float)heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width;

+(NSArray *)getSuburbInfo;

+(void)insertSuburbData:(NSArray *)suburbs;

+(void)insertNewsCacheData:(NSArray *)newsCache bySelected_index:(int)selectedIndex ifisGallary:(BOOL)isGallary;

+(NSMutableArray *)getNewsCacheDatabySelected_index:(int)selectedIndex ifisGallary:(BOOL)isGallary;



//Post item cache.
+(NSMutableArray *)getMyPost;

+(void)saveMyPost:(NSMutableDictionary *)saveDic;


+(NSString *)setStringValue:(id)value;

+(NSString *)returnDateString:(NSString *)dateString;

+ (NSString *)escape:(NSString *)text;

@end
